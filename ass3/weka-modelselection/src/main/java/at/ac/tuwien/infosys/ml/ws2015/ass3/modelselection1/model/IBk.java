package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;
import weka.core.*;
import weka.core.neighboursearch.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for Weka 'IBk' (k-Nearest-Neighbour) classifier.
 */
@Model(names = {"knn", "ibk"})
public class IBk extends AbstractModel<weka.classifiers.lazy.IBk> {

    /**
     * Nearest neighbour search algorithms.
     */
    enum NearestNeighbourSearchAlgorithm {

        LinearNN {
            @Override
            NearestNeighbourSearch createWekaInstance() {
                return new LinearNNSearch();
            }
        },

        KDTree {
            @Override
            NearestNeighbourSearch createWekaInstance() {
                return new KDTree();
            }
        },

        CoverTree {
            @Override
            NearestNeighbourSearch createWekaInstance() {
                return new CoverTree();
            }
        },

        BallTree {
            @Override
            NearestNeighbourSearch createWekaInstance() {
                return new BallTree();
            }
        };

        /**
         * Creates a Weka NearestNeighbourSearch instance for the NearestNeighbourSearch option.
         * @return a Weka NearestNeighbourSearch instance for the NearestNeighbourSearch option.
         */
        abstract NearestNeighbourSearch createWekaInstance();

        /**
         * Returns the default nearest neighbour search algorithm.
         * @return the default nearest neighbour search algorithm.
         */
        static NearestNeighbourSearchAlgorithm getDefault() {
            return LinearNN;
        }
    }

    /**
     * Distance functions for Nearest Neighbour search.
     */
    enum DistanceFunction {

        Chebyshev {
            @Override
            NormalizableDistance createWekaInstance() {
                return new ChebyshevDistance();
            }
        },

        Euclidean {
            @Override
            NormalizableDistance createWekaInstance() {
                return new EuclideanDistance();
            }
        },

        Manhattan {
            @Override
            NormalizableDistance createWekaInstance() {
                return new ManhattanDistance();
            }
        };

        /**
         * Creates a Weka NormalizableDistance instnace for the NearestNeighbourSearch option.
         * @return
         */
        abstract weka.core.NormalizableDistance createWekaInstance();

        /**
         * Returns the default distance function.
         * @return the default distance function.
         */
        static DistanceFunction getDefault() {
            return Euclidean;
        }
    }

    /**
     * The parameter information.
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -K :: Number of nearest neighbours (k) used in classification.
        paramInfo.put("K", Integer.class);
        // * -A :: The NN search algorithm (cf. NearestNeighbourSearchAlgorithm)
        paramInfo.put("A", String.class);
        // * -A.A :: The distance function used by the NN search algorithm (cf. DistanceFunction)
        paramInfo.put("A.A", String.class);
    }

    /**
     * The default parameter grid to use for model selection.
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("K", 1, 3, 5, 7);
        defaultGrid.setValues("A", NearestNeighbourSearchAlgorithm.getDefault().name());
        defaultGrid.setValues("A.A", DistanceFunction.getDefault().name());
    }

    /**
     * Constructor.
     */
    public IBk() {
        super(new weka.classifiers.lazy.IBk());
        // - disable normalization, our model selection frameworks takes care of it.
        weka.core.DistanceFunction df = this.wekaClassifier.getNearestNeighbourSearchAlgorithm().getDistanceFunction();
        if (df instanceof NormalizableDistance) {
            ((NormalizableDistance)df).setDontNormalize(true);
        }
    }


    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        return defaultGrid;
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        return paramInfo;
    }

    @Override
    public boolean checkForTaskTypeSupport(TaskType taskType) {
        return (taskType == TaskType.Classification);
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        switch (param) {
            case "K" :
                wekaClassifier.setKNN((Integer)value);
                break;
            case "A" : {
                NearestNeighbourSearchAlgorithm nnSearchAlgo = NearestNeighbourSearchAlgorithm.valueOf((String)value);

                NearestNeighbourSearch nnSearchWeka = nnSearchAlgo.createWekaInstance();
                nnSearchWeka.setDistanceFunction(wekaClassifier.getNearestNeighbourSearchAlgorithm().getDistanceFunction());

                wekaClassifier.setNearestNeighbourSearchAlgorithm(nnSearchWeka);
                break;
            }
            case "A.A" : {
                DistanceFunction distFunc = DistanceFunction.valueOf((String)value);

                NearestNeighbourSearch nnSearchWeka = wekaClassifier.getNearestNeighbourSearchAlgorithm();
                NormalizableDistance distFuncWeka = distFunc.createWekaInstance();
                // - disable normalization, our model selection frameworks takes care of it.
                distFuncWeka.setDontNormalize(true);

                nnSearchWeka.setDistanceFunction(distFuncWeka);
                break;
            }
            default :
                throw new UnknownParameterException("Unknown parameter '" + param + "'");
        }
    }
}
