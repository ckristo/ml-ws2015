package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

/**
 * Enumeration for ML task type (classification, regression).
 */
public enum TaskType {

    Classification,

    Regression

}
