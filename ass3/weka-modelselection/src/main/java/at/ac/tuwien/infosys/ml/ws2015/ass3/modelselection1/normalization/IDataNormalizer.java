package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization;

import weka.core.Instances;

/**
 * Data normalizer interface.
 */
public interface IDataNormalizer {

    /**
     * Performs preparation of a data normalizer.
     * @param dataSet the data set used to prepare the data normalizer.
     */
    void prepare(Instances dataSet);

    /**
     * Checks if the data normalizer was prepared yet.
     *
     * @return true if data normalizer was prepared, false otherwise.
     */
    boolean isPrepared();

    /**
     * Performs data normalization.
     * If the data normalizer was not prepared previously, it will be prepared using the given data set first.
     *
     * @param dataSet the data set to normalize
     * @return a normalized copy of the data set.
     */
    Instances normalize(Instances dataSet);

    /**
     * Performs data normalization.
     *
     * @param dataSet the data set to normalize
     * @param prepare whether the data normalizer should be prepared (once again) first.
     * @return a normalized copy of the data set.
     */
    Instances normalize(Instances dataSet, boolean prepare);

    /**
     * Resets the data normalizer.
     */
    void reset();
}
