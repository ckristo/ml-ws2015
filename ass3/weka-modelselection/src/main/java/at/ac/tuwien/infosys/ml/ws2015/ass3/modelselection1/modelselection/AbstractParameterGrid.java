package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import com.google.common.collect.Sets;

import java.util.*;

/**
 * Abstract parameter grid implementation.
 */
public abstract class AbstractParameterGrid<KeyType, ValueType> implements IParameterGrid<KeyType, ValueType> {

    /**
     * A map that contains the parameters set in the parameter grid.
     */
    private LinkedHashMap<KeyType, Set<ValueType>> paramGrid = new LinkedHashMap<>();

    /**
     * Iterator class for expanding the grid (to every possible parameter combination).
     */
    class ExpandGridIterator implements Iterator<Map<KeyType, ValueType>> {

        /**
         * The list of parameter names.
         */
        List<KeyType> keys = new ArrayList<>();
        /**
         * The list of parameter values.
         */
        List<Set<ValueType>> values = new ArrayList<>();
        /**
         * A set for the result of the cartesian product of all grid values.
         */
        Set<List<ValueType>> cartesianProductForValues = null;
        /**
         * An iterator instance for the cartesian product set.
         */
        Iterator<List<ValueType>> cartesianProductForValuesIterator = null;

        /**
         * Constructor.
         */
        ExpandGridIterator() {
            // extract a list of all parameters and their values
            for (Map.Entry<KeyType, Set<ValueType>> e : paramGrid.entrySet()) {
                keys.add(e.getKey());
                values.add(e.getValue());
            }
            // create cartesian product of all parameters & an iterator for it
            List<Set<ValueType>> paramGridValues = new ArrayList<>();
            paramGridValues.addAll(values);
            cartesianProductForValues = Sets.cartesianProduct(paramGridValues);
            cartesianProductForValuesIterator = cartesianProductForValues.iterator();
        }

        @Override
        public boolean hasNext() {
            return cartesianProductForValuesIterator.hasNext();
        }

        @Override
        public Map<KeyType, ValueType> next() {
            LinkedHashMap<KeyType, ValueType> result = new LinkedHashMap<>();
            List<ValueType> nextCombination = cartesianProductForValuesIterator.next();
            for (int i = 0; i < nextCombination.size(); i++) {
                result.put(keys.get(i), nextCombination.get(i));
            }
            return result;
        }
    }

    @Override
    public void addValue(KeyType key, ValueType val) {
        Set<ValueType> setValues;
        if (!paramGrid.containsKey(key)) {
            setValues = new HashSet<>();
            paramGrid.put(key, setValues);
        } else {
            setValues = paramGrid.get(key);
        }
        setValues.add(val);
    }

    @Override
    public void addValues(KeyType key, Set<ValueType> values) {
        Set<ValueType> setValues;
        if (!paramGrid.containsKey(key)) {
            setValues = new HashSet<>();
            paramGrid.put(key, setValues);
        } else {
            setValues = paramGrid.get(key);
        }
        setValues.addAll(values);
    }

    @Override
    public void addValues(KeyType key, ValueType... values) {
        Set<ValueType> setValues;
        if (!paramGrid.containsKey(key)) {
            setValues = new HashSet<>();
            paramGrid.put(key, setValues);
        } else {
            setValues = paramGrid.get(key);
        }
        Collections.addAll(setValues, values);
    }

    @Override
    public void setValues(KeyType key, Set<ValueType> values) {
        Set<ValueType> setValues = new HashSet<>();
        setValues.addAll(values);
        paramGrid.put(key, setValues);
    }

    @Override
    public void setValues(KeyType key, ValueType... values) {
        Set<ValueType> setValues = new HashSet<>();
        Collections.addAll(setValues, values);
        paramGrid.put(key, setValues);
    }

    @Override
    public void merge(IParameterGrid<KeyType, ValueType> paramGrid) {
        for (KeyType key : paramGrid.keySet()) {
            setValues(key, paramGrid.getValues(key));
        }
    }

    @Override
    public Set<ValueType> getValues(KeyType param) {
        if (!paramGrid.containsKey(param) || paramGrid.get(param) == null) {
            return Collections.<ValueType>emptySet();
        }
        return Collections.unmodifiableSet(paramGrid.get(param));
    }

    @Override
    public boolean containsValues(KeyType param) {
        return paramGrid.get(param) != null && !paramGrid.get(param).isEmpty();
    }

    @Override
    public Set<KeyType> keySet() {
        return paramGrid.keySet();
    }

    @Override
    public Iterator<Map<KeyType, ValueType>> expandGridIterator() {
        return new ExpandGridIterator();
    }
}
