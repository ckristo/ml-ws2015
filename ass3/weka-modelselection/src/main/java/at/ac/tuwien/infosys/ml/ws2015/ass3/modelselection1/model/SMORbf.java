package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;
import weka.classifiers.functions.supportVector.Kernel;
import weka.classifiers.functions.supportVector.RBFKernel;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for Weka 'SMO' (Support Vector Machine) classifier with RBFKernel.
 */
@Model(names = {"svmRbf", "svmRBF"})
public class SMORbf extends SMO {

    /**
     * The parameter information (will be merged with parent class).
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // - kernel parameters
        // * -C :: The size of the cache (a prime number), 0 for full cache and -1 to turn it off. (default: 250007)
        paramInfo.put("K.C", Integer.class);
        // * -G :: The Gamma parameter. (default: 0.01)
        paramInfo.put("K.G", Double.class);
    }

    /**
     * The default parameter grid to use for model selection (will be merged with parent class).
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("K.G", 0.01, 0.1);
    }

    /**
     * Constructor.
     */
    public SMORbf() {
        super();
        wekaClassifier.setKernel(new RBFKernel());
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        Map<String, Class> paramInfo = new LinkedHashMap<>();
        paramInfo.putAll(SMO.paramInfo);
        paramInfo.putAll(SMORbf.paramInfo);
        return paramInfo;
    }

    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        TypedParameterGrid defaultGrid = new TypedParameterGrid();
        defaultGrid.merge(SMO.defaultGrid);
        defaultGrid.merge(SMORbf.defaultGrid);
        return defaultGrid;
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        // - try to use parent setParameterInternal first
        try {
            super.setParameterInternal(param, value);
        } catch (UnknownParameterException ex) {
            // - try to set parameter known in subclass
            switch (param) {
                case "K.C" : {
                    RBFKernel wekaRBFKernel = getWekaRBFKernel();
                    wekaRBFKernel.setCacheSize((Integer)value);
                    break;
                }
                case "K.G" : {
                    RBFKernel wekaRBFKernel = getWekaRBFKernel();
                    wekaRBFKernel.setGamma((Double)value);
                    break;
                }
                default :
                    throw new UnknownParameterException("Unknown parameter '" + param + "'");
            }
        }
    }

    /**
     * Returns the Kernel of the Weka classifier as RBFKernel.
     * @return the Kernel casted to a RBFKernel.
     */
    protected RBFKernel getWekaRBFKernel() {
        Kernel wekaKernel = wekaClassifier.getKernel();
        if (!(wekaKernel instanceof RBFKernel)) {
            throw new IllegalStateException("Weka classifier of SMORbf was configured with a non RBF kernel");
        }
        return (RBFKernel)wekaKernel;
    }
}
