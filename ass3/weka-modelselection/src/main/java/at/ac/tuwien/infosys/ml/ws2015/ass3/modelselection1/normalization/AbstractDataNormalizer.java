package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization;

import weka.core.Instances;

/**
 * Created by christoph on 29/01/16.
 */
public abstract class AbstractDataNormalizer implements IDataNormalizer {

    /**
     * Indicates whether the data normalizer was prepared yet.
     */
    private boolean prepared = false;

    @Override
    public void prepare(Instances dataSet) {
        prepareInternal(dataSet);
        prepared = true;
    }

    @Override
    public boolean isPrepared() {
        return prepared;
    }

    @Override
    public Instances normalize(Instances dataSet, boolean prepare) {
        if (!prepared && !prepare) {
            throw new IllegalStateException("Data normalizer has to be prepared first");
        }
        if (prepare) {
            prepare(dataSet);
        }
        return normalize(dataSet);
    }

    @Override
    public Instances normalize(Instances dataSet) {
        if (!prepared) {
            prepare(dataSet);
        }
        return normalizeInternal(new Instances(dataSet));
    }

    @Override
    public void reset() {
        prepared = false;
    }

    /**
     * Performs preparing of the data normalizer.
     * @param dataSet the data set used to prepare the data normalizer
     */
    protected abstract void prepareInternal(Instances dataSet);

    /**
     * Performs data normalization
     * @param dataSet the data set to normalize (a copy which can be modified)
     */
    protected abstract Instances normalizeInternal(Instances dataSet);
}
