package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization.IDataNormalizer;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization.NOPDataNormalizer;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.util.DataSetUtils;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.util.TypeConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.io.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

/**
 * Abstract model implementation.
 */
abstract class AbstractModel<ClassifierType extends Classifier> implements IModel {

    /**
     * The logger instance.
     */
    private static final Logger logger = LogManager.getLogger(AbstractModel.class);

    /**
     * The name used to instantiate the model (null = use class.getSimpleName()).
     */
    protected String name = null;

    /**
     * The Weka classifier instance.
     */
    protected ClassifierType wekaClassifier;

    /**
     * Indiates whether the Weka classifier was trained yet.
     */
    protected boolean trained = false;

    /**
     * The map of parameters currently set.
     */
    protected Map<String, Object> parameters = new LinkedHashMap<>();

    /**
     * The data normalizer to use for data normalization (null = no normalization).
     */
    protected IDataNormalizer dataNormalizer = new NOPDataNormalizer();

    /**
     * Constructor.
     *
     * @param wekaClassifier the Weka classifier
     */
    protected AbstractModel(ClassifierType wekaClassifier) {
        this.wekaClassifier = wekaClassifier;
    }

    @Override
    public String getName() {
        if (name == null) {
            return this.getClass().getSimpleName();
        }
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void useDataNormalizer(IDataNormalizer dataNormalizer) {
        if (dataNormalizer == null) {
            throw new IllegalArgumentException("'dataNormalizer' must not be null (use NOPDataNormalizer instead)");
        }
        this.dataNormalizer = dataNormalizer;
    }

    @Override
    public void train(Instances train) {
        try {
            train = dataNormalizer.normalize(train, true);
            wekaClassifier.buildClassifier(train);
            trained = true;
        } catch (Exception ex) {
            throw new ModelException("Could not train model", ex);
        }
    }

    @Override
    public Evaluation evaluate(Instances test) {
        if (!trained) {
            throw new IllegalStateException("Model was not trained yet.");
        }
        try {
            if (!dataNormalizer.isPrepared()) {
                throw new IllegalStateException("Model was trained but data normalizer is not prepared -- maybe the data normalizer changed after training?");
            }
            test = dataNormalizer.normalize(test, false);
            Evaluation eval = new Evaluation(test);
            eval.useNoPriors();
            eval.evaluateModel(wekaClassifier, test);
            return eval;
        } catch (Exception ex) {
            throw new ModelException("Could not evaluate model", ex);
        }
    }

    @Override
    public Evaluation trainAndEvaluate(Instances train, Instances test) {
        train(train);
        return evaluate(test);
    }

    @Override
    public Evaluation trainAndEvaluateKFoldCV(Instances data, int k) {
        return trainAndEvaluateKFoldCVInternal(data, k, null);
    }

    @Override
    public Evaluation trainAndEvaluateKFoldCV(Instances data, int k, int seed) {
        return trainAndEvaluateKFoldCVInternal(data, k, seed);
    }

    /**
     * Internal implementation for trainAndEvaluateKFoldCV() methods.
     *
     * @param data the train data.
     * @param k    the number of folds for k-fold CV
     * @param seed the random seed value
     * @return the evaluation result
     */
    protected Evaluation trainAndEvaluateKFoldCVInternal(Instances data, int k, Integer seed) {
        try {
            Random random = (seed != null) ? new Random(seed) : new Random();
            Evaluation eval = new Evaluation(data);
            // k-fold CV
            for (int i = 0; i < k; i++) {
                logger.info("Start fold {}...", i+1);

                // - create train fold
                Instances train = data.trainCV(k, i, random);
                train = dataNormalizer.normalize(train, true);
                eval.setPriors(train);

                // - train a copy of classifier
                Classifier wekaClassifierCopy = Classifier.makeCopy(wekaClassifier);
                wekaClassifierCopy.buildClassifier(train);

                // - evaluate fold
                Instances test = data.testCV(k, i);
                test = dataNormalizer.normalize(test, false);
                eval.evaluateModel(wekaClassifierCopy, test);

                logger.info("Finished fold {}...", i+1);
            }
            return eval;
        } catch (Exception ex) {
            throw new ModelException("Could not train and evaluate model using k-fold CV", ex);
        }
    }

    @Override
    public Evaluation trainAndEvaluateBootstrap(Instances data, int numIterations) {
        return trainAndEvaluateBootstrapInternal(data, numIterations, null);
    }

    @Override
    public Evaluation trainAndEvaluateBootstrap(Instances data, int numIterations, int seed) {
        return trainAndEvaluateBootstrapInternal(data, numIterations, seed);
    }

    /**
     * Internal implementation for trainAndEvaluateBootstrap() methods.
     *
     * @param data          the train data.
     * @param numIterations the number of bootstrapping iterations.
     * @param seed          the random seed value
     * @return the evaluation result
     */
    protected Evaluation trainAndEvaluateBootstrapInternal(Instances data, int numIterations, Integer seed) {
        try {
            Random random = (seed != null) ? new Random(seed) : new Random();
            Evaluation eval = new Evaluation(data);
            // - repeated bootstrapping
            for (int i = 0; i < numIterations; i++) {
                logger.info("Start iteration {}...", i+1);

                // - resample data set
                Instances[] resampled = DataSetUtils.resample(data, data.numInstances(), random);
                Instances train = resampled[0];
                Instances test = resampled[1];

                // - train a copy of classifier
                train = dataNormalizer.normalize(train, true);
                Classifier wekaClassifierCopy = Classifier.makeCopy(wekaClassifier);
                wekaClassifierCopy.buildClassifier(train);

                // - evaluate classifier
                test = dataNormalizer.normalize(test, false);
                eval.setPriors(train);
                eval.evaluateModel(wekaClassifierCopy, test);

                logger.info("Finished iteration {}...", i+1);
            }
            return eval;
        } catch (Exception ex) {
            throw new ModelException("Could not train and evaluate model using bootstrapping", ex);
        }
    }

    @Override
    public void setParameter(String param, Object value) {
        Map<String, Class> paramInfo = getParameterInformation();

        // check if parameter is known
        if (!paramInfo.containsKey(param)) {
            throw new IllegalArgumentException("Provided unknown parameter '" + param + "' for model");
        }

        // check if correct type was provided
        Class valueClazz = paramInfo.get(param);
        if (!value.getClass().isAssignableFrom(valueClazz)) {
            throw new IllegalArgumentException("Illegal value for parameter '" + param + "' -- provided type '"
                    + value.getClass().getSimpleName() + "', expected '" + valueClazz.getSimpleName() + "'");
        }

        parameters.put(param, value);

        try {
            setParameterInternal(param, value);
        } catch (Exception ex) {
            throw new ModelException("Could not set parameter '"+param+"' = '"+value+"'", ex);
        }
    }

    @Override
    public void setParameters(Map<String, Object> values) {
        for (Map.Entry<String, Object> elem : values.entrySet()) {
            setParameter(elem.getKey(), elem.getValue());
        }
    }

    @Override
    public void setParameterAsString(String param, String value) {
        setParameter(param, convertParameter(param, value));
    }

    @Override
    public void setParametersAsString(Map<String, String> values) {
        for (Map.Entry<String, String> elem : values.entrySet()) {
            String param = elem.getKey();
            String value = elem.getValue();
            setParameter(param, convertParameter(param, value));
        }
    }

    @Override
    public Map<String, Object> getParameters() {
        return Collections.unmodifiableMap(parameters);
    }

    @Override
    public Classifier getWekaClassifier() {
        return wekaClassifier;
    }

    @Override
    public void exportWekaClassifier(File file) throws IOException {
        try (ObjectOutputStream modelOutStream = new ObjectOutputStream(new FileOutputStream(file))) {
            modelOutStream.writeObject(wekaClassifier);
        }
    }

    @Override
    public void exportWekaClassifier(String filePath) throws IOException {
        exportWekaClassifier(new File(filePath));
    }

    @Override
    public void importWekaClassifier(File file) throws IOException {
        try (ObjectInputStream modelInStream = new ObjectInputStream(new FileInputStream(file))) {
            try {
                @SuppressWarnings("unchecked")
                ClassifierType classifier = (ClassifierType)modelInStream.readObject();
                wekaClassifier = classifier;
            } catch (ClassNotFoundException | ClassCastException ex) {
                throw new ModelException("Could not import classifier from file", ex);
            }
        }
    }

    @Override
    public void importWekaClassifier(String filePath) throws IOException {
        importWekaClassifier(new File(filePath));
    }

    /**
     * Convert a parameter given as string to expected type.
     *
     * @param param the parameter name
     * @param value the parameter value (given as string)
     * @return the converted parameter value.
     */
    protected Object convertParameter(String param, String value) {
        try {
            return TypeConverter.convertString(param, getValueTypeForParameter(param));
        } catch (IllegalArgumentException ex) {
            throw new ModelException("Could not convert string parameter to expected parameter value type", ex);
        }
    }

    /**
     * Returns the parameter value type for a given parameter name.
     *
     * @param param the parameter's name
     * @return the parameter type (as class object).
     */
    protected Class getValueTypeForParameter(String param) {
        return getParameterInformation().get(param);
    }

    /**
     * Internal parameter set method.
     *
     * @param param the name of the parameter to set.
     * @param value the value of the parameter.
     */
    protected abstract void setParameterInternal(String param, Object value) throws Exception;
}
