package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.TaskType;
import weka.classifiers.Evaluation;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Enumeration for available performance measures.
 */
public enum PerformanceMeasure {

    PRECISION {
        @Override
        double getValue(Evaluation eval) {
            return eval.weightedPrecision();
        }

        @Override
        TaskType[] getSupportedTaskTypes() {
            return new TaskType[] { TaskType.Classification};
        }

        @Override
        public int getComparisonDirection() {
            return 1;
        }

        @Override
        public String toString() {
            return "precision";
        }
    },
    RECALL {
        @Override
        double getValue(Evaluation eval) {
            return eval.weightedRecall();
        }

        @Override
        TaskType[] getSupportedTaskTypes() {
            return new TaskType[] { TaskType.Classification};
        }

        @Override
        public String toString() {
            return "recall";
        }

        @Override
        public int getComparisonDirection() {
            return 1;
        }
    },
    F1SCORE {
        @Override
        double getValue(Evaluation eval) {
            return eval.weightedFMeasure();
        }

        @Override
        TaskType[] getSupportedTaskTypes() {
            return new TaskType[] { TaskType.Classification};
        }

        @Override
        public String toString() {
            return "f1score";
        }

        @Override
        public int getComparisonDirection() {
            return 1;
        }
    },
    KAPPA {
        @Override
        double getValue(Evaluation eval) {
            return eval.kappa();
        }

        @Override
        TaskType[] getSupportedTaskTypes() {
            return new TaskType[] { TaskType.Classification};
        }

        @Override
        public String toString() {
            return "kappa";
        }

        @Override
        public int getComparisonDirection() {
            return 1;
        }
    },
    RMSE {
        @Override
        double getValue(Evaluation eval) {
            return eval.rootMeanSquaredError();
        }

        @Override
        TaskType[] getSupportedTaskTypes() {
            return new TaskType[] { TaskType.Classification, TaskType.Regression};
        }

        @Override
        public String toString() {
            return "RMSE";
        }

        @Override
        public int getComparisonDirection() {
            return -1;
        }
    },
    MAE {
        @Override
        double getValue(Evaluation eval) {
            return eval.meanAbsoluteError();
        }

        @Override
        TaskType[] getSupportedTaskTypes() {
            return new TaskType[] { TaskType.Classification, TaskType.Regression};
        }

        @Override
        public String toString() {
            return "MAE";
        }

        @Override
        public int getComparisonDirection() {
            return -1;
        }
    };

    /**
     * Returns the respective performance measure value from an evaluation instance.
     *
     * @param eval the evaluation instance.
     * @return the performance measure value.
     */
    abstract double getValue(Evaluation eval);

    /**
     * Returns the supported task type for a performance measure.
     *
     * @return the supported task types.
     */
    abstract TaskType[] getSupportedTaskTypes();

    /**
     * Checks whether a performance measure is supported for a given task type.
     * @param taskType the task type
     * @return true if the task type is supported, false otherwise.
     */
    public boolean isSupported(TaskType taskType) {
        for (PerformanceMeasure pm : PerformanceMeasure.values()) {
            if (Arrays.asList(pm.getSupportedTaskTypes()).contains(taskType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Indicates whether a bigger or smaller value is better.
     * @return 1 if bigger is better, -1 if smaller is better.
     */
    public abstract int getComparisonDirection();

    /**
     * Returns the default performance measure for a given task type.
     * @param taskType the task type
     * @return the default performance measure for the task type.
     */
    public static PerformanceMeasure getDefaultPerformanceMeasure(TaskType taskType) {
        switch (taskType) {
            case Classification:
                return KAPPA;
            case Regression:
                return RMSE;
            default:
                throw new IllegalArgumentException("No default performance measure for Task type '"+taskType+"' defined.");
        }
    }

    /**
     * Get all supported performance measures for a task type.
     * @param taskType the task type
     * @return the supported performance measures
     */
    public static PerformanceMeasure[] getSupportedPerformanceMeasures(TaskType taskType) {
        ArrayList<PerformanceMeasure> supportedPerformanceMeasures = new ArrayList<>();
        for (PerformanceMeasure pm : PerformanceMeasure.values()) {
            if (Arrays.asList(pm.getSupportedTaskTypes()).contains(taskType)) {
                supportedPerformanceMeasures.add(pm);
            }
        }
        return supportedPerformanceMeasures.toArray(new PerformanceMeasure[supportedPerformanceMeasures.size()]);
    }
}
