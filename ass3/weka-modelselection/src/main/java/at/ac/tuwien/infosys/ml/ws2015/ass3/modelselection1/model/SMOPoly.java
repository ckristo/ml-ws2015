package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;
import weka.classifiers.functions.supportVector.Kernel;
import weka.classifiers.functions.supportVector.NormalizedPolyKernel;
import weka.classifiers.functions.supportVector.PolyKernel;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for Weka 'SMO' (Support Vector Machine) classifier with PolyKernel.
 */
@Model(names = {"svmPoly"})
public class SMOPoly extends SMO {

    /**
     * Kernel functions for SMOPoly.
     */
    enum KernelFunction {

        Poly {
            @Override
            PolyKernel createWekaInstance() {
                return new PolyKernel();
            }
        },

        NormalizedPoly {
            @Override
            PolyKernel createWekaInstance() {
                return new NormalizedPolyKernel();
            }
        };

        /**
         * Creates and returns the Weka Kernel instance.
         * @return the Weka Kernel instance.
         */
        abstract PolyKernel createWekaInstance();

        /**
         * Returns the default kernel function.
         * @return the default kernel function.
         */
        static KernelFunction getDefault() {
            return Poly;
        }
    }

    /**
     * The parameter information (will be merged with parent class).
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -K :: The Kernel to use.
        paramInfo.put("K", String.class);

        // - kernel parameters
        // * -C :: The size of the cache (a prime number), 0 for full cache and -1 to turn it off. (default: 250007)
        paramInfo.put("K.C", Integer.class);
        // * -E :: The Exponent to use. (default: 1.0)
        paramInfo.put("K.E", Double.class);
        // * -L :: Use lower-order terms. (default: no)
        paramInfo.put("K.L", Boolean.class);
    }

    /**
     * The default parameter grid to use for model selection (will be merged with parent class).
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("K", KernelFunction.getDefault().name());
        defaultGrid.setValues("K.E", 1.0, 2.0, 3.0);
        defaultGrid.setValues("K.L", false, true);
    }

    /**
     * Constructor.
     */
    public SMOPoly() {
        super();
        wekaClassifier.setKernel(KernelFunction.getDefault().createWekaInstance());
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        Map<String, Class> paramInfo = new LinkedHashMap<>();
        paramInfo.putAll(SMO.paramInfo);
        paramInfo.putAll(SMOPoly.paramInfo);
        return paramInfo;
    }

    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        TypedParameterGrid defaultGrid = new TypedParameterGrid();
        defaultGrid.merge(SMO.defaultGrid);
        defaultGrid.merge(SMOPoly.defaultGrid);
        return defaultGrid;
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        // - try to use parent setParameterInternal first
        try {
            super.setParameterInternal(param, value);
        } catch (UnknownParameterException ex) {
            // - try to set parameter known in subclass
            switch (param) {
                case "K" : {
                    PolyKernel newWekaPolyKernel = KernelFunction.valueOf((String) value).createWekaInstance();
                    PolyKernel oldWekaPolyKernel = getWekaPolyKernel();
                    // - migrate prev. set options
                    newWekaPolyKernel.setCacheSize(oldWekaPolyKernel.getCacheSize());
                    newWekaPolyKernel.setExponent(oldWekaPolyKernel.getExponent());
                    newWekaPolyKernel.setUseLowerOrder(oldWekaPolyKernel.getUseLowerOrder());

                    wekaClassifier.setKernel(newWekaPolyKernel);
                    break;
                }
                case "K.C" : {
                    PolyKernel wekaPolyKernel = getWekaPolyKernel();
                    wekaPolyKernel.setCacheSize((Integer)value);
                    break;
                }
                case "K.E" : {
                    PolyKernel wekaPolyKernel = getWekaPolyKernel();
                    wekaPolyKernel.setExponent((Double)value);
                    break;
                }
                case "K.L" : {
                    PolyKernel wekaPolyKernel = getWekaPolyKernel();
                    wekaPolyKernel.setUseLowerOrder((Boolean)value);
                    break;
                }
                default :
                    throw new UnknownParameterException("Unknown parameter '" + param + "'");
            }
        }
    }

    /**
     * Returns the Kernel of the Weka classifier as PolyKernel
     * @return the Kernel casted to a PolyKernel.
     */
    protected PolyKernel getWekaPolyKernel() {
        Kernel wekaKernel = wekaClassifier.getKernel();
        if (!(wekaKernel instanceof PolyKernel)) {
            throw new IllegalStateException("Weka classifier of SMOPoly was configured with a non PolyKernel");
        }
        return (PolyKernel)wekaKernel;
    }
}
