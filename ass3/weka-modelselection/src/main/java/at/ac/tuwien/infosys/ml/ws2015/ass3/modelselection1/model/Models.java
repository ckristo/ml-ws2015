package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Static utility class for div. model functionaltiy.
 */
public class Models {

    /**
     * A cache to avoid multiple classpath scan's for a given name.
     */
    static HashMap<String, Class<IModel>> cache = new HashMap<>();

    /**
     * Instantiates a model instance by model names.
     *
     * @param name the model names.
     * @return the instantiated model
     * @throws IllegalModelNameException if the model names is unknown
     * @throws ModelException            if the model could not be instantiated.
     */
    public static IModel instantiateByName(String name) throws ModelException {
        // - get model class (either from cache or scan)
        Class<IModel> candidateClazz = cache.get(name);
        if (cache.get(name) == null) {
            candidateClazz = scanForModelByName(name);
            if (candidateClazz == null) {
                throw new ModelException("Could not find a suited model class for name '" + name + "'");
            }
            // - save result in cache
            cache.put(name, candidateClazz);
        }

        // - instantiate class
        try {
            IModel model = candidateClazz.newInstance();
            model.setName(name);
            return model;
        } catch (IllegalAccessException | InstantiationException ex) {
            throw new ModelException("Could not instantiate '" + name + "' model instance", ex);
        }
    }

    /**
     * Returns the default parameter grid of a model given by name.
     * @param name the model name
     * @return the default parameter grid.
     * @throws IllegalModelNameException if the model names is unknown
     * @throws ModelException            if the model could not be instantiated.
     */
    public static TypedParameterGrid getDefaultParameterGrid(String name) throws ModelException {
        return instantiateByName(name).getDefaultParameterGrid();
    }

    /**
     * Returns the parameter information for a model given by name.
     * @param name the model name
     * @return the parameter information.
     * @throws IllegalModelNameException if the model names is unknown
     * @throws ModelException            if the model could not be instantiated.
     */
    public static Map<String, Class> getParameterInformationByName(String name) throws ModelException {
        return instantiateByName(name).getParameterInformation();
    }

    /**
     * Checks if a model supports a given task type.
     * @param name the model name
     * @param taskType the task type to check
     * @return true if task type is supported, false otherwise.
     * @throws IllegalModelNameException if the model names is unknown
     * @throws ModelException            if the model could not be instantiated.
     */
    public static boolean checkForTaskTypeSupport(String name, TaskType taskType) throws ModelException {
        return instantiateByName(name).checkForTaskTypeSupport(taskType);
    }

    /**
     * Scans the models package for a model given by name.
     * @param name the model name.
     * @return the class
     */
    protected static Class<IModel> scanForModelByName(String name) {
        // - scan models package for candidates
        Reflections reflections = new Reflections(Models.class.getPackage().getName());
        Set<Class<?>> modelTypes = reflections.getTypesAnnotatedWith(Model.class);
        // - look for a candidate
        Class candidateClazz = null;
        boolean foundCandidate = false;
        for (Class<?> type : modelTypes) {
            // -- check names set by annotation first
            Model modelAnnotation = type.getAnnotation(Model.class);
            if (modelAnnotation == null) {
                continue;
            }
            for (String n : modelAnnotation.names()) {
                if (n.equals(name)) {
                    candidateClazz = type;
                    foundCandidate = true;
                    break;
                }
            }
            // - stop on first candidate
            if (foundCandidate) {
                break;
            }
            // -- check also class name
            if (type.getSimpleName().equals(name)) {
                candidateClazz = type;
            }
        }
        // - check if we found a suited candidate
        if (candidateClazz == null || !IModel.class.isAssignableFrom(candidateClazz)) {
            return null;
        } else {
            @SuppressWarnings("unchecked")
            Class<IModel> modelClazz = (Class<IModel>)candidateClazz;
            return modelClazz;
        }
    }
}
