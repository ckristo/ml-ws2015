package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.IModel;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.Models;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.TaskType;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.*;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization.FeatureNormalizationMethod;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.util.DataSetUtils;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.visualize.MisclassificationPlot;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.visualize.PerformanceMeasurePlot;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.visualize.ROCPlot;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import weka.core.Attribute;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Main().
 */
public class Main {

    /**
     * The logger instance.
     */
    private static final Logger logger = LogManager.getLogger();

    /**
     * Return code for indicating success.
     */
    private static final int EXIT_SUCCESS = 0;

    /**
     * Return code for indicating failure.
     */
    private static final int EXIT_FAILURE = 1;

    /**
     * Command line options.
     */
    private static Options options = new Options();
    static {
        options.addOption("c", "config", true, "The path to the model selection configuration file");
        options.addOption("o", "outDir", true, "The path to the directory where to write result files (defaults to current working directory)");
        options.addOption("t", "taskType", true, "The task type (classification, regression) -- default type will be set based on the class attribute type");
        options.addOption("i", "classIndex", true, "Class index of the data set to perform model selection on");
        options.addOption("s", "scaling", true, "Normalization method to use on the provided data set");
        options.addOption("p", "validationSplit", true, "Value used to split data into training+test and validation set (between 0 and 1)");
        options.addOption("m", "evalMethod", true, "Evaluation method to perform model selection with, one of:\n" +
                "random-split: split can be set by adding parameter -q\n" +
                "cv: k-fold cross validation, set fold by adding parameter -k\n" +
                "boot: bootstrapping, set number of iterations by adding parameter -n\n");
        options.addOption("q", true, "Split value when using random-split for evaluation");
        options.addOption("k", true, "Number of folds when using k-fold cross validation");
        options.addOption("n", true, "Number of iterations when using bootstrapping");
        options.addOption("r", true, "Seed to use for functions involving randomness (defaults to 1)");
        options.addOption("h", "help", false, "Prints this help information.");
    }

    /**
     * Command line parsing.
     */
    private static CommandLineParser parser;
    private static CommandLine cmd;

    /**
     * Prints usage.
     */
    private static void printUsage() {
        String cmdLineSyntax = "java -jar weka-modelselection.jar [-c <modelSelectionConfigFile>|-o <outputDir>|-t <taskType>|-ci <classIndex>|-s <scalingMethod>|-vp <validationSplitP>|-m <evalMethod>|-rp <randomSplitP>|-k <numFolds>|-n <bootIterations>|-r <randomSeed>|-h] <dataSetFile>";
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp(cmdLineSyntax, options);
    }

    /**
     * Loads data set from given location.
     *
     * @param filePath
     * @return the loaded data set
     * @throws IOException
     */
    private static Instances loadData(String filePath) throws IOException {
        Instances data;
        if (cmd.hasOption("i")) { // class index was provided
            data = DataSetUtils.loadArffFile(filePath, Integer.parseInt(cmd.getOptionValue("i")));
        } else {
            data = DataSetUtils.loadArffFile(filePath);
        }
        return data;
    }

    /**
     * Determines the task type for this run, by first checking if
     * the corresponding command line argument was provided. If not,
     * the function tries to determine the suitable type via the given
     * data set's class attribute. If this also fails, null is returned.
     *
     * @param data
     * @return the determined task type, or null if not possible
     * @throws IllegalArgumentException
     */
    private static TaskType getTaskType(Instances data) throws IllegalArgumentException {
        TaskType taskType = null;
        if (cmd.hasOption("t")) { // task type was provided
            try {
                String taskTypeStr = WordUtils.capitalize(cmd.getOptionValue("t"));
                taskType = TaskType.valueOf(taskTypeStr);
            } catch (IllegalArgumentException ex) {
                System.err.println("Illegal value provided for option 't'");
                printUsage();
                System.exit(EXIT_FAILURE);
            }
        } else {
            switch (data.classAttribute().type()) {
                case Attribute.NOMINAL :
                    taskType = TaskType.Classification;
                    break;
                case Attribute.NUMERIC :
                    taskType = TaskType.Regression;
                    break;
                default :
                    System.err.println("Cannot infer correct task type, please set option 't'");
                    System.exit(EXIT_FAILURE);
            }
        }
        return taskType;
    }

    /**
     * Returns the feature normalization method configured via the corresponding
     * command line parameter, or FeatureNormalizationMethod.NONE if no such parameter
     * was provided.
     *
     * @return the feature normalization method to be used for this run
     */
    private static FeatureNormalizationMethod getNormalizationMethod() {
        FeatureNormalizationMethod method = null;
        if(cmd.hasOption("s")) {
            try {
                String methodStr = WordUtils.capitalize(cmd.getOptionValue("s"));
                method = FeatureNormalizationMethod.valueOf(methodStr);
            } catch (IllegalArgumentException ex) {
                System.err.println("Illegal value provided for option 's'");
                printUsage();
                System.exit(EXIT_FAILURE);
            }
        } else {
            method = FeatureNormalizationMethod.NONE;
        }
        return method;
    }

    /**
     * Registers all configured models at the given model selector,
     * or the default ones based on the task type if none were provided.
     *
     * @param modelSelector
     * @param taskType
     * @throws IOException
     */
    private static void registerModels(ModelSelector modelSelector, TaskType taskType) throws IOException {
        if(cmd.hasOption("c")) { // config file was provided
            ModelSelectionConfig config = new ModelSelectionConfig(new File(cmd.getOptionValue("c")));
            String[] models = config.getModels();
            for(String model : models) {
                StringParameterGrid configGrid = null;
                HashMap<String, String[]> params = config.getParameters(model);
                if (params != null && !params.isEmpty()) {
                    configGrid = new StringParameterGrid();
                    for (Map.Entry<String, String[]> param : params.entrySet()) {
                        configGrid.setValues(param.getKey(), param.getValue());
                    }
                    // - merge config grid with default grid (config parameters will overwrite default parameters)
                    TypedParameterGrid defaultGrid = Models.getDefaultParameterGrid(model);
                    defaultGrid.merge(configGrid, Models.getParameterInformationByName(model));
                    modelSelector.registerModel(model, configGrid);
                } else {
                    modelSelector.registerModel(model);
                }
            }
        } else {
            // register some default models
            if (taskType == TaskType.Classification) {
                modelSelector.registerModel("nb");
                modelSelector.registerModel("knn");
                modelSelector.registerModel("svmPoly");
            } else if (taskType == TaskType.Regression) {
                modelSelector.registerModel("linreg");
                modelSelector.registerModel("cart");
            } else {
                System.err.println("No default models for task type '" + taskType + "' defined, please specify a model selection config file via option 'c'");
                System.exit(EXIT_FAILURE);
            }
        }
    }

    /**
     * Trains all registered models with either the evaluation method provided
     * with the corresponding command line parameter, or with a 10-fold cv if no such parameter
     * is given, and returns the collected results.
     *
     * @param data
     * @param modelSelector
     * @return results for each parameter configuration for each registered model
     */
    private static ModelSelectionResults trainAndEvaluate(Instances data, ModelSelector modelSelector) {
        if(cmd.hasOption("m")) { // evaluation method was provided
            switch(cmd.getOptionValue("m")) {
                case "random-split":
                    if(cmd.hasOption("q")) {
                        return modelSelector.trainAndEvaluateRandomSplit(data, Float.parseFloat(cmd.getOptionValue("q")));
                    } else {
                        logger.warn("Using random-split without explicitly set split -- using 80/20% split");
                        return modelSelector.trainAndEvaluateRandomSplit(data, 0.8f);
                    }
                case "cv":
                    if(cmd.hasOption("k")) {
                        return modelSelector.trainAndEvaluateKFoldCV(data, Integer.parseInt(cmd.getOptionValue("k")));
                    } else {
                        logger.warn("Using k-fold cv without explicitly set k -- using 10 folds");
                        return modelSelector.trainAndEvaluateKFoldCV(data, 10);
                    }
                case "boot":
                    if(cmd.hasOption("n")) {
                        return modelSelector.trainAndEvaluateBootstrapping(data, Integer.parseInt(cmd.getOptionValue("n")));
                    } else {
                        logger.warn("Using bootstrapping without explicitly set n -- using 25 iterations");
                        return modelSelector.trainAndEvaluateBootstrapping(data, 25);
                    }
                default:
                    System.err.println("Illegal value provided for option 'm'");
                    printUsage();
                    System.exit(EXIT_FAILURE);
                    /* NOTREACHED */
                    return null;
            }
        } else { // use 10-fold cross validation by default
            return modelSelector.trainAndEvaluateKFoldCV(data, 10);
        }
    }

    public static void main(String[] args) {
        // parse command line arguments
        parser = new DefaultParser();
        cmd = null;
        try {
            cmd = parser.parse(options, args);
            if(cmd.hasOption("h")) {
                printUsage();
                System.exit(EXIT_SUCCESS);
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            printUsage();
            System.exit(EXIT_FAILURE);
        }

        // check remaining arguments
        String[] remainingArgs = cmd.getArgs();
        if (remainingArgs.length < 1) {
            System.err.println("Please specify required argument <dataSetFile>");
            printUsage();
            System.exit(EXIT_FAILURE);
        }

        // load data set
        Instances data = null;
        try {
            data = loadData(remainingArgs[0]);
        } catch (IOException ex) {
            logger.error("Could not load data set", ex);
            System.exit(1);
        }

        // parse numeric option values
        float vp = Float.NaN;
        int seed = -1;
        try {
            vp = Float.parseFloat(cmd.getOptionValue("p", "0.8"));
        } catch (NumberFormatException ex) {
            System.err.println("Illegal value provided for option 'vp'");
            printUsage();
            System.exit(EXIT_FAILURE);
        }
        try {
            seed = Integer.parseInt(cmd.getOptionValue("r", "1"));
        } catch (NumberFormatException ex) {
            System.err.println("Illegal value provided for option 'r'");
            printUsage();
            System.exit(EXIT_FAILURE);
        }

        Instances[] instances = DataSetUtils.randomizedSplit(data, vp, seed);
        data = instances[0];
        Instances validation = instances[1];

        // determine task type
        TaskType taskType = getTaskType(data);

        // instantiate model selector
        ModelSelector modelSelector = new ModelSelector(taskType);

        // determine feature normalization method
        FeatureNormalizationMethod method = getNormalizationMethod();
        modelSelector.setFeatureNormalizationMethod(method);
        modelSelector.setSeed(seed);

        // register models
        try {
            registerModels(modelSelector, taskType);
        } catch (IOException ex) {
            System.err.println("Could not read <modelSelectionConfigFile>, error = " + ex.getMessage());
            System.exit(EXIT_FAILURE);
        }

        // train and evaluate chosen models
        ModelSelectionResults results = trainAndEvaluate(data, modelSelector);
        assert(results != null);
        ModelEvaluation bestResult = results.getBestEvaluation();

        System.out.println("Finished model selection, best model was ::");
        System.out.println(bestResult);

        // train final model
        IModel model = Models.instantiateByName(bestResult.getModelName());
        model.setParameters(bestResult.getParameters());
        model.train(data);

        // evaluate final model
        ModelEvaluation finalResult = new ModelEvaluation(taskType, bestResult.getModelName(),
                bestResult.getParameters(), model.evaluate(validation), data);

        System.out.println("Result of model validation ::");
        System.out.println(finalResult);

        // get output directory
        File outputDir = new File( cmd.getOptionValue("o", "."));
        // create output dir if it not exists
        if (!outputDir.exists() && !outputDir.mkdirs()) {
            System.err.println("Could not create output directory");
            System.exit(EXIT_FAILURE);
        } else if (!outputDir.isDirectory()) {
            System.err.println("Value of option 'o' must be a directory");
            System.exit(EXIT_FAILURE);
        }

        // write results to ARFF file
        try {
            File resultsArffFile = new File(outputDir, "results.arff");
            logger.info("Write model selection results as ARFF file to '{}'", resultsArffFile.getPath());
            DataSetUtils.writeArffFile(results.toInstances(), resultsArffFile);
        } catch (IOException ex) {
            logger.warn("Could not write results to ARFF file", ex);
        }

        // write trained final model to file
        try {
            File trainedFinalModelFile = new File(outputDir, model.getName()+"-final.classifier");
            logger.info("Write trained model to '{}'", trainedFinalModelFile.getPath());
            model.exportWekaClassifier(trainedFinalModelFile);
        } catch (IOException ex) {
            logger.warn("Could not export trained model to file", ex);
        }

        // write plots
        try {
            PerformanceMeasurePlot performancePlot = new PerformanceMeasurePlot();
            performancePlot.setPerformanceMeasure(PerformanceMeasure.RMSE);
            performancePlot.saveToFile(new File(outputDir, "plot-rmse.png"), 1024, 768, results);

            if(taskType == TaskType.Classification) {
                performancePlot.setPerformanceMeasure(PerformanceMeasure.KAPPA);
                performancePlot.saveToFile(new File(outputDir, "plot-kappa.png"), 1024, 768, results);

                MisclassificationPlot classPlot = new MisclassificationPlot();
                classPlot.saveToFile(new File(outputDir, "plot-classification.png"), 1024, 768, results);

                ROCPlot rocPlot = new ROCPlot();
                rocPlot.saveToFile(new File(outputDir, "plot-roc.png"), 1024, 768, results);
            }
        } catch (Exception ex) {
            logger.warn("Could not plot image files", ex);
        }
    }
}
