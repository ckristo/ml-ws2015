package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for Weka NaiveBayes classifier.
 */
@Model(names = {"nb"})
public class NaiveBayes extends AbstractModel<weka.classifiers.bayes.NaiveBayes> {

    /**
     * The parameter information.
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -K :: Use kernel density estimator rather than normal distribution for numeric attributes
        paramInfo.put("K", Boolean.class);

        // * -D :: Use supervised discretization to process numeric attributes
        paramInfo.put("D", Boolean.class);
    }

    /**
     * The default parameter grid to use for model selection.
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("K", false, true);
        defaultGrid.setValues("D", false, true);
    }

    /**
     * Constructor.
     */
    public NaiveBayes() {
        super(new weka.classifiers.bayes.NaiveBayes());
    }

    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        return defaultGrid;
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        return paramInfo;
    }

    @Override
    public boolean checkForTaskTypeSupport(TaskType taskType) {
        return (taskType == TaskType.Classification);
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        switch (param) {
            case "K":
                wekaClassifier.setUseKernelEstimator((Boolean)value);
                break;
            case "D":
                wekaClassifier.setUseSupervisedDiscretization((Boolean)value);
                break;
            default:
                throw new UnknownParameterException("Unknown parameter '" + param + "'");
        }
    }
}
