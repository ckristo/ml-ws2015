package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization;

/**
 * Enumeration for normalization methods.
 */
public enum FeatureNormalizationMethod {

    /**
     * Min/Max Scaling
     */
    MIN_MAX {
        @Override
        public IDataNormalizer createDataNormalizer() {
            return new MinMaxDataNormalizer();
        }
    },

    /**
     * Z-Score Standardization
     */
    Z_SCORE {
        @Override
        public IDataNormalizer createDataNormalizer() {
            return new ZScoreDataNormalizer();
        }
    },

    /**
     * Unit Length Scaling
     */
    UNIT_LENGTH {
        @Override
        public IDataNormalizer createDataNormalizer() {
            return new UnitLengthDataNormalizer();
        }
    },

    NONE {
        @Override
        public IDataNormalizer createDataNormalizer() {
            return new NOPDataNormalizer();
        }
    };

    /**
     * Returns a new data normalizer instance for a normalization method.
     *
     * @return a new data normalizer instance for a normalization method.
     */
    public abstract IDataNormalizer createDataNormalizer();
}
