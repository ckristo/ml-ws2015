package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.visualize;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.ModelEvaluation;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.ModelSelectionResults;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.PerformanceMeasure;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import weka.core.Instances;

import java.util.Map;
import java.util.Set;

public class MisclassificationPlot extends AbstractPlot {

    @Override
    public JFreeChart createChart(ModelSelectionResults results) {
        JFreeChart chart = null;
        DefaultCategoryDataset chartData = new DefaultCategoryDataset();

        for(Map.Entry<String, Set<ModelEvaluation>> result : results.getEvaluations().entrySet()) {
            Set<ModelEvaluation> evals = result.getValue();

            int i = 0;
            for(ModelEvaluation eval : evals) {
                String params = eval.getParameters().toString();

                double numCorrect   = eval.getEvaluation().correct();
                double numIncorrect = eval.getEvaluation().incorrect();

                chartData.addValue(numCorrect,
                        "correctly classified",
                        eval.getModelName() + i + params);
                chartData.addValue(numIncorrect,
                        "misclassified",
                        eval.getModelName() + i + params);

                i++;
            }
        }

        chart = ChartFactory.createBarChart(
                "Number of correctly/incorrectly classified instances per model", "Model", "Number of correctly/incorrectly classified instances",
                chartData,
                PlotOrientation.VERTICAL,
                true, // legend
                true, // tooltips
                false // urls
        );

        // rotate labels
        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.UP_45);

        return chart;
    }
}
