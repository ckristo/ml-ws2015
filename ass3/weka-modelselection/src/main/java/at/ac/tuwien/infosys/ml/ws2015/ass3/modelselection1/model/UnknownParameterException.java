package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

/**
 * Exception thrown by classes of the models package when a user tries to set an unknown parameter.
 */
public class UnknownParameterException extends ModelException {

    public UnknownParameterException() {
        super();
    }

    public UnknownParameterException(String msg) {
        super(msg);
    }

    public UnknownParameterException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
