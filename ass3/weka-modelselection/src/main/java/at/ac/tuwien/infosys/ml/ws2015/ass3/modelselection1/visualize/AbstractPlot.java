package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.visualize;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.ModelSelectionResults;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;

public abstract class AbstractPlot {

    public void saveToFile(String filePath, int width, int height, ModelSelectionResults results) throws IOException {
        saveToFile(new File(filePath), width, height, results);
    }

    public void saveToFile(File file, int width, int height, ModelSelectionResults results) throws IOException {
        JFreeChart chart = this.createChart(results);
        ChartUtilities.saveChartAsPNG(file, chart, width, height);
    }

    public abstract JFreeChart createChart(ModelSelectionResults results);
}
