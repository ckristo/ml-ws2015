package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.TaskType;
import weka.core.Instances;

import java.util.*;

/**
 * A class that represents a model selection result.
 */
public class ModelSelectionResults {

    /**
     * The string used to set the relation name of the instances created by toInstances().
     */
    private static final String INSTANCES_RELATION_NAME = "model_selection_results";

    /**
     * The task type.
     */
    final TaskType taskType;

    /**
     * Stores all evaluation results by model.
     */
    private HashMap<String, Set<ModelEvaluation>> allEvaluations = new HashMap<>();

    /**
     * Stores the best evaluation result per model.
     */
    private HashMap<String, ModelEvaluation> bestEvaluations = new HashMap<>();

    /**
     * Constructor.
     *
     * @param taskType the task type
     */
    public ModelSelectionResults(TaskType taskType) {
        this.taskType = taskType;
    }

    /**
     * Adds an evaluation result to the model selection result.
     * @param eval the evaluation result.
     */
    public void addEvaluation(ModelEvaluation eval) {
        if (!eval.getTaskType().equals(taskType)) {
            throw new IllegalArgumentException("Provided model evaluation result's taskType differs, make sure that the taskTypes match");
        }
        String modelName = eval.getModelName();
        Set<ModelEvaluation> evaluations = allEvaluations.get(modelName);
        ModelEvaluation bestEval = bestEvaluations.get(modelName);
        if (evaluations == null) {
            evaluations = new LinkedHashSet<>();
            allEvaluations.put(modelName, evaluations);
        }
        if (bestEval == null || bestEval.compareTo(eval) < 0) {
            bestEvaluations.put(modelName, eval);
        }
        // TODO: what to do when performance metric value is equal, check others?
        evaluations.add(eval);
    }

    /**
     * Returns evaluation results for a model.
     * @return an unmodifiable set of evaluation results.
     */
    public Set<ModelEvaluation> getEvaluations(String modelName) {
        Set<ModelEvaluation> evaluations = allEvaluations.get(modelName);
        if (evaluations == null) {
            return null;
        } else {
            return Collections.unmodifiableSet(allEvaluations.get(modelName));
        }
    }

    /**
     * Returns all evaluation results as map.
     * The key is the model name, the values are the evaluation results.
     * NOTE: both the map and the set of evaluation results are unmodifiable!
     * @return an unmodifiable map with evaluation results per model.
     */
    public Map<String, Set<ModelEvaluation>> getEvaluations() {
        Map<String, Set<ModelEvaluation>> allEvaluationsToReturn = new HashMap<>();
        for (Map.Entry<String, Set<ModelEvaluation>> entry : allEvaluations.entrySet()) {
            allEvaluationsToReturn.put(entry.getKey(), Collections.unmodifiableSet(entry.getValue()));
        }
        return Collections.unmodifiableMap(allEvaluationsToReturn);
    }

    /**
     * Returns the best evaluation result for a model given by name.
     * @param modelName the model name.
     * @return the best evaluation result for the model or null.
     */
    public ModelEvaluation getBestEvaluation(String modelName) {
        return bestEvaluations.get(modelName);
    }

    /**
     * Returns the overall best evaluation result.
     * @return the best evaluation result over all models or null.
     */
    public ModelEvaluation getBestEvaluation() {
        ModelEvaluation bestEval = null;
        for (Map.Entry<String, ModelEvaluation> entry : bestEvaluations.entrySet()) {
            ModelEvaluation eval = entry.getValue();
            if (bestEval == null || bestEval.compareTo(eval) < 0) {
                bestEval = eval;
            }
            // TODO: what to do when performance metric value is equal, check others?
        }
        return bestEval;
    }

    /**
     * Returns a Weka Instances object that contains the model selection results for a given model.
     * @param modelName the model name
     * @return the Weka Instances object.
     */
    public Instances toInstances(String modelName) {
        Set<ModelEvaluation> evaluations = allEvaluations.get(modelName);
        Instances instances = new Instances(INSTANCES_RELATION_NAME, ModelEvaluation.instanceAttrInfo(taskType), evaluations.size());
        for (ModelEvaluation eval : evaluations) {
            instances.add(eval.toInstance(instances));
        }
        return instances;
    }

    /**
     * Returns a Weka Instances object that contains the model selection results for all model.
     * @return the Weka Instances object.
     */
    public Instances toInstances() {
        // - calculate overall size
        int overallSize = 0;
        for (Set<ModelEvaluation> evaluations : allEvaluations.values()) {
            overallSize += evaluations.size();
        }
        // - instantiate new instances object
        Instances instances = new Instances(INSTANCES_RELATION_NAME, ModelEvaluation.instanceAttrInfo(taskType), overallSize);
        // - fill in instances
        for (Set<ModelEvaluation> evaluations : allEvaluations.values()) {
            for (ModelEvaluation eval : evaluations) {
                instances.add(eval.toInstance(instances));
            }
        }
        return instances;
    }
}
