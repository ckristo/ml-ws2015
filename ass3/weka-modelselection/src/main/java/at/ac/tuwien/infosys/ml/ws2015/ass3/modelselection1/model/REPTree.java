package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for Weka REPTree (decision/regression tree).
 */
@Model(names = {"reptree"})
public class REPTree extends AbstractModel<weka.classifiers.trees.REPTree> {

    /**
     * The parameter information.
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -M :: Set minimum number of instances per leaf (default 2).
        paramInfo.put("M", Double.class);

        // * -V :: Set minimum numeric class variance proportion of train variance for split (default 1e-3).
        paramInfo.put("V", Double.class);

        // * -N :: Number of folds for reduced error pruning (default 3).
        paramInfo.put("N", Integer.class);

        // * -S :: Seed for random data shuffling (default 1).
        paramInfo.put("S", Integer.class);

        // * -P :: No pruning.
        paramInfo.put("P", Boolean.class);

        // * -L :: Maximum tree depth (default -1, no maximum)
        paramInfo.put("L", Integer.class);
    }

    /**
     * The default parameter grid to use for model selection.
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("M", 1, 2, 3, 5);
        defaultGrid.setValues("V", 1e-4, 1e-3, 1e-2, 1e-1);
        defaultGrid.setValues("N", 3, 5);
        defaultGrid.setValues("S", 1);
        defaultGrid.setValues("P", false, true);
        defaultGrid.setValues("L", -1, 100, 50, 15, 10, 5);
    }

    /**
     * Constructor.
     */
    public REPTree() {
        super(new weka.classifiers.trees.REPTree());
    }

    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        return defaultGrid;
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        return paramInfo;
    }

    @Override
    public boolean checkForTaskTypeSupport(TaskType taskType) {
        return (taskType == TaskType.Classification || taskType == TaskType.Regression);
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        switch (param) {
            case "M":
                wekaClassifier.setMinNum((Double)value);
                break;
            case "V":
                wekaClassifier.setMinVarianceProp((Double)value);
                break;
            case "N":
                wekaClassifier.setNumFolds((Integer)value);
                break;
            case "S":
                wekaClassifier.setSeed((Integer)value);
                break;
            case "P":
                wekaClassifier.setNoPruning((Boolean)value);
                break;
            case "L":
                wekaClassifier.setMaxDepth((Integer)value);
                break;
            default:
                throw new UnknownParameterException("Unknown parameter '" + param + "'");
        }
    }
}
