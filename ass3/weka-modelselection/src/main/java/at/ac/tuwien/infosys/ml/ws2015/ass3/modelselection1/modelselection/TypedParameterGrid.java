package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import java.util.Map;

/**
 * Parameter grid that contains classifier parameters to use for model selection.
 */
public class TypedParameterGrid extends AbstractParameterGrid<String, Object> {

    /**
     * Merges a string parameter grid with a typed parameter grid.
     *
     * @param stringGrid the string parameter grid to merge.
     * @param typeInfo the type information to convert parameter values.
     */
    public void merge(StringParameterGrid stringGrid, Map<String, Class> typeInfo) {
        for (String key : stringGrid.keySet()) {
            setValues(key, stringGrid.getTypedValues(key, typeInfo));
        }
    }
}
