package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.util.TypeConverter;

import java.util.Map;
import java.util.Set;

/**
 * A parameter grid where keys and values are strings.
 */
public class StringParameterGrid extends AbstractParameterGrid<String, String> {

    /**
     * Converts a string parameter grid to a typed parameter grid.
     * @param typeInfo the parameter type information.
     * @return the converted parameter grid.
     */
    public TypedParameterGrid convertToTypedParameterGrid(Map<String, Class> typeInfo) {
        TypedParameterGrid typedGrid = new TypedParameterGrid();
        for (Map.Entry<String, Class> entry : typeInfo.entrySet()) {
            String param = entry.getKey();
            Class type = entry.getValue();
            if (containsValues(param)) {
                typedGrid.addValues(param, TypeConverter.convertStringSet(getValues(param), type));
            }
        }
        return typedGrid;
    }

    /**
     * Converts the parameter values of a parameter and returns them.
     *
     * @param param the parameter to return its values.
     * @param typeInfo the type information used to convert the string grid values.
     * @return the typed parameter values.
     */
    public Set<Object> getTypedValues(String param, Map<String, Class> typeInfo) {
        Class type = typeInfo.get(param);
        if (type == null) {
            throw new IllegalArgumentException("Not type information present for param '" + param +"'");
        }
        return TypeConverter.convertStringSet(getValues(param), type);
    }
}
