package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization;

import weka.core.Instances;

/**
 * Data Normalizer implementation that does not perform any operation.
 */
public class NOPDataNormalizer implements IDataNormalizer {

    @Override
    public void prepare(Instances dataSet) {}

    @Override
    public boolean isPrepared() {
        return true;
    }

    @Override
    public Instances normalize(Instances dataSet) {
        return dataSet;
    }

    @Override
    public Instances normalize(Instances dataSet, boolean prepare) {
        return dataSet;
    }

    @Override
    public void reset() {}
}
