package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for Weka RandomForest.
 */
@Model(names = {"rf"})
public class RandomForest extends AbstractModel<weka.classifiers.trees.RandomForest> {

    /**
     * The parameter information.
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -I :: Number of trees to build.
        paramInfo.put("I", Integer.class);

        // * -K :: Number of features to consider (<0 = int(log_2(#predictors+1)).
        paramInfo.put("K", Integer.class);

        // * -S :: Seed for random number generator. (default 1)
        paramInfo.put("S", Integer.class);

        // * -depth :: The maximum depth of the trees, 0 for unlimited. (default 0)
        paramInfo.put("depth", Integer.class);
    }

    /**
     * The default parameter grid to use for model selection.
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("I", 5, 10, 50, 100, 250, 500);
        defaultGrid.setValues("K", -1, 10, 7, 5, 3, 1);
        defaultGrid.setValues("S", 1);
        defaultGrid.setValues("depth", 0, 5, 10, 15, 50, 100);
    }

    /**
     * Constructor.
     */
    public RandomForest() {
        super(new weka.classifiers.trees.RandomForest());
    }

    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        return defaultGrid;
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        return paramInfo;
    }

    @Override
    public boolean checkForTaskTypeSupport(TaskType taskType) {
        return (taskType == TaskType.Classification);
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        switch (param) {
            case "I":
                wekaClassifier.setNumTrees((Integer)value);
                break;
            case "K":
                wekaClassifier.setNumFeatures((Integer)value);
                break;
            case "S":
                wekaClassifier.setSeed((Integer)value);
                break;
            case "depth":
                wekaClassifier.setMaxDepth((Integer)value);
                break;
            default:
                throw new UnknownParameterException("Unknown parameter '" + param + "'");
        }
    }
}
