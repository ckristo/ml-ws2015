package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * DataNormalizer implementation for unit length scaling.
 */
public class UnitLengthDataNormalizer extends AbstractDataNormalizer {

    /**
     * The length value.
     */
    int length;

    @Override
    public void prepareInternal(Instances dataSet) {
        length = dataSet.numAttributes();
    }

    @Override
    public Instances normalizeInternal(Instances dataSet) {
        // - check if number of attributes match the data set used to prepare normalizer.
        int numAttrs = dataSet.numAttributes();
        if (numAttrs != length) {
            throw new IllegalArgumentException("Attribute number of provided data set differs from attribute number of data set used for preparation.");
        }

        // - normalize values
        for (int i = 0; i < dataSet.numInstances(); i++) {
            Instance instance = dataSet.instance(i);
            for (int j = 0; j < numAttrs; j++) {
                // - skip class attribute
                if (instance.classIndex() == j) {
                    continue;
                }
                // - skip non-numeric attributes
                if (instance.attribute(j).type() != Attribute.NUMERIC) {
                    continue;
                }
                // - scale to unit length
                instance.setValue(j, instance.value(j) / length);
            }
        }

        return dataSet;
    }
}
