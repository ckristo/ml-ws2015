package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;
import weka.core.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for Weka 'SMO' (Support Vector Machine) classifier.
 */
abstract class SMO extends AbstractModel<weka.classifiers.functions.SMO> {

    /**
     * The parameter information.
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -C :: The complexity constant C. (default 1)
        paramInfo.put("C", Double.class);
        // * -L :: The tolerance parameter. (default 1.0e-3)
        paramInfo.put("L", Double.class);
        // * -P :: The epsilon for round-off error. (default 1.0e-12)
        paramInfo.put("P", Double.class);
        // * -W :: The random number seed. (default 1)
        paramInfo.put("W", Integer.class);
    }

    /**
     * The default parameter grid to use for model selection.
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("C", 1e-2, 1e-1, 1e0, 1e1, 1e2);
        defaultGrid.setValues("L", 1e-3);
        defaultGrid.setValues("P", 1e-12);
    }

    /**
     * Constructor.
     */
    public SMO() {
        super(new weka.classifiers.functions.SMO());
        // - disable normalization, our model selection frameworks takes care of it.
        wekaClassifier.setFilterType(new SelectedTag(weka.classifiers.functions.SMO.FILTER_NONE,
                weka.classifiers.functions.SMO.TAGS_FILTER));
    }

    @Override
    public boolean checkForTaskTypeSupport(TaskType taskType) {
        return (taskType == TaskType.Classification);
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        switch (param) {
            case "C" :
                wekaClassifier.setC((Double)value);
                break;
            case "L" :
                wekaClassifier.setToleranceParameter((Double)value);
                break;
            case "P" :
                wekaClassifier.setEpsilon((Double)value);
                break;
            case "W" :
                wekaClassifier.setRandomSeed((Integer)value);
                break;
            default :
                throw new UnknownParameterException("Unknown parameter '" + param + "'");
        }
    }
}
