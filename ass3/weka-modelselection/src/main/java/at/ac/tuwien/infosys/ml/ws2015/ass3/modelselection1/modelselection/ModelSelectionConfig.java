package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by Alex on 25.01.2016.
 */
public class ModelSelectionConfig {

    private static final Logger logger = LogManager.getLogger(ModelSelectionConfig.class);

    private static final String CONFIG_KEY_MODELS = "models";
    private static final String CONFIG_KEY_MODELS_PARAMS = "parameters";

    private final Properties properties;

    public ModelSelectionConfig(File configFile) throws IOException {
        InputStream inputStream = new FileInputStream(configFile);
        if (inputStream == null) {
            throw new IOException("Could not load config. file");
        }
        this.properties = new Properties();
        this.properties.load(inputStream);
    }

    public boolean hasProperty(String key) {
        return properties.containsKey(key);
    }

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public String[] getModels() {
        String models = this.getProperty(CONFIG_KEY_MODELS);
        if(models == null || models.length() == 0) return null;
        return models.split(",");
    }

    public HashMap<String, String[]> getParameters(String model) {
        Enumeration keys = this.properties.propertyNames();
        HashMap<String, String[]> parameters = new HashMap<>();

        while(keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            if(key.contains(model)) {
                parameters.put(
                        // TODO: there HAS to be a more elegant way than this
                        // use everything after "model.<model_name>.parameters." as key, i.e., the parameter name
                        key.substring(key.indexOf(CONFIG_KEY_MODELS_PARAMS) + CONFIG_KEY_MODELS_PARAMS.length() + 1),

                        // remove all whitespaces from value string, explode string into array
                        this.getProperty(key).replaceAll("\\s+", "").split(",")
                );
            }
        }
        if(parameters == null || parameters.size() == 0) return null;
        return parameters;
    }
}
