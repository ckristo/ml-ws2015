package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import java.util.Arrays;

/**
 * DataNormalizer implementation for Min/Max scaling.
 */
public class MinMaxDataNormalizer extends AbstractDataNormalizer {

    /**
     * Contains the min values per attribute.
     * (non-numeric attributes are NaN)
     */
    double[] min;

    /**
     * Contains the max values per attribute.
     * (non-numeric attributes are NaN)
     */
    double[] max;

    @Override
    protected void prepareInternal(Instances dataSet) {
        int numAttrs = dataSet.numAttributes();

        // - initialize min, max arrays
        min = new double[numAttrs];
        Arrays.fill(min, Double.POSITIVE_INFINITY);
        max = new double[numAttrs];
        Arrays.fill(max, Double.NEGATIVE_INFINITY);

        // - normalize values
        for (int i = 0; i < dataSet.numInstances(); i++) {
            Instance instance = dataSet.instance(i);
            for (int j = 0; j < numAttrs; j++) {
                // - skip class attribute
                if (instance.classIndex() == j) {
                    continue;
                }
                if (instance.attribute(j).type() != Attribute.NUMERIC) {
                    min[j] = max[j] = Double.NaN;
                } else {
                    double attrVal = instance.value(j);
                    if (min[j] > attrVal) {
                        min[j] = attrVal;
                    }
                    if (max[j] < attrVal) {
                        max[j] = attrVal;
                    }
                }
            }
        }
    }

    @Override
    public Instances normalizeInternal(Instances dataSet) {
        assert(min.length == max.length);

        // - check if number of attributes match the data set used to prepare normalizer.
        int numAttrs = dataSet.numAttributes();
        if (numAttrs != min.length) {
            throw new IllegalArgumentException("Attribute number of provided data set differs from attribute number of data set used for preparation.");
        }

        // - find min and max attribute values per attribute
        for (int i = 0; i < dataSet.numInstances(); i++) {
            Instance instance = dataSet.instance(i);
            for (int j = 0; j < numAttrs; j++) {
                // - skip class attribute
                if (instance.classIndex() == j) {
                    continue;
                }
                // - skip non-numeric attributes
                if (instance.attribute(j).type() != Attribute.NUMERIC) {
                    continue;
                }
                // - scale to range [0, 1]
                instance.setValue(j, (instance.value(j) - min[j]) / (max[j] - min[j]));
            }
        }

        return dataSet;
    }

}
