package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;
import weka.core.SelectedTag;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for linear regression.
 */
@Model(names = {"linreg"})
public class LinearRegression extends AbstractModel<weka.classifiers.functions.LinearRegression> {

    /**
     * Attribute selection methods.
     */
    enum AttributeSelectionMethod {

        M5 {
            @Override
            int getTagValue() {
                return weka.classifiers.functions.LinearRegression.SELECTION_M5;
            }
        },
        None {
            @Override
            int getTagValue() {
                return weka.classifiers.functions.LinearRegression.SELECTION_NONE;
            }
        },
        Greedy {
            @Override
            int getTagValue() {
                return weka.classifiers.functions.LinearRegression.SELECTION_GREEDY;
            }
        };

        /**
         * Returns the Weka Tag constant of the respective method.
         *
         * @return the Weka Tag constant of the respective method.
         */
        abstract int getTagValue();

        /**
         * The Weka Tag instance.
         *
         * @return the Weka Tag instance.
         */
        public SelectedTag getWekaInstance() {
            return new SelectedTag(getTagValue(), weka.classifiers.functions.LinearRegression.TAGS_SELECTION);
        }
    }

    /**
     * The parameter information.
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -S :: Set the attribute selection method to use. 1 = None, 2 = Greedy. (default 0 = M5 method)
        paramInfo.put("S", String.class);

        // * -C :: Try to eliminate colinear attributes.
        paramInfo.put("C", Boolean.class);

        // * -R :: Set ridge parameter (default 1.0e-8).
        paramInfo.put("R", Double.class);
    }

    /**
     * The default parameter grid to use for model selection.
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("S", "M5", "None");
        defaultGrid.setValues("C", true, false);
        defaultGrid.setValues("R", 1e-8, 1e-7, 1e-6, 1e-5);
    }

    /**
     * Constructor.
     */
    public LinearRegression() {
        super(new weka.classifiers.functions.LinearRegression());
    }

    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        return defaultGrid;
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        return paramInfo;
    }

    @Override
    public boolean checkForTaskTypeSupport(TaskType taskType) {
        return (taskType == TaskType.Regression);
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        switch (param) {
            case "S":
                AttributeSelectionMethod attrSelectMethod = AttributeSelectionMethod.valueOf(((String)value));
                wekaClassifier.setAttributeSelectionMethod(attrSelectMethod.getWekaInstance());
                break;
            case "C":
                wekaClassifier.setEliminateColinearAttributes((Boolean)value);
                break;
            case "R":
                wekaClassifier.setRidge((Double)value);
                break;
            default:
                throw new UnknownParameterException("Unknown parameter '" + param + "'");
        }
    }
}
