package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization;

import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import java.util.Arrays;

/**
 * DataNormalizer implementation for Z-Score standardization.
 */
public class ZScoreDataNormalizer extends AbstractDataNormalizer {

    /**
     * The mean values per attribute.
     * (non-numeric attributes are NaN)
     */
    double[] mean;

    /**
     * The standard deviation per attribute.
     * (non-numeric attributes are NaN)
     */
    double[] sd;

    @Override
    public void prepareInternal(Instances dataSet) {
        int numAttrs = dataSet.numAttributes();

        // - initialize mean, sd arrays
        mean = new double[numAttrs];
        Arrays.fill(mean, Double.NaN);
        sd = new double[numAttrs];
        Arrays.fill(sd, Double.NaN);

        for (int j = 0; j < numAttrs; j++) {
            // - skip class attribute
            if (dataSet.classIndex() == j) {
                continue;
            }
            if (dataSet.attribute(j).type() == Attribute.NUMERIC) {
                mean[j] = dataSet.meanOrMode(j);
                sd[j] = Math.sqrt(dataSet.variance(j));
            }
        }
    }

    @Override
    public Instances normalizeInternal(Instances dataSet) {
        assert(mean.length == sd.length);

        // - check if number of attributes match the data set used to prepare normalizer.
        int numAttrs = dataSet.numAttributes();
        if (numAttrs != mean.length) {
            throw new IllegalArgumentException("Attribute number of provided data set differs from attribute number of data set used for preparation.");
        }

        // - normalize values
        for (int i = 0; i < dataSet.numInstances(); i++) {
            Instance instance = dataSet.instance(i);
            for (int j = 0; j < numAttrs; j++) {
                // - skip class attribute
                if (instance.classIndex() == j) {
                    continue;
                }
                // - skip non-numeric attributes
                if (instance.attribute(j).type() != Attribute.NUMERIC) {
                    continue;
                }
                // - scale to zero mean, unit variance
                instance.setValue(j, (instance.value(j) - mean[j]) / sd[j]);
            }
        }

        return dataSet;
    }
}
