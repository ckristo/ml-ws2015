package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by christoph on 25/01/16.
 */
@Target(value={ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Model {
    String[] names() default "";
}
