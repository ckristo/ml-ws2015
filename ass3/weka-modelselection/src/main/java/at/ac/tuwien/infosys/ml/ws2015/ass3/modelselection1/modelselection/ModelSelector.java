package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.IModel;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.ModelException;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.Models;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.TaskType;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization.IDataNormalizer;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization.FeatureNormalizationMethod;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization.NOPDataNormalizer;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.util.DataSetUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Model selection implementation.
 */
public class ModelSelector {

    /**
     * Encapsulates model parameters.
     */
    class ModelSelectionParameters {

        /**
         * The parameter grid.
         */
        TypedParameterGrid parameterGrid;

        /**
         * Constructor.
         *
         * @param parameterGrid the parameter grid
         */
        ModelSelectionParameters(TypedParameterGrid parameterGrid) {
            this.parameterGrid = parameterGrid;
        }
    }

    /**
     * Logger instance.
     */
    private static final Logger logger = LogManager.getLogger(ModelSelector.class);

    /**
     * List of models that should be trained and evaluated against each other.
     */
    private HashMap<String, ModelSelectionParameters> registeredModels = new HashMap<>();

    /**
     * The task type.
     */
    private final TaskType taskType;

    /**
     * The normalization method to use.
     */
    private IDataNormalizer dataNormalizer = new NOPDataNormalizer();

    /**
     * The seed value to use for randomized operations.
     */
    private Integer seed = null;

    /**
     * Constructor.
     *
     * @param taskType the task type
     */
    public ModelSelector(TaskType taskType) {
        if (taskType == null) {
            throw new IllegalArgumentException("Please specify a task type");
        }
        this.taskType = taskType;
        this.dataNormalizer = null;
    }

    /**
     * Constructor.
     *
     * @param taskType the task type
     * @param normalizationMethod the data normalization method to apply
     */
    public ModelSelector(TaskType taskType, FeatureNormalizationMethod normalizationMethod) {
        if (taskType == null) {
            throw new IllegalArgumentException("Please specify a task type");
        }
        this.taskType = taskType;
        this.dataNormalizer = normalizationMethod.createDataNormalizer();
    }

    /**
     * Constructor.
     *
     * @param taskType the task type
     * @param seed     the seed value
     */
    public ModelSelector(TaskType taskType, int seed) {
        if (taskType == null) {
            throw new IllegalArgumentException("Please specify a task type");
        }
        this.taskType = taskType;
        this.seed = seed;
        this.dataNormalizer = null;
    }

    /**
     * Constructor.
     *
     * @param taskType the task type
     * @param normalizationMethod the data normalization method to apply
     * @param seed     the seed value
     */
    public ModelSelector(TaskType taskType, FeatureNormalizationMethod normalizationMethod, int seed) {
        if (taskType == null) {
            throw new IllegalArgumentException("Please specify a task type");
        }
        this.taskType = taskType;
        this.seed = seed;
        this.dataNormalizer = normalizationMethod.createDataNormalizer();
    }

    /**
     * Registers a model with default parameter grid.
     *
     * @param modelName the name of the model to register
     */
    public void registerModel(String modelName) {
        if (!Models.checkForTaskTypeSupport(modelName, taskType)) {
            throw new ModelSelectorException("Model '" + modelName + "' does not support '" + taskType + "' tasks");
        }
        if (registeredModels.containsKey(modelName)) {
            logger.warn("Model '{}' was already registered, overwriting prev. settings");
        }
        logger.info("Registering model '" + modelName + "' with default parameter grid.");
        registeredModels.put(modelName, new ModelSelectionParameters(Models.getDefaultParameterGrid(modelName)));
    }

    /**
     * Registers a model with the provided parameter grid.
     *
     * @param modelName     the name of the model to register
     * @param parameterGrid the parameter grid to use (null = use default parameter grid)
     */
    public void registerModel(String modelName, TypedParameterGrid parameterGrid) {
        if (!Models.checkForTaskTypeSupport(modelName, taskType)) {
            throw new ModelSelectorException("Model '" + modelName + "' does not support '" + taskType + "' tasks");
        }
        if (registeredModels.containsKey(modelName)) {
            logger.warn("Model '{}' was already registered, overwriting prev. settings");
        }
        if (parameterGrid == null) {
            logger.info("Registering model '" + modelName + "' with default parameter grid.");
            parameterGrid = Models.getDefaultParameterGrid(modelName);
        } else {
            logger.info("Registering model '" + modelName + "' with custom parameter grid.");
        }
        registeredModels.put(modelName, new ModelSelectionParameters(parameterGrid));
    }

    /**
     * Registers a model with the provided parameter grid.
     *
     * @param modelName     the name of the model to register
     * @param parameterGrid the parameter grid to use (null = use default parameter grid)
     */
    public void registerModel(String modelName, StringParameterGrid parameterGrid) {
        if (parameterGrid == null) {
            registerModel(modelName, (TypedParameterGrid)null);
        } else {
            TypedParameterGrid typedParamGrid = parameterGrid.convertToTypedParameterGrid(Models.getParameterInformationByName(modelName));
            registerModel(modelName, typedParamGrid);
        }
    }

    /**
     * Removes the model with the given names from the list of registered models.
     *
     * @param modelName
     */
    public void deregisterModel(String modelName) {
        registeredModels.remove(modelName);
    }

    /**
     * Sets the random seed value for randomized operations.
     *
     * @param seed the random seed value.
     */
    public void setSeed(int seed) {
        this.seed = seed;
    }

    /**
     * Sets the normalization method used to perform feature scaling.
     *
     * @param normalizationMethod the normalization method
     */
    public void setFeatureNormalizationMethod(FeatureNormalizationMethod normalizationMethod) {
        this.dataNormalizer = normalizationMethod.createDataNormalizer();
    }

    /**
     * Trains and evaluates all registered models.
     * This variant will use an explicitly given training and test set for evaluation.
     *
     * @param train the training set.
     * @param test  the test set.
     * @return the model selection results.
     */
    public ModelSelectionResults trainAndEvaluate(Instances train, Instances test) {
        ModelSelectionResults results = new ModelSelectionResults(taskType);
        for (Map.Entry<String, ModelSelectionParameters> registeredModel : registeredModels.entrySet()) {
            String modelName = registeredModel.getKey();
            ModelSelectionParameters modelSelectionParameters = registeredModel.getValue();

            // - instantiate model
            IModel model = Models.instantiateByName(modelName);

            // - register data normalizer
            model.useDataNormalizer(dataNormalizer);

            // - expand parameter grid -- train with each parameter combination
            Iterator<Map<String, Object>> expGridIter = modelSelectionParameters.parameterGrid.expandGridIterator();
            while (expGridIter.hasNext()) {
                // -- set parameter
                Map<String, Object> params = expGridIter.next();
                try {
                    model.setParameters(params);
                } catch (ModelException ex) {
                    logger.warn("Could not set parameters '{}' for model '{}' - ignore parameter combination", params, modelName, ex);
                    continue;
                }

                logger.info("Train and evaluate model '{}' with parameters {}", modelName, params);

                // -- train & evaluate model
                Evaluation eval = model.trainAndEvaluate(train, test);
                ModelEvaluation result = new ModelEvaluation(taskType, modelName, params, eval, train);

                logger.info("Evaluation results for '{}' with parameters {} ::", modelName, params);
                logger.info(result.toString());

                results.addEvaluation(result);
            }
        }
        return results;
    }

    /**
     * Trains and evaluates all registered models.
     * This variant will perform a random split into training and test set for evaluation.
     *
     * @param train the training set.
     * @param p     determines the size of the split (between 0 and 1)
     * @return the model selection results.
     */
    public ModelSelectionResults trainAndEvaluateRandomSplit(Instances train, float p) {
        Instances[] split;
        if (seed != null) {
            split = DataSetUtils.randomizedSplit(train, p, seed);
        } else {
            split = DataSetUtils.randomizedSplit(train, p);
        }
        return trainAndEvaluate(split[0], split[1]);
    }

    /**
     * Trains and evaluates all registered models.
     * This variant will use an k-fold CV for evaluation.
     *
     * @param train the training set.
     * @param k     the number of folds for k-fold CV
     * @return the model selection results.
     */
    public ModelSelectionResults trainAndEvaluateKFoldCV(Instances train, int k) {
        ModelSelectionResults results = new ModelSelectionResults(taskType);
        for (Map.Entry<String, ModelSelectionParameters> registeredModel : registeredModels.entrySet()) {
            String modelName = registeredModel.getKey();
            ModelSelectionParameters modelSelectionParameters = registeredModel.getValue();

            // - instantiate model
            IModel model = Models.instantiateByName(modelName);

            // - register data normalizer
            model.useDataNormalizer(dataNormalizer);

            // - expand parameter grid -- train with each parameter combination
            Iterator<Map<String, Object>> expGridIter = modelSelectionParameters.parameterGrid.expandGridIterator();
            while (expGridIter.hasNext()) {
                // -- set parameter
                Map<String, Object> params = expGridIter.next();
                try {
                    model.setParameters(params);
                } catch (ModelException ex) {
                    logger.warn("Could not set parameters '{}' for model '{}' - ignore parameter combination", params, modelName, ex);
                    continue;
                }

                logger.info("Train and evaluate model '{}' using {}-fold CV with parameters {}", modelName, k, params);

                // -- train & evaluate model
                Evaluation eval;
                if (seed != null) {
                    eval = model.trainAndEvaluateKFoldCV(train, k, seed);
                } else {
                    eval = model.trainAndEvaluateKFoldCV(train, k);
                }
                ModelEvaluation result = new ModelEvaluation(taskType, modelName, params, eval, train);

                logger.info("Evaluation results for '{}' with parameters {} ::", modelName, params);
                logger.info(result.toString());

                results.addEvaluation(result);
            }
        }
        return results;
    }

    /**
     * Trains and evaluates all registered models.
     * This variant will use bootstrapping for evaluation.
     *
     * @param train the training set.
     * @return the model selection results.
     */
    public ModelSelectionResults trainAndEvaluateBootstrapping(Instances train, int numIterations) {
        ModelSelectionResults results = new ModelSelectionResults(taskType);
        for (Map.Entry<String, ModelSelectionParameters> registeredModel : registeredModels.entrySet()) {
            String modelName = registeredModel.getKey();
            ModelSelectionParameters modelSelectionParameters = registeredModel.getValue();

            // - instantiate model
            IModel model = Models.instantiateByName(modelName);

            // - register data normalizer
            model.useDataNormalizer(dataNormalizer);

            // - expand parameter grid -- train with each parameter combination
            Iterator<Map<String, Object>> expGridIter = modelSelectionParameters.parameterGrid.expandGridIterator();
            while (expGridIter.hasNext()) {
                // -- set parameter
                Map<String, Object> params = expGridIter.next();
                try {
                    model.setParameters(params);
                } catch (ModelException ex) {
                    logger.warn("Could not set parameters '{}' for model '{}' - ignore parameter combination", params, modelName, ex);
                    continue;
                }

                logger.info("Train and evaluate model '{}' using bootstrapping ({} iterations) with parameters {}", modelName, numIterations, params);

                // -- train & evaluate model
                Evaluation eval;
                if (seed != null) {
                    eval = model.trainAndEvaluateBootstrap(train, numIterations, seed);
                } else {
                    eval = model.trainAndEvaluateBootstrap(train, numIterations);
                }
                ModelEvaluation result = new ModelEvaluation(taskType, modelName, params, eval, train);

                logger.info("Evaluation results for '{}' with parameters {} ::", modelName, params);
                logger.info(result.toString());

                results.addEvaluation(result);
            }
        }
        return results;
    }
}
