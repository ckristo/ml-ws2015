package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.visualize;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.ModelEvaluation;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.ModelSelectionResults;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.*;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.core.FastVector;
import weka.core.Instances;

import java.util.Map;
import java.util.Set;

public class ROCPlot extends AbstractPlot {

    @Override
    public JFreeChart createChart(ModelSelectionResults results) {
        JFreeChart chart;
        XYSeriesCollection chartData = new XYSeriesCollection();

        for(Map.Entry<String, Set<ModelEvaluation>> result : results.getEvaluations().entrySet()) {
            Set<ModelEvaluation> evals = result.getValue();

            int i = 0;
            for(ModelEvaluation eval : evals) {
                String params = eval.getParameters().toString();
                XYSeries dataSeries = this.getSeries(eval.getModelName() + i + params, eval);
                chartData.addSeries(dataSeries);

                i++;
            }
        }

        chart = ChartFactory.createXYLineChart(
                "ROC", "FP Rate", "TP Rate", chartData,
                PlotOrientation.VERTICAL,
                true, // legend
                true, // tooltips
                false // urls
        );

        return chart;
    }

    private XYSeries getSeries(String modelName, ModelEvaluation eval) {
        XYSeries dataSeries = new XYSeries(modelName);
        Evaluation evaluation = eval.getEvaluation();

        // This is the reason why i had to propagate the train data instances all the way
        // down to the model evaluation object. The only other possibility seems to be
        // to retrieve the predictions fast vector from weka's internal evaluation object,
        // then to iterate over all the predictions to determine the number of classes,
        // then do a second iteration to compare the actual and predicted classes in order to determine
        // if a mismatch is a true negative or a false positive.
        //
        // overall, this still seemed like the better approach.
//        for(int i = 0;i < eval.getTrainData().numClasses();i++) {
//            dataSeries.add(
//                    evaluation.falsePositiveRate(i),
//                    evaluation.truePositiveRate(i)
//            );
//        }

        FastVector predictions = evaluation.predictions();

        ThresholdCurve curve = new ThresholdCurve();
        Instances result = curve.getCurve(predictions);

        double[] tpRate = result.attributeToDoubleArray(result.attribute(ThresholdCurve.TP_RATE_NAME).index());
        double[] fpRate = result.attributeToDoubleArray(result.attribute(ThresholdCurve.FP_RATE_NAME).index());

        for(int i = 0;i < tpRate.length;i++) {
            dataSeries.add(fpRate[i], tpRate[i]);
        }

        return dataSeries;
    }
}
