package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.util;

import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;

import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * Data set loader component -- also may be used to split and resample data sets.
 */
public class DataSetUtils {

    /**
     * Loads a data set from an ARFF file.
     * The class column of the resulting data set will be the last by default.
     *
     * @param file the file to load
     * @return the loaded data set.
     * @throws IOException
     */
    public static Instances loadArffFile(File file) throws IOException {
        return loadArffFileInternal(file, null);
    }

    /**
     * Loads a data set from an ARFF file.
     * The class column of the resulting data set will be the last by default.
     *
     * @param filePath the path of the file to load.
     * @return the loaded data set
     * @throws IOException
     */
    public static Instances loadArffFile(String filePath) throws IOException {
        return loadArffFileInternal(new File(filePath), null);
    }

    /**
     * Loads a data set from an ARFF file.
     *
     * @param file       the file to load
     * @param classIndex the index of the class column
     * @return the loaded data set.
     * @throws IOException
     */
    public static Instances loadArffFile(File file, int classIndex) throws IOException {
        return loadArffFileInternal(file, classIndex);
    }

    /**
     * Loads a data set from an ARFF file.
     * The class column of the resulting data set will be the last by default.
     *
     * @param filePath   the path of the file to load.
     * @param classIndex the index of the class column
     * @return the loaded data set
     * @throws IOException
     */
    public static Instances loadArffFile(String filePath, int classIndex) throws IOException {
        return loadArffFileInternal(new File(filePath), classIndex);
    }

    /**
     * Internal implementation for loading a data set from an ARFF file.
     *
     * @param file       the file to load
     * @param classIndex the class index (null = last column)
     * @return the loaded data set.
     */
    private static Instances loadArffFileInternal(File file, Integer classIndex) throws IOException {
        ArffLoader arffLoader = new ArffLoader();
        arffLoader.setFile(file);
        Instances data = arffLoader.getDataSet();
        if (classIndex == null) {
            data.setClassIndex(data.numAttributes() - 1);
        } else {
            data.setClassIndex(classIndex);
        }
        return data;
    }

    /**
     * Write data to an ARFF file.
     *
     * @param data the data to write.
     * @param file the output file.
     * @throws IOException
     */
    public static void writeArffFile(Instances data, File file) throws IOException {
        ArffSaver arffSaver = new ArffSaver();
        arffSaver.setInstances(data);
        arffSaver.setFile(file);
        arffSaver.writeBatch();
    }

    /**
     * Write data to an ARFF file.
     *
     * @param data     the data to write.
     * @param filePath the path to the output file.
     * @throws IOException
     */
    public static void writeArffFile(Instances data, String filePath) throws IOException {
        writeArffFile(data, new File(filePath));
    }

    /**
     * Splits a data set using a percentage split.
     *
     * @param dataSet the data set to split.
     * @param p       determines the split sizes (between 0 and 1)
     * @return the split parts of the data set.
     */
    public static Instances[] split(Instances dataSet, float p) {
        Instances[] splits = new Instances[2];

        int trainSize = Math.round(dataSet.numInstances() * p);
        int testSize = dataSet.numInstances() - trainSize;

        splits[0] = new Instances(dataSet, 0, trainSize);
        splits[0].setClassIndex(dataSet.classIndex());
        splits[1] = new Instances(dataSet, trainSize, testSize);
        splits[1].setClassIndex(dataSet.classIndex());

        return splits;
    }

    /**
     * Randomizes the data set and then performs a percentage split.
     *
     * @param dataSet the data set
     * @param p       determines the split sizes (between 0 and 1)
     * @param random  the random object for data set randomization
     * @return the randomized split parts of the data set.
     */
    public static Instances[] randomizedSplit(Instances dataSet, float p, Random random) {
        Instances randDataSet = new Instances(dataSet);
        randDataSet.randomize(random);
        return split(randDataSet, p);
    }

    /**
     * Randomizes the data set and then performs a percentage split.
     *
     * @param dataSet the data set
     * @param p       determines the split sizes (between 0 and 1)
     * @param seed    the random seed for data set randomization
     * @return the randomized split parts of the data set.
     */
    public static Instances[] randomizedSplit(Instances dataSet, float p, int seed) {
        return randomizedSplit(dataSet, p, new Random(seed));
    }

    /**
     * Randomizes the data set and then performs a percentage split.
     *
     * @param dataSet the data set
     * @param p       determines the split sizes (between 0 and 1)
     * @return the randomized split parts of the data set.
     */
    public static Instances[] randomizedSplit(Instances dataSet, float p) {
        return randomizedSplit(dataSet, p, new Random());
    }

    /**
     * Performs resampling for a data set.
     *
     * @param dataSet    the data set
     * @param sampleSize the size of the resampled data set
     * @param random     the random object to use for getting random numbers
     * @return an array of instances, [0]: resampled data set (train), [1]: samples not in the sample (test)
     */
    public static Instances[] resample(Instances dataSet, int sampleSize, Random random) {
        int dataSetSize = dataSet.numInstances();

        // - get random indices for resampling
        int[] sampleIndices = new int[sampleSize];
        boolean[] usedIndices = new boolean[dataSetSize];
        for (int i = 0; i < sampleSize; i++) {
            sampleIndices[i] = random.nextInt(dataSetSize);
            usedIndices[sampleIndices[i]] = true;
        }

        // - create two data sets, one with sampled instances, one with instances not sampled
        Instances[] instancesArray = new Instances[2];
        instancesArray[0] = new Instances(dataSet, sampleSize);
        instancesArray[1] = new Instances(dataSet, dataSetSize);
        for (int i = 0; i < sampleSize; i++) {
            instancesArray[0].add(dataSet.instance(sampleIndices[i]));
            if (!usedIndices[i]) {
                instancesArray[1].add(dataSet.instance(i));
            }
        }
        for (int i = sampleSize; i < dataSetSize; i++) {
            if (!usedIndices[i]) {
                instancesArray[1].add(dataSet.instance(i));
            }
        }
        instancesArray[1].compactify();

        return instancesArray;
    }

    /**
     * Performs resampling of a data set.
     *
     * @param dataSet    the data set
     * @param sampleSize the size of the resampled data set
     * @param seed       the random seed to use for getting random numbers
     * @return an array of instances, [0]: resampled data set (train), [1]: samples not in the sample (test)
     */
    public static Instances[] resample(Instances dataSet, int sampleSize, int seed) {
        return resample(dataSet, sampleSize, new Random(seed));
    }

    /**
     * Performs resampling of a data set.
     *
     * @param dataSet    the data set
     * @param sampleSize the size of the resampled data set
     * @return an array of instances, [0]: resampled data set (train), [1]: samples not in the sample (test)
     */
    public static Instances[] resample(Instances dataSet, int sampleSize) {
        return resample(dataSet, sampleSize, new Random());
    }

}
