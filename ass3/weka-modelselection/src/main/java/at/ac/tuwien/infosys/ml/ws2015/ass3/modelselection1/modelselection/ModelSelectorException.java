package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

/**
 * Created by christoph on 24/01/16.
 */
public class ModelSelectorException extends RuntimeException {

    public ModelSelectorException() {
        super();
    }

    public ModelSelectorException(String msg) {
        super(msg);
    }

    public ModelSelectorException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
