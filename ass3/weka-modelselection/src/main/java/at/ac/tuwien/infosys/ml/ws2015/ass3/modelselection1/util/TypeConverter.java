package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.util;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by christoph on 26/01/16.
 */
public class TypeConverter {

    /**
     * Converts a string value to an object of a given type.
     * @param value the value to convert
     * @param clazz the type for conversion
     * @return the converted value as object
     * @throws IllegalArgumentException
     */
    public static Object convertString(String value, Class clazz) throws IllegalArgumentException {
        // parse string value to expected parameter value type.
        // - String
        if (String.class.equals(clazz)) {
            return value;
        }
        // - Character
        if (Character.class.equals(clazz)) {
            if (value.length() != 1) {
                throw new IllegalArgumentException("Provided string could not be parsed into a character");
            }
            return value.charAt(0);
        }
        // - Boolean
        if (Boolean.class.equals(clazz)) {
            return Boolean.parseBoolean(value);
        }
        // - Short
        if (Short.class.equals(clazz)) {
            return Short.parseShort(value);
        }
        // - Integer
        if (Integer.class.equals(clazz)) {
            return Integer.parseInt(value);
        }
        // - Long
        if (Long.class.equals(clazz)) {
            return Long.parseLong(value);
        }
        // - Float
        if (Float.class.equals(clazz)) {
            return Float.parseFloat(value);
        }
        // - Double
        if (Double.class.equals(clazz)) {
            return Double.parseDouble(value);
        }
        // - Byte
        if (Byte.class.equals(clazz)) {
            return Byte.parseByte(value);
        }
        throw new UnsupportedOperationException("Do not know how to convert parameter type '" + clazz.getSimpleName() + "' from string");
    }

    /**
     *
     * @param values
     * @param clazz
     * @return
     * @throws IllegalArgumentException
     */
    public static Set<Object> convertStringSet(Set<String> values, Class clazz) throws IllegalArgumentException {
        Set<Object> convertedValues = values.stream()
                .map(v -> convertString(v, clazz))
                .collect(Collectors.toCollection(LinkedHashSet::new));
        return convertedValues;
    }
}
