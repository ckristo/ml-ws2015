package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;
import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.normalization.IDataNormalizer;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Lightweight wrapper interface around Weka ML classifier.
 */
public interface IModel {

    /**
     * Returns the name the classifier was instantiated with (or the simple class name if none was given).
     *
     * @return the name the classifier was instantiated with (or the simple class name if none was given).
     */
    String getName();

    /**
     * Sets the data normalizer to use for training and evaluation.
     *
     * @param normalizer the data normalizer instance.
     */
    void useDataNormalizer(IDataNormalizer normalizer);

    /**
     * Sets the name of the model.
     *
     * @return the new model name.
     */
    void setName(String name);

    /**
     * Train the model.
     *
     * @param train the data to train the model.
     */
    void train(Instances train);

    /**
     * Evaluate the model.
     *
     * @param test the test data
     * @return the evaluation result.
     */
    Evaluation evaluate(Instances test);

    /**
     * Trains and evaluates the model.
     *
     * @param train the train data.
     * @param test the test data.
     * @return the evaluation result.
     */
    Evaluation trainAndEvaluate(Instances train, Instances test);

    /**
     * Evaluates a model using k-fold cross validation.
     *
     * @param data the training data.
     * @param k the number of folds
     * @return the evaluation result.
     */
    Evaluation trainAndEvaluateKFoldCV(Instances data, int k);

    /**
     * Evalues a model using k-fold cross validation.
     *
     * @param data the training data.
     * @param k the number of folds.
     * @param seed the random seed to use.
     * @return the evaluation result.
     */
    Evaluation trainAndEvaluateKFoldCV(Instances data, int k, int seed);

    /**
     * Evaluates a model using bootstrapping.
     *
     * @param data the training data.
     * @param numIterations the number of iterations.
     * @return the evaluation result.
     */
    Evaluation trainAndEvaluateBootstrap(Instances data, int numIterations);

    /**
     * Evaluates a model using bootstrapping.
     *
     * @param data the training data.
     * @param numIterations the number of iterations.
     * @param seed the random seed to use.
     * @return the evaluation result.
     */
    Evaluation trainAndEvaluateBootstrap(Instances data, int numIterations, int seed);

    /**
     * Sets a parameter for the model.
     *
     * @param param the parameter name.
     * @param value the parameter value.
     */
    void setParameter(String param, Object value);

    /**
     * Sets a bunch of parameters for the model.
     *
     * @param values a map where parameter names are keys and parameter values are values.
     */
    void setParameters(Map<String, Object> values);

    /**
     * Sets a parameter for the model given as string.
     * NOTE: Parameter will be parsed to expected type.
     *
     * @param param the parameter name.
     * @param value the parameter value (as string).
     */
    void setParameterAsString(String param, String value);

    /**
     * Sets a bunch of parameters for the model.
     * NOTE: Parameter will be parsed to expected type.
     *
     * @param values a map where parameter names are keys and parameter values are values.
     */
    void setParametersAsString(Map<String, String> values);

    /**
     * Returns an unmodifiable instance of the parameters map.
     *
     * @return an unmodifiable instance of the parameters map.
     */
    Map<String, Object> getParameters();

    /**
     * Returns the default parameter grid to use for model selection.
     *
     * @return the default parameter grid to use for model selection.
     */
    TypedParameterGrid getDefaultParameterGrid();

    /**
     * Return parameter information (which parameters are known and which types are expected).
     *
     * @return parameter information as map.
     */
    Map<String, Class> getParameterInformation();

    /**
     * Returns the classifier associated with this model.
     *
     * @return classifier
     */
    Classifier getWekaClassifier();

    /**
     * Writes the Weka classifier to an output file.
     *
     * @param file the output file.
     */
    void exportWekaClassifier(File file) throws IOException;

    // TODO: we should export the whole model object (including the normalizer) to a file instead of exporting only the Weka classifier

    /**
     * Writes the Weka Classifier to an output file.
     *
     * @param filePath the path to the output file.
     * @throws IOException
     */
    void exportWekaClassifier(String filePath) throws IOException;

    /**
     * Imports an exported Weka classifier.
     *
     * @param file the input file
     * @throws IOException
     */
    void importWekaClassifier(File file) throws IOException;

    /**
     * Imports an exported Weka classifier.
     *
     * @param filePath the path to the input file
     * @throws IOException
     */
    void importWekaClassifier(String filePath) throws IOException;

    /**
     * Checks if a model does support a specific task type (e.g. classification or regression).
     *
     * @param taskType the task type.
     * @return true if the task type is supported, false otherwise.
     */
    boolean checkForTaskTypeSupport(TaskType taskType);
}
