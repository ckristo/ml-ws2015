package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

/**
 * Exception thrown when a passed model names is unknown.
 */
public class IllegalModelNameException extends ModelException {

    public IllegalModelNameException() {
        super();
    }

    public IllegalModelNameException(String msg) {
        super(msg);
    }

    public IllegalModelNameException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
