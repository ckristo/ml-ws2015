package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by christoph on 26/01/16.
 */
public interface IParameterGrid<KeyType, ValueType> {

    /**
     * Puts values for a parameter to the parameter grid.
     * NOTE: if values were added previously, they will be overwritten!
     *
     * @param key  the parameter names
     * @param values the parameter values
     */
    void setValues(KeyType key, Set<ValueType> values);

    /**
     * Puts values for a parameter to the parameter grid.
     * NOTE: if values were added previously, they will be overwritten!
     *
     * @param key  the parameter names
     * @param values the parameter values
     */
    void setValues(KeyType key, ValueType... values);

    /**
     * Adds values to the parameter grid.
     *
     * @param key  the parameter names.
     * @param values the parameter values to add.
     */
    void addValues(KeyType key, Set<ValueType> values);

    /**
     * Adds values to the parameter grid.
     *
     * @param key  the parameter names.
     * @param values the parameter values to add.
     */
    void addValues(KeyType key, ValueType... values);

    /**
     * Adds a value to the parameter grid.
     *
     * @param key the parameter names.
     * @param value the parameter value to add.
     */
    void addValue(KeyType key, ValueType value);

    /**
     * Merges a parameter grid into another.
     * NOTE: when both contain the same key, the keys of the calling parameter grid will be overwritten!
     *
     * @param paramGrid the parameter grid to merge.
     */
    void merge(IParameterGrid<KeyType, ValueType> paramGrid);

    /**
     * Returns the values for a given parameter as unmodifiable set.
     * NOTE: this function returns an unmodifiable set instance!
     *
     * @param param the parameter name
     * @return the set (unmodifiable!)
     */
    Set<ValueType> getValues(KeyType param);

    /**
     * Checks if the parameter grid contains values for a given parameter name.
     * @param param the parameter name
     * @return true if it contains values, false otherwise.
     */
    boolean containsValues(KeyType param);

    /**
     * Returns a set of all parameter keys.
     * @return a set of all parameter keys.
     */
    Set<KeyType> keySet();

    /**
     * Returns an iterator that allows iterating over the expanded parameter grid
     * (i.e. all possible parameter combinations).
     *
     * @return an iterator instance.
     */
    Iterator<Map<KeyType, ValueType>> expandGridIterator();
}
