package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

/**
 * Exception thrown by classes of the models package on failure.
 */
public class ModelException extends RuntimeException {

    public ModelException() {
        super();
    }

    public ModelException(String msg) {
        super(msg);
    }

    public ModelException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
