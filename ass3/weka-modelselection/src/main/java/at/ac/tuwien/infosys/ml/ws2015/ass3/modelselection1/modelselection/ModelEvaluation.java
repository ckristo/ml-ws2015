package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model.TaskType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import java.util.Map;

/**
 * Class encapsulating a model selection result.
 */
public class ModelEvaluation implements Comparable<ModelEvaluation> {

    /**
     * The logger instance.
     */
    private static final Logger logger = LogManager.getLogger(ModelEvaluation.class);

    /**
     * The task type the
     */
    private final TaskType taskType;

    /**
     * The name of the model that was evaluated.
     */
    private String modelName;

    /**
     * The parameters the model was trained with.
     */
    private Map<String, Object> parameters;

    /**
     * The evaluation result.
     */
    private Evaluation eval;

    /**
     * The train data.
     */
    private Instances trainData;

    /**
     * Constructor.
     *
     * @param modelName  the name of the model.
     * @param parameters the parameters the model was trained with.
     * @param eval       the evaluation result instance.
     */
    public ModelEvaluation(TaskType taskType, String modelName, Map<String, Object> parameters, Evaluation eval, Instances trainData) {
        if (taskType == null) {
            throw new IllegalArgumentException("'taskType' cannot be null");
        }
        this.taskType = taskType;
        this.modelName = modelName;
        this.parameters = parameters;
        this.eval = eval;
        this.trainData = trainData;
    }

    /**
     * Returns the task type of the model evaluation result.
     *
     * @return the task type of the model evaluation result.
     */
    public TaskType getTaskType() {
        return taskType;
    }

    /**
     * Returns the name of the model that was evaluated.
     *
     * @return the name of the model that was evaluated.
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * Returns the model parameters used to train the model that was evaluated.
     *
     * @return the model parameters used to train the model that was evaluated.
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * Returns the evaluation results.
     *
     * @return the evaluation results.
     */
    public Evaluation getEvaluation() {
        return eval;
    }

    /**
     * Returns the training data.
     *
     * @return the training data.
     */
    public Instances getTrainData() {
        return trainData;
    }

    /**
     * Returns the result value for a given performance measure.
     *
     * @return the result value for a given performance measure.
     */
    public double getResultValue(PerformanceMeasure performanceMeasure) {
        return performanceMeasure.getValue(eval);
    }

    /**
     * Returns the confusion matrix for the evaluation result.
     *
     * @return the confusion matrix for the evaluation result.
     * @throws UnsupportedOperationException if called when the model evaluation result is for other task types than Classification.
     */
    public double[][] getConfusionMatrix() {
        if (taskType != TaskType.Classification) {
            throw new UnsupportedOperationException("Can return a confusion matrix only for " + TaskType.Classification + "' tasks");
        } else {
            return eval.confusionMatrix();
        }
    }

    /**
     * compareTo().
     */
    public int compareTo(ModelEvaluation o, PerformanceMeasure measure) {
        if (measure == null) {
            throw new IllegalStateException("Please specify performance measure first!");
        }
        return Double.compare(measure.getValue(eval),
                measure.getValue(o.getEvaluation())) * measure.getComparisonDirection();
    }

    /**
     * compareTo() -- using the default performance measure defined for the task type the model evaluation result is for.
     */
    @Override
    public int compareTo(ModelEvaluation o) {
        return compareTo(o, PerformanceMeasure.getDefaultPerformanceMeasure(taskType));
    }

    /**
     * Returns the string representation of the model evaluation result.
     *
     * @return the string representation of the model evaluation result.
     */
    @Override
    public String toString() {
        String ret = "";
        ret += String.format("Model: '%s', Parameters: %s%n", modelName, parameters);
        ret += String.format("=== Results ============%n");
        for (PerformanceMeasure pm : PerformanceMeasure.getSupportedPerformanceMeasures(taskType)) {
            ret += String.format("  %9s = %.4f%n", pm.toString(), pm.getValue(eval));
        }
        if (taskType == TaskType.Classification) {
            String confMatrix = null;
            try {
                confMatrix = eval.toMatrixString();
            } catch (Exception ex) {
                logger.error("Could not create confusion matrix string", ex);
            }
            if (confMatrix != null) {
                ret += confMatrix;
            }
        }
        return ret;
    }

    /**
     * Returns the model evaluation result as Weka Instance object.
     *
     * @return the Weka Instance object.
     */
    public Instance toInstance(Instances dataSet) {
        PerformanceMeasure[] performanceMeasures = PerformanceMeasure.getSupportedPerformanceMeasures(taskType);

        int offset = 2;
        int numAttributes = performanceMeasures.length + offset; // + 2 => model name, parameters
        Instance instance = new Instance(numAttributes);
        instance.setDataset(dataSet);
        instance.setValue(0, modelName);
        instance.setValue(1, parameters.toString());

        int i = offset;
        for (PerformanceMeasure pm : performanceMeasures) {
            instance.setValue(i, pm.getValue(eval));
            i++;
        }

        return instance;
    }


    /**
     * Returns the attribute information for the Weka Instance object.
     *
     * @param taskType the task type (needed for determining the applicable performance measure attributes).
     * @return the attribute information for the Weka Instance object.
     */
    public static FastVector instanceAttrInfo(TaskType taskType) {
        PerformanceMeasure[] perfMeasures = PerformanceMeasure.getSupportedPerformanceMeasures(taskType);
        int numAttributes = perfMeasures.length + 2; // + 2 => model name, parameters

        FastVector attributes = new FastVector(numAttributes);
        attributes.addElement(new Attribute("modelName", (FastVector)null));
        attributes.addElement(new Attribute("parameters", (FastVector)null));

        for (PerformanceMeasure pm : perfMeasures) {
            attributes.addElement(new Attribute(pm.toString()));
        }

        return attributes;
    }

}
