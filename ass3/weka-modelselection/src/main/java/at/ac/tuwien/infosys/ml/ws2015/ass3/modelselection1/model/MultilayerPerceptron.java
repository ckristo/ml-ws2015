package at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.model;

import at.ac.tuwien.infosys.ml.ws2015.ass3.modelselection1.modelselection.TypedParameterGrid;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model wrapper for multilayer perceptron.
 */
@Model(names = {"mlp"})
public class MultilayerPerceptron extends AbstractModel<weka.classifiers.functions.MultilayerPerceptron> {

    /**
     * The parameter information.
     */
    static Map<String, Class> paramInfo = new LinkedHashMap<>();
    static {
        // * -L :: Learning Rate for the backpropagation algorithm.
        //         (Value should be between 0 - 1, Default = 0.3).
        paramInfo.put("L", Double.class);

        // * -M :: Momentum Rate for the backpropagation algorithm.
        //         (Value should be between 0 - 1, Default = 0.2).
        paramInfo.put("M", Double.class);

        // * -N :: Number of epochs to train through. (Default = 500).
        paramInfo.put("N", Integer.class);

        // * -V :: Percentage size of validation set to use to terminate
        //         training (if this is non zero it can pre-empt num of epochs).
        //         (Value should be between 0 - 100, Default = 0).
        paramInfo.put("V", Integer.class);

        // * -S :: The value used to seed the random number generator.
        //         (Value should be >= 0, Default = 0).
        paramInfo.put("S", Integer.class);

        // * -E :: The consequetive number of errors allowed for validation
        //         testing before the network terminates.
        //         (Value should be > 0, Default = 20).
        paramInfo.put("E", Integer.class);

        // * -B :: A NominalToBinary filter will NOT automatically be used.
        paramInfo.put("B", Boolean.class);

        // * -H :: The hidden layers to be created for the network.
        //         (Value should be a list of comma separated Natural
        //          numbers or the letters = 'a' = (attribs + classes) / 2,
        //          'i' = attribs, 'o' = classes, 't' = (attribs .+ classes
        //          for wildcard values, Default = a).
        paramInfo.put("H", String.class);

        // * -C :: Normalizing a numeric class will NOT be done.
        paramInfo.put("C", Boolean.class);

        // * -I :: Normalizing the attributes will NOT be done.
        paramInfo.put("I", Boolean.class);

        // * -R :: Resetting the network will NOT be allowed.
        paramInfo.put("R", Boolean.class);

        // * -D :: Learning rate decay will occur.
        paramInfo.put("D", Boolean.class);
    }

    /**
     * The default parameter grid to use for model selection.
     */
    static TypedParameterGrid defaultGrid = new TypedParameterGrid();
    static {
        defaultGrid.setValues("L", 0.3);
        defaultGrid.setValues("M", 0.2);
        defaultGrid.setValues("N", 50, 250, 500);
        defaultGrid.setValues("V", 0);
        defaultGrid.setValues("S", 0);
        defaultGrid.setValues("E", 20);
        defaultGrid.setValues("B", false);
        defaultGrid.setValues("H", "a", "i", "o", "t");
        defaultGrid.setValues("C", false);
        defaultGrid.setValues("I", false);
        defaultGrid.setValues("R", false);
        defaultGrid.setValues("D", false);
    }

    /**
     * Constructor.
     */
    public MultilayerPerceptron() {
        super(new weka.classifiers.functions.MultilayerPerceptron());
    }

    @Override
    public TypedParameterGrid getDefaultParameterGrid() {
        return defaultGrid;
    }

    @Override
    public Map<String, Class> getParameterInformation() {
        return paramInfo;
    }

    @Override
    public boolean checkForTaskTypeSupport(TaskType taskType) {
        return taskType == TaskType.Classification || taskType == TaskType.Regression;
    }

    @Override
    protected void setParameterInternal(String param, Object value) throws Exception {
        switch (param) {
            case "L":
                wekaClassifier.setLearningRate((Double)value);
                break;
            case "M":
                wekaClassifier.setMomentum((Double)value);
                break;
            case "N":
                wekaClassifier.setTrainingTime((Integer)value);
                break;
            case "V":
                wekaClassifier.setValidationSetSize((Integer)value);
                break;
            case "S":
                wekaClassifier.setSeed((Integer)value);
                break;
            case "E":
                wekaClassifier.setValidationThreshold((Integer)value);
                break;
            case "B":
                wekaClassifier.setNominalToBinaryFilter(!(Boolean)value);
                break;
            case "H":
                wekaClassifier.setHiddenLayers((String)value);
                break;
            case "C":
                wekaClassifier.setNormalizeNumericClass(!(Boolean)value);
                break;
            case "I":
                wekaClassifier.setNormalizeAttributes(!(Boolean)value);
                break;
            case "R":
                wekaClassifier.setReset(!(Boolean)value);
                break;
            case "D":
                wekaClassifier.setDecay((Boolean)value);
                break;
            default:
                throw new UnknownParameterException("Unknown parameter '" + param + "'");
        }
    }
}
