
Correctly Classified Instances          11               91.6667 %
Incorrectly Classified Instances         1                8.3333 %
Kappa statistic                          0.8554
Mean absolute error                      0.0492
Root mean squared error                  0.1506
Total Number of Instances               12     

-----------------
Confusion matrix:
1.0 0.0
0.0 5.0
