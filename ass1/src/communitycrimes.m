
%%
% community_crimes().
%
function communitycrimes()
    % - fetch data set
    data = setup();
    
    % - fix the random seed value (for reproducable results)
    rng('default');
    rng(42);
    
    % - store goal attribute values
    goal = data.(130); % murders
    %goal = data.(138); % burglaries
    
    % - remove goal attributes
    data = data(1:height(data), 1:(width(data) - 18));
    
    % - reset goal attribute
    data.murders = goal;
    %data.burglaries = goal;

    % remove hindering (non predictive) attributes from training and test set
    % 1 = communityname (information only, not predictive)
    % 3 = countyCode (many missing values, not predictive)
    % 4 = communityCode (many missing values, not predictive)
    % 5 = fold number for non-random 10 fold cross validation (not predictive)
    % 
    % 104 - 120, 124 - 129: 1872 NaNs out of 2215 total
    data(:,[1,3,4,5,104:120,124:129]) = [];
    
    % - replace missing values of each feature with means of that feature
    %columns_with_missing_values = 2:125;
    columns_with_missing_values = 2:102;
    data = nans_to_means(data, columns_with_missing_values);
    
    % - define some constants
    %  * the name of the goal variable in the data table
    goal_var_name = 'murders';
    %goal_var_name = 'burglaries';
    %  * the index of the goal variable in the data table
    %goal_var_index = 126;
    goal_var_index = 103;
    %  * define which features to standardize
    %features_to_normalize = 2:125;
    features_to_normalize = 2:102;
    %  * define the normalization function
    normalizefun = @scale_features; % @scale_features, @standardize_features or 'none'
    
    % - print summary for dataset
    %print_summary(data, 'Community Crimes');
    
    % - linear regression -----------------------
    t = tic;
    evals = crossval(@linear_regression, data);
    t_total = toc(t);
    fprintf('Linear Regression\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');

    % - regression tree -------------------------
    t = tic;
    evals = crossval(@regression_tree, data);
    t_total = toc(t);
    fprintf('Regression Tree\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - random forests --------------------------
    t = tic;
    evals = crossval(@random_forests, data);
    t_total = toc(t);
    fprintf('Random Forests\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - SVM -------------------------------------
    t = tic;
    evals = crossval(@svm, data);
    t_total = toc(t);
    fprintf('SVM\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');

    %%
    % Applies feature standardization.
    %
    function [ tbl_X_train, tbl_X_test ] = apply_feature_normalization(tbl_X_train, tbl_X_test)
        if isa(normalizefun, 'function_handle')
            % - apply normalization to training set
            [ stdvals, X_mean, X_sd ] = normalizefun(tbl_X_train{:, features_to_normalize});
            tbl_X_train(:, features_to_normalize) = array2table(stdvals);
            % - apply normalization to test set
            stdvals = normalizefun(tbl_X_test{:, features_to_normalize}, X_mean, X_sd);
            tbl_X_test(:, features_to_normalize) = array2table( stdvals );
        end
    end

    %%
    % Performs linear regression training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = linear_regression(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        lrmodel = fitlm(train, 'linear', 'ResponseVar',goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(lrmodel, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs regression tree training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = regression_tree(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrtree(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs random forests training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = random_forests(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        tree = templateTree();
        model = fitensemble(train, goal_var_name, 'Bag', 50, tree, 'type','regression');
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs SVM training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = svm(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrsvm(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
        plot(1:height(test),test{:,goal_var_index},'o', 1:height(test),pred,'x');
    end
end

function crimes = setup()
    % - add `common` subdir to path
    addpath('./common');
    
    % - read, prepare and partition our data set
    filepath = '../data/community-crimes/communitycrimes_normalized.txt';
    crimes = readtable(filepath, 'Delimiter',',', 'ReadVariableNames',false);
    %crimes = standardizeMissing(crimes,{'?'});

    % - name table variables
    % -- communityname: Community name - not predictive - for information only (string)
    crimes.Properties.VariableNames{'Var1'} = 'communityname';
    % -- state: US state (by 2 letter postal abbreviation)(nominal)
    crimes.Properties.VariableNames{'Var2'} = 'state';
    % -- countyCode: numeric code for county - not predictive, and many missing values (numeric)
    crimes.Properties.VariableNames{'Var3'} = 'countyCode';
    % -- communityCode: numeric code for community - not predictive and many missing values (numeric)
    crimes.Properties.VariableNames{'Var4'} = 'communityCode';
    % -- fold: fold number for non-random 10 fold cross validation, potentially useful for debugging, paired tests - not predictive (numeric - integer)
    crimes.Properties.VariableNames{'Var5'} = 'fold';
    % -- population: population for community: (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var6'} = 'population';
    % -- householdsize: mean people per household (numeric - decimal)
    crimes.Properties.VariableNames{'Var7'} = 'householdsize';
    % -- racepctblack: percentage of population that is african american (numeric - decimal)
    crimes.Properties.VariableNames{'Var8'} = 'racepctblack';
    % -- racePctWhite: percentage of population that is caucasian (numeric - decimal)
    crimes.Properties.VariableNames{'Var9'} = 'racePctWhite';
    % -- racePctAsian: percentage of population that is of asian heritage (numeric - decimal)
    crimes.Properties.VariableNames{'Var10'} = 'racePctAsian';
    % -- racePctHisp: percentage of population that is of hispanic heritage (numeric - decimal)
    crimes.Properties.VariableNames{'Var11'} = 'racePctHisp';
    % -- agePct12t21: percentage of population that is 12-21 in age (numeric - decimal)
    crimes.Properties.VariableNames{'Var12'} = 'agePct12t21';
    % -- agePct12t29: percentage of population that is 12-29 in age (numeric - decimal)
    crimes.Properties.VariableNames{'Var13'} = 'agePct12t29';
    % -- agePct16t24: percentage of population that is 16-24 in age (numeric - decimal)
    crimes.Properties.VariableNames{'Var14'} = 'agePct16t24';
    % -- agePct65up: percentage of population that is 65 and over in age (numeric - decimal)
    crimes.Properties.VariableNames{'Var15'} = 'agePct65up';
    % -- numbUrban: number of people living in areas classified as urban (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var16'} = 'numbUrban';
    % -- pctUrban: percentage of people living in areas classified as urban (numeric - decimal)
    crimes.Properties.VariableNames{'Var17'} = 'pctUrban';
    % -- medIncome: median household income (numeric - may be integer)
    crimes.Properties.VariableNames{'Var18'} = 'medIncome';
    % -- pctWWage: percentage of households with wage or salary income in 1989 (numeric - decimal)
    crimes.Properties.VariableNames{'Var19'} = 'pctWWage';
    % -- pctWFarmSelf: percentage of households with farm or self employment income in 1989 (numeric - decimal)
    crimes.Properties.VariableNames{'Var20'} = 'pctWFarmSelf';
    % -- pctWInvInc: percentage of households with investment / rent income in 1989 (numeric - decimal)
    crimes.Properties.VariableNames{'Var21'} = 'pctWInvInc';
    % -- pctWSocSec: percentage of households with social security income in 1989 (numeric - decimal)
    crimes.Properties.VariableNames{'Var22'} = 'pctWSocSec';
    % -- pctWPubAsst: percentage of households with public assistance income in 1989 (numeric - decimal)
    crimes.Properties.VariableNames{'Var23'} = 'pctWPubAsst';
    % -- pctWRetire: percentage of households with retirement income in 1989 (numeric - decimal)
    crimes.Properties.VariableNames{'Var24'} = 'pctWRetire';
    % -- medFamInc: median family income (differs from household income for non-family households) (numeric - may be integer)
    crimes.Properties.VariableNames{'Var25'} = 'medFamInc';
    % -- perCapInc: per capita income (numeric - decimal)
    crimes.Properties.VariableNames{'Var26'} = 'perCapInc';
    % -- whitePerCap: per capita income for caucasians (numeric - decimal)
    crimes.Properties.VariableNames{'Var27'} = 'whitePerCap';
    % -- blackPerCap: per capita income for african americans (numeric - decimal)
    crimes.Properties.VariableNames{'Var28'} = 'blackPerCap';
    % -- indianPerCap: per capita income for native americans (numeric - decimal)
    crimes.Properties.VariableNames{'Var29'} = 'indianPerCap';
    % -- AsianPerCap: per capita income for people with asian heritage (numeric - decimal)
    crimes.Properties.VariableNames{'Var30'} = 'AsianPerCap';
    % -- OtherPerCap: per capita income for people with 'other' heritage (numeric - decimal)
    crimes.Properties.VariableNames{'Var31'} = 'OtherPerCap';
    % -- HispPerCap: per capita income for people with hispanic heritage (numeric - decimal)
    crimes.Properties.VariableNames{'Var32'} = 'HispPerCap';
    % -- NumUnderPov: number of people under the poverty level (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var33'} = 'NumUnderPov';
    % -- PctPopUnderPov: percentage of people under the poverty level (numeric - decimal)
    crimes.Properties.VariableNames{'Var34'} = 'PctPopUnderPov';
    % -- PctLess9thGrade: percentage of people 25 and over with less than a 9th grade education (numeric - decimal)
    crimes.Properties.VariableNames{'Var35'} = 'PctLess9thGrade';
    % -- PctNotHSGrad: percentage of people 25 and over that are not high school graduates (numeric - decimal)
    crimes.Properties.VariableNames{'Var36'} = 'PctNotHSGrad';
    % -- PctBSorMore: percentage of people 25 and over with a bachelors degree or higher education (numeric - decimal)
    crimes.Properties.VariableNames{'Var37'} = 'PctBSorMore';
    % -- PctUnemployed: percentage of people 16 and over, in the labor force, and unemployed (numeric - decimal)
    crimes.Properties.VariableNames{'Var38'} = 'PctUnemployed';
    % -- PctEmploy: percentage of people 16 and over who are employed (numeric - decimal)
    crimes.Properties.VariableNames{'Var39'} = 'PctEmploy';
    % -- PctEmplManu: percentage of people 16 and over who are employed in manufacturing (numeric - decimal)
    crimes.Properties.VariableNames{'Var40'} = 'PctEmplManu';
    % -- PctEmplProfServ: percentage of people 16 and over who are employed in professional services (numeric - decimal)
    crimes.Properties.VariableNames{'Var41'} = 'PctEmplProfServ';
    % -- PctOccupManu: percentage of people 16 and over who are employed in manufacturing (numeric - decimal)
    crimes.Properties.VariableNames{'Var42'} = 'PctOccupManu';
    % -- PctOccupMgmtProf: percentage of people 16 and over who are employed in management or professional occupations (numeric - decimal)
    crimes.Properties.VariableNames{'Var43'} = 'PctOccupMgmtProf';
    % -- MalePctDivorce: percentage of males who are divorced (numeric - decimal)
    crimes.Properties.VariableNames{'Var44'} = 'MalePctDivorce';
    % -- MalePctNevMarr: percentage of males who have never married (numeric - decimal)
    crimes.Properties.VariableNames{'Var45'} = 'MalePctNevMarr';
    % -- FemalePctDiv: percentage of females who are divorced (numeric - decimal)
    crimes.Properties.VariableNames{'Var46'} = 'FemalePctDiv';
    % -- TotalPctDiv: percentage of population who are divorced (numeric - decimal)
    crimes.Properties.VariableNames{'Var47'} = 'TotalPctDiv';
    % -- PersPerFam: mean number of people per family (numeric - decimal)
    crimes.Properties.VariableNames{'Var48'} = 'PersPerFam';
    % -- PctFam2Par: percentage of families (with kids) that are headed by two parents (numeric - decimal)
    crimes.Properties.VariableNames{'Var49'} = 'PctFam2Par';
    % -- PctKids2Par: percentage of kids in family housing with two parents (numeric - decimal)
    crimes.Properties.VariableNames{'Var50'} = 'PctKids2Par';
    % -- PctYoungKids2Par: percent of kids 4 and under in two parent households (numeric - decimal)
    crimes.Properties.VariableNames{'Var51'} = 'PctYoungKids2Par';
    % -- PctTeen2Par: percent of kids age 12-17 in two parent households (numeric - decimal)
    crimes.Properties.VariableNames{'Var52'} = 'PctTeen2Par';
    % -- PctWorkMomYoungKids: percentage of moms of kids 6 and under in labor force (numeric - decimal)
    crimes.Properties.VariableNames{'Var53'} = 'PctWorkMomYoungKids';
    % -- PctWorkMom: percentage of moms of kids under 18 in labor force (numeric - decimal)
    crimes.Properties.VariableNames{'Var54'} = 'PctWorkMom';
    % -- NumKidsBornNeverMar: number of kids born to never married (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var55'} = 'NumKidsBornNeverMar';
    % -- PctKidsBornNeverMar: percentage of kids born to never married (numeric - decimal)
    crimes.Properties.VariableNames{'Var56'} = 'PctKidsBornNeverMar';
    % -- NumImmig: total number of people known to be foreign born (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var57'} = 'NumImmig';
    % -- PctImmigRecent: percentage of _immigrants_ who immigated within last 3 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var58'} = 'PctImmigRecent';
    % -- PctImmigRec5: percentage of _immigrants_ who immigated within last 5 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var59'} = 'PctImmigRec5';
    % -- PctImmigRec8: percentage of _immigrants_ who immigated within last 8 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var60'} = 'PctImmigRec8';
    % -- PctImmigRec10: percentage of _immigrants_ who immigated within last 10 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var61'} = 'PctImmigRec10';
    % -- PctRecentImmig: percent of _population_ who have immigrated within the last 3 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var62'} = 'PctRecentImmig';
    % -- PctRecImmig5: percent of _population_ who have immigrated within the last 5 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var63'} = 'PctRecImmig5';
    % -- PctRecImmig8: percent of _population_ who have immigrated within the last 8 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var64'} = 'PctRecImmig8';
    % -- PctRecImmig10: percent of _population_ who have immigrated within the last 10 years (numeric - decimal)
    crimes.Properties.VariableNames{'Var65'} = 'PctRecImmig10';
    % -- PctSpeakEnglOnly: percent of people who speak only English (numeric - decimal)
    crimes.Properties.VariableNames{'Var66'} = 'PctSpeakEnglOnly';
    % -- PctNotSpeakEnglWell: percent of people who do not speak English well (numeric - decimal)
    crimes.Properties.VariableNames{'Var67'} = 'PctNotSpeakEnglWell';
    % -- PctLargHouseFam: percent of family households that are large (6 or more) (numeric - decimal)
    crimes.Properties.VariableNames{'Var68'} = 'PctLargHouseFam';
    % -- PctLargHouseOccup: percent of all occupied households that are large (6 or more people) (numeric - decimal)
    crimes.Properties.VariableNames{'Var69'} = 'PctLargHouseOccup';
    % -- PersPerOccupHous: mean persons per household (numeric - decimal)
    crimes.Properties.VariableNames{'Var70'} = 'PersPerOccupHous';
    % -- PersPerOwnOccHous: mean persons per owner occupied household (numeric - decimal)
    crimes.Properties.VariableNames{'Var71'} = 'PersPerOwnOccHous';
    % -- PersPerRentOccHous: mean persons per rental household (numeric - decimal)
    crimes.Properties.VariableNames{'Var72'} = 'PersPerRentOccHous';
    % -- PctPersOwnOccup: percent of people in owner occupied households (numeric - decimal)
    crimes.Properties.VariableNames{'Var73'} = 'PctPersOwnOccup';
    % -- PctPersDenseHous: percent of persons in dense housing (more than 1 person per room) (numeric - decimal)
    crimes.Properties.VariableNames{'Var74'} = 'PctPersDenseHous';
    % -- PctHousLess3BR: percent of housing units with less than 3 bedrooms (numeric - decimal)
    crimes.Properties.VariableNames{'Var75'} = 'PctHousLess3BR';
    % -- MedNumBR: median number of bedrooms (numeric - decimal)
    crimes.Properties.VariableNames{'Var76'} = 'MedNumBR';
    % -- HousVacant: number of vacant households (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var77'} = 'HousVacant';
    % -- PctHousOccup: percent of housing occupied (numeric - decimal)
    crimes.Properties.VariableNames{'Var78'} = 'PctHousOccup';
    % -- PctHousOwnOcc: percent of households owner occupied (numeric - decimal)
    crimes.Properties.VariableNames{'Var79'} = 'PctHousOwnOcc';
    % -- PctVacantBoarded: percent of vacant housing that is boarded up (numeric - decimal)
    crimes.Properties.VariableNames{'Var80'} = 'PctVacantBoarded';
    % -- PctVacMore6Mos: percent of vacant housing that has been vacant more than 6 months (numeric - decimal)
    crimes.Properties.VariableNames{'Var81'} = 'PctVacMore6Mos';
    % -- MedYrHousBuilt: median year housing units built (numeric - may be integer)
    crimes.Properties.VariableNames{'Var82'} = 'MedYrHousBuilt';
    % -- PctHousNoPhone: percent of occupied housing units without phone (in 1990, this was rare!) (numeric - decimal)
    crimes.Properties.VariableNames{'Var83'} = 'PctHousNoPhone';
    % -- PctWOFullPlumb: percent of housing without complete plumbing facilities (numeric - decimal)
    crimes.Properties.VariableNames{'Var84'} = 'PctWOFullPlumb';
    % -- OwnOccLowQuart: owner occupied housing - lower quartile value (numeric - decimal)
    crimes.Properties.VariableNames{'Var85'} = 'OwnOccLowQuart';
    % -- OwnOccMedVal: owner occupied housing - median value (numeric - decimal)
    crimes.Properties.VariableNames{'Var86'} = 'OwnOccMedVal';
    % -- OwnOccHiQuart: owner occupied housing - upper quartile value (numeric - decimal)
    crimes.Properties.VariableNames{'Var87'} = 'OwnOccHiQuart';
    % -- OwnOccQrange: owner occupied housing - difference between upper quartile and lower quartile values (numeric - decimal)
    crimes.Properties.VariableNames{'Var88'} = 'OwnOccQrange';
    % -- RentLowQ: rental housing - lower quartile rent (numeric - decimal)
    crimes.Properties.VariableNames{'Var89'} = 'RentLowQ';
    % -- RentMedian: rental housing - median rent (Census variable H32B from file STF1A) (numeric - decimal)
    crimes.Properties.VariableNames{'Var90'} = 'RentMedian';
    % -- RentHighQ: rental housing - upper quartile rent (numeric - decimal)
    crimes.Properties.VariableNames{'Var91'} = 'RentHighQ';
    % -- RentQrange: rental housing - difference between upper quartile and lower quartile rent (numeric - decimal)
    crimes.Properties.VariableNames{'Var92'} = 'RentQrange';
    % -- MedRent: median gross rent (Census variable H43A from file STF3A - includes utilities) (numeric - decimal)
    crimes.Properties.VariableNames{'Var93'} = 'MedRent';
    % -- MedRentPctHousInc: median gross rent as a percentage of household income (numeric - decimal)
    crimes.Properties.VariableNames{'Var94'} = 'MedRentPctHousInc';
    % -- MedOwnCostPctInc: median owners cost as a percentage of household income - for owners with a mortgage (numeric - decimal)
    crimes.Properties.VariableNames{'Var95'} = 'MedOwnCostPctInc';
    % -- MedOwnCostPctIncNoMtg: median owners cost as a percentage of household income - for owners without a mortgage (numeric - decimal)
    crimes.Properties.VariableNames{'Var96'} = 'MedOwnCostPctIncNoMtg';
    % -- NumInShelters: number of people in homeless shelters (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var97'} = 'NumInShelters';
    % -- NumStreet: number of homeless people counted in the street (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var98'} = 'NumStreet';
    % -- PctForeignBorn: percent of people foreign born (numeric - decimal)
    crimes.Properties.VariableNames{'Var99'} = 'PctForeignBorn';
    % -- PctBornSameState: percent of people born in the same state as currently living (numeric - decimal)
    crimes.Properties.VariableNames{'Var100'} = 'PctBornSameState';
    % -- PctSameHouse85: percent of people living in the same house as in 1985 (5 years before) (numeric - decimal)
    crimes.Properties.VariableNames{'Var101'} = 'PctSameHouse85';
    % -- PctSameCity85: percent of people living in the same city as in 1985 (5 years before) (numeric - decimal)
    crimes.Properties.VariableNames{'Var102'} = 'PctSameCity85';
    % -- PctSameState85: percent of people living in the same state as in 1985 (5 years before) (numeric - decimal)
    crimes.Properties.VariableNames{'Var103'} = 'PctSameState85';
    % -- LemasSwornFT: number of sworn full time police officers (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var104'} = 'LemasSwornFT';
    % -- LemasSwFTPerPop: sworn full time police officers per 100K population (numeric - decimal)
    crimes.Properties.VariableNames{'Var105'} = 'LemasSwFTPerPop';
    % -- LemasSwFTFieldOps: number of sworn full time police officers in field operations (on the street as opposed to administrative etc) (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var106'} = 'LemasSwFTFieldOps';
    % -- LemasSwFTFieldPerPop: sworn full time police officers in field operations (on the street as opposed to administrative etc) per 100K population (numeric - decimal)
    crimes.Properties.VariableNames{'Var107'} = 'LemasSwFTFieldPerPop';
    % -- LemasTotalReq: total requests for police (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var108'} = 'LemasTotalReq';
    % -- LemasTotReqPerPop: total requests for police per 100K popuation (numeric - decimal)
    crimes.Properties.VariableNames{'Var109'} = 'LemasTotReqPerPop';
    % -- PolicReqPerOffic: total requests for police per police officer (numeric - decimal)
    crimes.Properties.VariableNames{'Var110'} = 'PolicReqPerOffic';
    % -- PolicPerPop: police officers per 100K population (numeric - decimal)
    crimes.Properties.VariableNames{'Var111'} = 'PolicPerPop';
    % -- RacialMatchCommPol: a measure of the racial match between the community and the police force. High values indicate proportions in community and police force are similar (numeric - decimal)
    crimes.Properties.VariableNames{'Var112'} = 'RacialMatchCommPol';
    % -- PctPolicWhite: percent of police that are caucasian (numeric - decimal)
    crimes.Properties.VariableNames{'Var113'} = 'PctPolicWhite';
    % -- PctPolicBlack: percent of police that are african american (numeric - decimal)
    crimes.Properties.VariableNames{'Var114'} = 'PctPolicBlack';
    % -- PctPolicHisp: percent of police that are hispanic (numeric - decimal)
    crimes.Properties.VariableNames{'Var115'} = 'PctPolicHisp';
    % -- PctPolicAsian: percent of police that are asian (numeric - decimal)
    crimes.Properties.VariableNames{'Var116'} = 'PctPolicAsian';
    % -- PctPolicMinor: percent of police that are minority of any kind (numeric - decimal)
    crimes.Properties.VariableNames{'Var117'} = 'PctPolicMinor';
    % -- OfficAssgnDrugUnits: number of officers assigned to special drug units (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var118'} = 'OfficAssgnDrugUnits';
    % -- NumKindsDrugsSeiz: number of different kinds of drugs seized (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var119'} = 'NumKindsDrugsSeiz';
    % -- PolicAveOTWorked: police average overtime worked (numeric - decimal)
    crimes.Properties.VariableNames{'Var120'} = 'PolicAveOTWorked';
    % -- LandArea: land area in square miles (numeric - decimal)
    crimes.Properties.VariableNames{'Var121'} = 'LandArea';
    % -- PopDens: population density in persons per square mile (numeric - decimal)
    crimes.Properties.VariableNames{'Var122'} = 'PopDens';
    % -- PctUsePubTrans: percent of people using public transit for commuting (numeric - decimal)
    crimes.Properties.VariableNames{'Var123'} = 'PctUsePubTrans';
    % -- PolicCars: number of police cars (numeric - expected to be integer)
    crimes.Properties.VariableNames{'Var124'} = 'PolicCars';
    % -- PolicOperBudg: police operating budget (numeric - may be integer)
    crimes.Properties.VariableNames{'Var125'} = 'PolicOperBudg';
    % -- LemasPctPolicOnPatr: percent of sworn full time police officers on patrol (numeric - decimal)
    crimes.Properties.VariableNames{'Var126'} = 'LemasPctPolicOnPatr';
    % -- LemasGangUnitDeploy: gang unit deployed (numeric - integer - but really nominal - 0 means NO, 10 means YES, 5 means Part Time)
    crimes.Properties.VariableNames{'Var127'} = 'LemasGangUnitDeploy';
    % -- LemasPctOfficDrugUn: percent of officers assigned to drug units (numeric - decimal)
    crimes.Properties.VariableNames{'Var128'} = 'LemasPctOfficDrugUn';
    % -- PolicBudgPerPop: police operating budget per population (numeric - decimal)
    crimes.Properties.VariableNames{'Var129'} = 'PolicBudgPerPop';
    % -- murders: number of murders in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var130'} = 'murders';
    % -- murdPerPop: number of murders per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var131'} = 'murdPerPop';
    % -- rapes: number of rapes in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var132'} = 'rapes';
    % -- rapesPerPop: number of rapes per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var133'} = 'rapesPerPop';
    % -- robberies: number of robberies in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var134'} = 'robberies';
    % -- robbbPerPop: number of robberies per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var135'} = 'robbbPerPop';
    % -- assaults: number of assaults in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var136'} = 'assaults';
    % -- assaultPerPop: number of assaults per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var137'} = 'assaultPerPop';
    % -- burglaries: number of burglaries in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var138'} = 'burglaries';
    % -- burglPerPop: number of burglaries per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var139'} = 'burglPerPop';
    % -- larcenies: number of larcenies in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var140'} = 'larcenies';
    % -- larcPerPop: number of larcenies per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var141'} = 'larcPerPop';
    % -- autoTheft: number of auto thefts in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var142'} = 'autoTheft';
    % -- autoTheftPerPop: number of auto thefts per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var143'} = 'autoTheftPerPop';
    % -- arsons: number of arsons in 1995 (numeric - expected to be integer) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var144'} = 'arsons';
    % -- arsonsPerPop: number of arsons per 100K population (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var145'} = 'arsonsPerPop';
    % -- ViolentCrimesPerPop: total number of violent crimes per 100K popuation (numeric - decimal) GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var146'} = 'ViolentCrimesPerPop';
    % -- nonViolPerPop: total number of non-violent crimes per 100K popuation (numeric - decimal) potential GOAL attribute (to be predicted)
    crimes.Properties.VariableNames{'Var147'} = 'nonViolPerPop';
end