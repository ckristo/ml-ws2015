
%%
% housing().
%
function housing()
    % - fetch data set
    data = setup();
    
    % - fix the random seed value (for reproducable results)
    rng('default');
    rng(42);
    
    % - define some constants
    %  * the name of the goal variable in the data table
    goal_var_name = 'MEDV';               
    %  * the index of the goal variable in the data table
    goal_var_index = 14;
    %  * define which features to standardize
    features_to_normalize = [ 1:3 5:13 ];
    %  * define the normalization function
    normalizefun = @scale_features; % @scale_features, @standardize_features or 'none'
    
    % print summary for dataset
    %print_summary(data, 'Housing');
    
    % - linear regression -----------------------
    t = tic;
    evals = crossval(@linear_regression, data);
    t_total = toc(t);
    fprintf('Linear Regression\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - regression tree -------------------------
    t = tic;
    evals = crossval(@regression_tree, data);
    t_total = toc(t);
    fprintf('Regression Tree\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - random forests --------------------------
    t = tic;
    evals = crossval(@random_forests, data);
    t_total = toc(t);
    fprintf('Random Forests\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - SVM -------------------------------------
    t = tic;
    evals = crossval(@svm, data);
    t_total = toc(t);
    fprintf('SVM\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');

    %%
    % Applies feature standardization.
    %
    function [ tbl_X_train, tbl_X_test ] = apply_feature_normalization(tbl_X_train, tbl_X_test)
        if isa(normalizefun, 'function_handle')
            % - apply normalization to training set
            [ stdvals, X_mean, X_sd ] = normalizefun(tbl_X_train{:, features_to_normalize});
            tbl_X_train(:, features_to_normalize) = array2table(stdvals);
            % - apply normalization to test set
            stdvals = normalizefun(tbl_X_test{:, features_to_normalize}, X_mean, X_sd);
            tbl_X_test(:, features_to_normalize) = array2table( stdvals );
        end
    end

    %%
    % Performs linear regression training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = linear_regression(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        lrmodel = fitlm(train, 'linear', 'ResponseVar',goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(lrmodel, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs regression tree training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = regression_tree(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrtree(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs random forests training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = random_forests(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        tree = templateTree();
        model = fitensemble(train, goal_var_name, 'Bag', 50, tree, 'type','regression');
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs SVM training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = svm(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrsvm(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
        plot(1:height(test),test{:,goal_var_index},'o', 1:height(test),pred,'x');
    end
end

function data = setup()
    % - add `common` subdir to path
    addpath('./common');
    
    % - read, prepare and partition our data set
    filepath = '../data/housing/housing.dat';
    data = readtable(filepath, ...
        'Format','%f%f%f%C%f%f%f%f%f%f%f%f%f%f', 'Delimiter','space', 'ReadVariableNames',false);
    
    % 1. CRIM      per capita crime rate by town
    data.Properties.VariableNames{'Var1'} = 'CRIM';
    
    % 2. ZN        proportion of residential land zoned for lots over 
    %             25,000 sq.ft.
    data.Properties.VariableNames{'Var2'} = 'ZN';
    
    % 3. INDUS     proportion of non-retail business acres per town
    data.Properties.VariableNames{'Var3'} = 'INDUS';
    
    % 4. CHAS      Charles River dummy variable (= 1 if tract bounds 
    %            river; 0 otherwise)
    data.Properties.VariableNames{'Var4'} = 'CHAS';
    
    % 5. NOX       nitric oxides concentration (parts per 10 million)
    data.Properties.VariableNames{'Var5'} = 'NOX';
    
    % 6. RM        average number of rooms per dwelling
    data.Properties.VariableNames{'Var6'} = 'RM';
    
    % 7. AGE       proportion of owner-occupied units built prior to 1940
    data.Properties.VariableNames{'Var7'} = 'AGE';
    
    % 8. DIS       weighted distances to five Boston employment centres
    data.Properties.VariableNames{'Var8'} = 'DIS';
    
    % 9. RAD       index of accessibility to radial highways
    data.Properties.VariableNames{'Var9'} = 'RAD';
    
    % 10. TAX      full-value property-tax rate per $10,000
    data.Properties.VariableNames{'Var10'} = 'TAX';
    
    % 11. PTRATIO  pupil-teacher ratio by town
    data.Properties.VariableNames{'Var11'} = 'PTRATIO';
    
    % 12. B        1000(Bk - 0.63)^2 where Bk is the proportion of blacks 
    %              by town
    data.Properties.VariableNames{'Var12'} = 'B';
    
    % 13. LSTAT    % lower status of the population
    data.Properties.VariableNames{'Var13'} = 'LSTAT';
    
    % 14. MEDV     Median value of owner-occupied homes in $1000's
    data.Properties.VariableNames{'Var14'} = 'MEDV';
end
