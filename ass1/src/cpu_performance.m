
%%
% cpu_performance().
%
function cpu_performance()
    
    % - add `common` subdir to path
    addpath('./common');

    % - read, prepare and partition our data set
    data = readtable('../data/cpu-performance/machine.dat', ...
        'Format','%C%C%f%f%f%f%f%f%f%f', 'Delimiter',',');
    data = data(:,1:end-1); % strip of estimated relative performance 
    
    % - fix the random seed value (for reproducable results)
    rng('default');
    rng(42);
    
    % - define some constants
    %  * the name of the goal variable in the data table
    goal_var_name = 'PRP';               
    %  * the index of the goal variable in the data table
    goal_var_index = 9;
    %  * define which features to standardize
    features_to_normalize = 3:8;
    %  * define the normalization function
    normalizefun = @scale_features; % @scale_features, @standardize_features or 'none'
    
    % print summary for dataset
    %print_summary(data, 'CPU-Performance');

    % - linear regression -----------------------
    t = tic;
    evals = crossval(@linear_regression, data);
    t_total = toc(t);
    fprintf('Linear Regression\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');

    % - regression tree -------------------------
    t = tic;
    evals = crossval(@regression_tree, data);
    t_total = toc(t);
    fprintf('Regression Tree\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - random forests --------------------------
    t = tic;
    evals = crossval(@random_forests, data);
    t_total = toc(t);
    fprintf('Random Forests\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - SVM -------------------------------------
    t = tic;
    evals = crossval(@svm, data);
    t_total = toc(t);
    fprintf('SVM\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    %%
    % Applies feature standardization.
    %
    function [ tbl_X_train, tbl_X_test ] = apply_feature_normalization(tbl_X_train, tbl_X_test)
        if isa(normalizefun, 'function_handle')
            % - apply normalization to training set
            [ stdvals, X_mean, X_sd ] = normalizefun(tbl_X_train{:, features_to_normalize});
            tbl_X_train(:, features_to_normalize) = array2table(stdvals);
            % - apply normalization to test set
            stdvals = normalizefun(tbl_X_test{:, features_to_normalize}, X_mean, X_sd);
            tbl_X_test(:, features_to_normalize) = array2table( stdvals );
        end
    end
    
    %%
    % Performs linear regression training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = linear_regression(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitlm(train, 'linear', 'ResponseVar',goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
         % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs regression tree training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = regression_tree(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrtree(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
         % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs random forests training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = random_forests(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitensemble(train, goal_var_name, 'Bag', 50, 'Tree', 'type','regression');
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
         % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs SVM training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = svm(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrsvm(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
         % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end
    
end
