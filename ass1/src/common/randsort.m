%%
% Applies a random permutation to a table `T`.
%  - `T` the table
%  - `seed` the seed for reproducable random permutations (defaults to 0).
%
function T_rand = randsort(T, seed)
    if nargin > 2
       error('randperm(): too many arguments.') 
    elseif nargin == 0
        error('randperm(): requires at least one argument')
    elseif nargin == 1
        seed = 0;
    end
    rand = RandStream('mt19937ar', 'Seed',seed);
    T_rand = T(randperm(rand, height(T)), :);
end
