%%
% Replaces all NaNs in the given columns with the mean of that column.
%  - `data` the data to work on
%  - `column_indices` the columns containing NaNs that should be replaced
% Returns:
%  - the data with replaced NaNs
function data = nans_to_means(data, column_indices)
    nan_means = nanmean(data{:,column_indices});
    
    j = 1;
    for i = column_indices
        temp_col = data{:,i};
        temp_col(isnan(temp_col)) = nan_means(j);
        
        data{:,i} = temp_col;
        j = j + 1;
    end
end