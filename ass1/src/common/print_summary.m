%%
% Prints a summary of the statistical figures of a dataset
%
function print_summary(data, name)
    fprintf('%s ::\n', name);
    fprintf('- Number of samples:\t%d\n', size(data, 1));
    fprintf('- Number of features:\t%d\n', size(data, 2));
    fprintf('- Features:\n');
    % - print summary for each feature
    for i = 1:size(data, 2)
        c = data{:, i};
        fprintf('  * `%s` ', char(data.Properties.VariableNames(i)));
        if isnumeric(c)
            fprintf('(numeric)');
            % - check for NaN values
            nans = sum(isnan(c));
            if nans > 0
                fprintf(' [%dxNaN]', nans);
            end
            fprintf('\n');
            fprintf('    - min  = %f\n', nanmin(data{:, i}));
            fprintf('    - max  = %f\n', nanmax(data{:, i}));
            fprintf('    - mean = %f\n', nanmean(data{:, i}));
            fprintf('    - sd   = %f\n', nanstd(data{:, i}));
        else
            fprintf('(categorical)\n');
            uc = unique(c);
            for j = 1:size(uc)
                fprintf('    - %s\n', char(uc(j)));
            end
        end
    end
end
