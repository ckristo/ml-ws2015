%%
% Computes the mean square error of two numeric vectors.
% Based on http://www.mathworks.com/matlabcentral/fileexchange/21383-rmse/content/rmse.m
%
function m = mse(data, estimate)
    I = ~isnan(data) & ~isnan(estimate); 
    data = data(I); estimate = estimate(I);
    m = sum((data(:)-estimate(:)).^2)/numel(data);
end
