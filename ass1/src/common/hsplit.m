%%
% Horizontally splits a table into two parts.
%  - `T` the table to split
%  - `ratio` the ratio for the split (e.g. 0.8 = 80% of the rows in T are in T1, the rest in T2)
%
function [T1, T2] = hsplit(T, ratio)
    if nargin ~= 2
        error('split(): requires exactly two arguments')
    end
    T1 = T( 1:floor(height(T) * ratio), : );
    T2  = T( (floor(height(T) * ratio)+1):end, : );
end
