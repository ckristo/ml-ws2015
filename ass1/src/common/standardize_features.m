%%
% Performs feature standardization of a vector of numeric features
% using the Z-Score Standardization method.
%  - `X` the column vector with the feature values
%  - `X_mean` (optional) the mean value(s) used for standardization
%  - `X_sd` (optional) the standard deviation value(s) used for standardization
% Returns:
%  - the vector with the scaled features
%  - `X_mean` calculated (only if `X_mean` was not provided as input param)
%  - `X_sd` calculated (only if `X_sd` was not provided as input param)
function [Y, varargout] = standardize_features(X, X_mean, X_sd)
    % - argument checking
    if nargin > 3
       error('standardize_features(): too many arguments.')
    elseif nargin == 0
        error('standardize_features(): requires at least one argument')
    elseif nargin == 1
        % - determine mean and sd
        X_mean = nanmean(X, 1);
        X_sd   = nanstd(X, 0, 1);
        varargout{1} = X_mean;
        varargout{2} = X_sd;
    elseif nargin == 2
        error('standardize_features(): you need to specify both `X_mean` and `X_sd`')
    end
    % - check if feature vector is numeric
    if ~isnumeric(X)
       error('standardize_features(): `X` needs to contain numeric values only'); 
    end
    % - repeat min/max vector to the size of the original matrix
    X_mean = repmat(X_mean, size(X, 1), 1);
    X_sd = repmat(X_sd, size(X, 1), 1);
    % - perform standardization operation
    Y = double(X);
    Y = Y - X_mean;
    Y = Y ./ X_sd;
end
