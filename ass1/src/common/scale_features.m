%%
% Performs feature scaling of a vector of numeric features
% using the Min-Max Scaling method.
%  - `X` the column vector with the feature values
%  - `X_min` (optional) the minimum value(s) used for scaling
%  - `X_max` (optional) the maximum value(s) used for scaling
% Returns:
%  - the vector with the scaled features
%  - `X_min` calculated (only if `X_min` was not provided as input param)
%  - `X_max` calculated (only if `X_max` was not provided as input param)
function [Y, varargout] = scale_features(X, X_min, X_max)
    % - argument checking
    if nargin > 3
       error('scale_features(): too many arguments.')
    elseif nargin == 0
        error('scale_features(): requires at least one argument')
    elseif nargin == 1
        % - determine min and max
        X_min = min(X, [], 1);
        X_max = max(X, [], 1);
        varargout{1} = X_min;
        varargout{2} = X_max;
    elseif nargin == 2
        error('scale_features(): you need to specify both `X_min` and `X_max`')
    end
    % - check if feature vector is numeric
    if ~isnumeric(X)
       error('scale_features(): `X` needs to contain numeric values only'); 
    end
    % - repeat min/max vector to the size of the original matrix
    X_min_rep = repmat(X_min, size(X, 1), 1);
    X_max_rep = repmat(X_max, size(X, 1), 1);
    % - perform scaling operation
    Y = double(X);
    Y = Y - X_min_rep;
    Y = Y ./ (X_max_rep - X_min_rep);
end
