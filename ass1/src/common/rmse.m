%%
% Computes the root mean square error.
% Taken from http://www.mathworks.com/matlabcentral/fileexchange/21383-rmse/content/rmse.m
%
function r = rmse(data, estimate)
    I = ~isnan(data) & ~isnan(estimate); 
    data = data(I); estimate = estimate(I);
    r = sqrt(sum((data(:)-estimate(:)).^2)/numel(data));
end
