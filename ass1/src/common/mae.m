%%
% Computes the mean absolute scaled error of two numeric vectors.
% Based on http://www.mathworks.com/matlabcentral/fileexchange/21383-rmse/content/rmse.m
%
function m = mae(data, estimate)
    I = ~isnan(data) & ~isnan(estimate); 
    data = data(I); estimate = estimate(I);
    m = sum(abs(estimate(:) - data(:)))/numel(data);
end
