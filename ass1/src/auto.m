
%%
% auto().
%
function auto()
    % - fetch data set
    data = setup();
    
    % - fix the random seed value (for reproducable results)
    rng('default');
    rng(42);
    
    % - define some constants
    %  * the name of the goal variable in the data table
    goal_var_name = 'mpg';               
    %  * the index of the goal variable in the data table
    goal_var_index = 1;
    %  * define which features to standardize
    features_to_normalize = 3:6;
    %  * define the normalization function
    normalizefun = @scale_features; % @scale_features, @standardize_features or 'none'
    
    % print summary for dataset
    %print_summary(data, 'Auto');
    
    % - linear regression -----------------------
    t = tic;
    evals = crossval(@linear_regression, data);
    t_total = toc(t);
    fprintf('Linear Regression\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');

    % - regression tree -------------------------
    t = tic;
    evals = crossval(@regression_tree, data);
    t_total = toc(t);
    fprintf('Regression Tree\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - random forests --------------------------
    t = tic;
    evals = crossval(@random_forests, data);
    t_total = toc(t);
    fprintf('Random Forests\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');
    
    % - SVM -------------------------------------
    t = tic;
    evals = crossval(@svm, data);
    t_total = toc(t);
    fprintf('SVM\n');
    fprintf(' * time train    = %f sec\n', mean(evals(:,1)));
    fprintf(' * time predict  = %f sec\n', mean(evals(:,2)));
    fprintf(' * RMSE          = %f\n', mean(evals(:,3)));
    fprintf(' * MAE           = %f\n', mean(evals(:,4)));
    fprintf('\n');

    %%
    % Applies feature standardization.
    %
    function [ tbl_X_train, tbl_X_test ] = apply_feature_normalization(tbl_X_train, tbl_X_test)
        if isa(normalizefun, 'function_handle')
            % - apply normalization to training set
            [ stdvals, X_mean, X_sd ] = normalizefun(tbl_X_train{:, features_to_normalize});
            tbl_X_train(:, features_to_normalize) = array2table(stdvals);
            % - apply normalization to test set
            stdvals = normalizefun(tbl_X_test{:, features_to_normalize}, X_mean, X_sd);
            tbl_X_test(:, features_to_normalize) = array2table( stdvals );
        end
    end

    %%
    % Performs linear regression training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = linear_regression(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        lrmodel = fitlm(train, 'linear', 'ResponseVar',goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(lrmodel, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
        
        %plot_model(lrmodel);
        %plot_diff(test,pred);
        %plot(1:height(test),test{:,goal_var_index},'o', 1:height(test),pred,'x');
        %plot(1:height(test),test{:,goal_var_index},'o', 1:height(test),pred,'x');
        %plot(lrmodel);
    end

    %%
    % Performs regression tree training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = regression_tree(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrtree(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs random forests training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = random_forests(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        tree = templateTree();
        model = fitensemble(train, goal_var_name, 'Bag', 50, tree, 'type','regression');
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end

    %%
    % Performs SVM training and evaluation.
    % Returns a vector with evaluation measures:
    %  1. column: training time (in sec)
    %  2. column: test time (in sec)
    %  3. column: RSME
    %  4. column: MAE
    %
    function evals = svm(train, test)
        % - perform data normalization first 
        [ train, test ] = apply_feature_normalization(train, test);
        % - train model
        t1 = tic;
        model = fitrsvm(train, goal_var_name);
        t_train = toc(t1);
        % - predict test values
        t2 = tic;
        pred = predict(model, test);
        t_test = toc(t2);
        % - calculate accuarcy metrics
        vals = test{:,goal_var_index};
        %   * RMSE
        r = rmse(vals, pred);
        %   * MAE
        m = mae(vals, pred);
        % - create result col vector
        evals = [ t_train; t_test; r; m  ];
    end
end

function data = setup()
    % - add `common` subdir to path
    addpath('./common');
    
    % - read, prepare and partition our data set
    filepath = '../data/auto-mpg/auto-mpg.dat';
    data = readtable(filepath, ...
        'Format','%f%C%f%f%f%f%C%C', 'Delimiter','space', 'ReadVariableNames',false);
    
    % 1. mpg: continuous
    data.Properties.VariableNames{'Var1'} = 'mpg';
    
    % 2. cylinders: multi-valued discrete
    data.Properties.VariableNames{'Var2'} = 'cylinders';
    
    % 3. displacement: continuous
    data.Properties.VariableNames{'Var3'} = 'displacement';
    
    % 4. horsepower: continuous
    data.Properties.VariableNames{'Var4'} = 'horsepower';
    
    % 5. weight: continuous
    data.Properties.VariableNames{'Var5'} = 'weight';
    
    % 6. acceleration: continuous
    data.Properties.VariableNames{'Var6'} = 'acceleration';
    
    % 7. model year: multi-valued discrete
    data.Properties.VariableNames{'Var7'} = 'year';
    
    % 8. origin: multi-valued discrete
    data.Properties.VariableNames{'Var8'} = 'origin';
end