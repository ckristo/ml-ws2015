
\section*{Methodology}
\label{sec:methodology}

\subsection*{Software}

We chose \textsf{R} \cite{R} for this assignment because we wanted to explore its capabilities for performing machine learning experiments with it. After some initial research on \textsf{R} and some initial tries with stand-alone packages for machine learning (e.g. \texttt{class} \cite{CRAN-class}), we found the \texttt{caret} \cite{CRAN-caret, caret} package to be the perfect framework for the assignment.
\\

\texttt{caret} (short for \textbf{C}lassification \textbf{A}nd \textbf{RE}gression \textbf{T}raining) is a library that attempts to streamline the process of creating predictive machine learning models \cite{caret}. It provides a coherent interface for a lot of different machine learning algorithms (which are available as separate \textsf{R} packages), as well as tools for simplifying common machine learning tasks like data splitting, data normalization, data preprocessing, model tuning, and many more. Caret supports a huge set of different machine learning algorithms for classification and regression (cf. \cite{caret-modelList} for a full list).

\subsection*{Classifiers}

We decided to use the following classification algorithms for the assignment:
\begin{itemize}

	\item \textbf{k-Nearest-Neighbor}: (k-NN) a simple classification algorithm that assigns a class to a new sample based on its $k$ nearest neighbors in the training set. It belongs to the family of \textit{lazy learning} machine algorithms. For our experiments, we chose caret's \texttt{knn} \cite{caret-Prototype-Models} model which uses caret's own k-NN implementation \texttt{knn3} \cite{inside-R-docs-caret-knn3}. \texttt{knn3} uses the \textit{Euclidian distance} as distance measure.
	
	\item \textbf{Random Forests}: an \textit{ensemble of bootstrapped decision trees} where the class is determined by majority voting of all trees in the forest. We chose caret's \texttt{rf} \cite{caret-Random-Forest-Models} model which uses \texttt{randomForest} \cite{inside-R-docs-randomForest-randomForest} from the \textit{randomForest} \cite{CRAN-randomForest} package.
	
	\item \textbf{Support Vector Machine}: a \textit{large margin classifier} that tries to find an optimal decision boundary with maximum margin between it and the outermost samples of a class (the support vectors). We decided to use caret's \texttt{svmLinear}, \texttt{svmPoly} and \texttt{svmRadial} models \cite{caret-Support-Vector-Machines} which make use of \texttt{ksvm} \cite{inside-R-docs-kernlab-ksvm} from the \textit{kernlab} \cite{CRAN-kernlab} package.
	
	\item \textbf{Multilayer Perceptron}: (MLP) a \text{feed-forward artificial neural network} classifier consisting of perceptrons arranged in multiple layers. We decided to use caret's \texttt{mlp} and \texttt{mlpML} models \cite{caret-Neural-Network} which use \texttt{mlp} \cite{inside-R-docs-RSNNS-mlp} from the \textit{RSNNS} \cite{CRAN-RSNNS} package internally.
	
	\item \textbf{Naive Bayes}: a simple \textit{probabilistic classifier} which makes a decision based on probabilities calculated using the \textit{Bayes' theorem}, assuming independants between features. We picked caret's \texttt{nb} model \cite{caret-Bayesian-Model} which utilizes \texttt{NaiveBayes} \cite{inside-R-docs-klaR-NaiveBayes} from the \textit{klaR} \cite{CRAN-klaR} package.
	
\end{itemize}

We chose the above listed classification algorithms on the one hand to fulfil the requirement that at least three of the classifiers are from different families (as far as we see, they all come from different families) and also because we think that they are widely used, well known and interesting for comparison regarding classification performance and runtime performance for training and testing. One aspect of special intererst for us was if more complex classification algorithms (like the Multilayer Perceptron) achieve better results in general compared to simple ones (like the Naive Bayes).

\subsection*{Performance measures}

We decided to apply the following approaches and measures for comparing the performance of classifier in the course of our experiments:

\begin{itemize}
	\item \textbf{Confusion Matrix}: (or \textit{contingency table}) provides a good overview of a classifier's performance. The observed class labels are usually put across the top, where as the predicted class labels are down the side. We also follow this convention throughout this report.
	\\
	
	An example confusion matrix for a binary classification problem looks like the following:
	\begin{table}[h]
	%\resizebox{15cm}{!} {
		\begin{tabular}{|l|l|l|}
			\hline
			 & \textbf{Positive} & \textbf{Negative} \\
			\hline
			\textbf{Positive} & True Positive (TP) & False Positive (FP) \\
			\hline
			\textbf{Negative} & False Negative (FN) & True Negative (TN) \\
			\hline
		\end{tabular}
	%}
	\centering
	\caption{An exemplary confusion matrix}
	\label{tbl:confusionMatrixExample}
	\end{table}
	
	We use caret's \texttt{confusionMatrix} \cite{inside-R-docs-caret-confusionMatrix} function for generating the confusion matrix during evaluation process.
	
	\item \textbf{Accuracy}: refers to the number of correctly predicted samples in relation to the total number of samples. It is defined as follows:
	
	\begin{equation*}
		accuracy = \frac{TP + TN}{TP + FP + TN + FN} = \frac{TP + TN}{\# samples}
	\end{equation*}
	\vspace{0.1em}
	
	The problem with accuracy is that it is usually not enough information to estimate the quality for a model. For example, if you have a data set with 100 samples, of which 80 are positive samples, and a classifier that always assigns the class "positive", the classifier would have a very good accuracy but would also provide a terrible model.
	\\
	
	We tried to find an \textsf{R} package for calculating accuracy and other performance metrics, but could not find one that fitted our needs, so we decided to implement a function for, cf. \texttt{calculatePerformanceMeasures}\footnote{\url{https://bitbucket.org/ckristo/ml-ws2015/src/master/ass2/src/R/common.R\#common.R-307}} \cite{Bitbucket-repo}
	
	\item \textbf{Precision}: the number of true positive predictions divided by the total number classes predicted as positive, and can be thought of as a measure of a classifier's exactness. A low precision can also indicate a large number of False Positives.  It is defined as follows:
	
	\begin{equation*}
		precision = \frac{TP}{TP + FP}
	\end{equation*}
	\vspace{0.1em}
	
	As with accuracy, this measure is calculated by our \texttt{calculatePerformanceMeasures} function.
	
	\item \textbf{Recall}: (sometimes also referred to as \textit{sensitivity}) is the number of true positive predictions divided by the number of classes labelled as positive in the test data, and can be thought of as a measure of a classifier's completeness. A low recall value indicates many False Negatives.  It is defined as follows:
	
	\begin{equation*}
		recall = \frac{TP}{TP + FN}
	\end{equation*}
	\vspace{0.1em}
	
	As with accuracy, this measure is calculated by our \texttt{calculatePerformanceMeasures} function.

	\item \textbf{F1 score}: weighted balance between recall and precision. It is defined as follows:
	
	\begin{equation*}
		f1score = \frac{2 * (precision * recall)}{precision + recall}
	\end{equation*}
	\vspace{0.1em}
	
	As with accuracy, this measure is calculated by our \texttt{calculatePerformanceMeasures} function.
	
	%\item \textbf{ROC Curve}: (\textit{receiver operating characteristic}) curve shows the relationship between the true positive rate and false positive rate for a binary classifier (in our case, this applies only to the voting\ref{sec:voting} data set).
	%\\
	%
	%ROC curves are plotted in a 2D graph, where the $y$ axis holds the true positive rate, and the $x$ axis the false positive rate. To compare classifiers, we look at the \textit{area under the curve (AUC)}. The values of the AUC lie within in the interval $[0,1]$. A random-guessing classifier would produce a line between $(0,0)$ and $(1,1)$, which yields an AUC of 0.5, so our aim is to score a value higher than that.
	% TODO: cite "would produce a line between (0,0), and (1,1)!
	% TODO: ROC Curve example
	
\end{itemize}

\subsection*{Data preprocessing}

\subsubsection*{Feature scaling}

Feature scaling is an important aspect of data preprocessing that normalizes the data to comparable scale. If not done, the accuracy and runtime performance of several machine learning algorithms may suffer. There are several approaches for feature scaling; we decided to apply two of them:

\begin{itemize}
	\item \textbf{Z-score standardization}: all features are scaled down to zero mean and unit variance, which results in values between $[-1, 1]$:
		\begin{equation*}
			x' = \frac{x - \bar{x}}{s}
		\end{equation*}
		where $\bar{x}$ is the mean of all values of the feature, and $s$ is the standard deviation.
	
	\item \textbf{Min/Max scaling}: all features are scaled to the fixed range $[0, 1]$:
		\begin{equation*}
			x' = \frac{x - x_{min}}{x_{max} - x_{min}}
		\end{equation*}
		where $x_{min}$ and $x_{max}$ are the minimum and maximum of all values of the feature.
\end{itemize}

Min/Max scaling has an influence on standard deviation of the data. Therefore, Z-score standardization should be preferred where standard deviation is important (e.g. when performing a \textit{Principal Component Analysis}).
\\

% TODO: above paragraph needs a citation, taken from http://sebastianraschka.com/Articles/2014_about_feature_scaling.html

We used caret's built-in preprocessing functionality which i.a. allows to apply feature scaling (via the \texttt{preProcess} argument for \texttt{train} \cite{inside-R-docs-caret-train}). We applied Z-score standardization (\verb+c("center", "scale")+) and Min/Max scaling (\verb+"range"+).

\subsubsection*{Missing values strategies}
\label{sec:handlingMissingValues}

% TODO: maybe write a few sentences about missing at random (mar), missing completely at random (mcar), missing not at random (mnar)

According to Acuna and Rodriguez\cite{paper-AcunaRodriguez-TreatmingOfMissingValuesAndItsEffect}, there are four different methods to deal with missing values:

\begin{itemize}

	\item \textbf{Case Deletion (CD)} (or \textit{complete case analysis}) is the simplest approach to tackle the problem of missing values, where all instances (cases) with missing values for at least one feature are deleted. A variation of this method is to determine the amount of missing values of each sample and attribute, and then remove those with a high percentage of missing values.
	\\
	
	While this is usually fine for samples (if the amount of samples to be removed is not too large), removing an entire attribute tends to be more critical (\textbf{citation needed}). Before deleting any attribute, it is necessary to evaluate its relevance to the analysis of the data set. If the attribute is indeed relevant, it should be kept regardless of the number of missing values.
	\\
	
	It is also important to determine whether the distribution of missing values is random, or if there is a certain pattern associated to it. The latter usually indicates a problem with the data gathering process which should be addressed.
	
	% TODO: "The latter usually ... which should be addressed" citation needed!

	\item \textbf{Mean Imputation} is one of the most frequently used methods, where the missing values for a given attribute are replaced with the mean of the present values for that attribute within the class the sample who is missing the attribute value belongs to.
	\\
	
	% TODO: "is one of the most frequently used methods" citation needed!
	
	This corresponds to the following formula:
	\begin{equation*}
		\widehat{x}_{ij} = \sum_{x_{ij} \in C_{k}} \frac{x_{ij}}{n_{k}}
	\end{equation*}
	\vspace{0.1em}

	where $\widehat{x}_{ij}$ is a missing value of class $C_{k}$, and $n_{k}$ represents the number of non-missing values in the j-th feature of the k-th class.

	% TODO: look up paper of Little and Rubin (2002) for drawbacks of mean imputation
	
	\item \textbf{Median Imputation} follows the same approach as Mean Imputation, but uses the median instead of the mean to replace missing values. This is more robust against outliers, and is also a recommended choice when the distribution of the values of a given feature is skewed \cite{paper-AcunaRodriguez-TreatmingOfMissingValuesAndItsEffect}.

	\begin{equation*}
		\widehat{x}_{ij} = median_{x_{ij} \in C_{k}} \lbrace x_{ij} \rbrace
	\end{equation*}
	\vspace{0.1em}
	
	where $\widehat{x}_{ij}$ is a missing value of class $C_{k}$.

	\item \textbf{Mode Imputation} is used instead of Mean or Median Imputation when dealing with categorical variables (also see section \ref{sec:voting}, where we had to deal with missing values of categoricals). The mode of a categorical (qualitative) variable is the most frequently observed value\cite{website-StatisticalLanguage-Mode}\cite{paper-Ramirez-MLPImputation}.

	\item \textbf{Imputation using a predictive model}: With this method, a predictive model is used to estimate replacements for missing values. The attribute with missing data is used as the response attribute, and the remaining attributes are used as input for the predictive model.
	
\end{itemize}

\subsection*{Splitting data set for test and training}

If the data set was not already splitted (e.g. as the case with the \texttt{shuttle} data set), we created a randomized but balanced 80/20\% split using caret's \texttt{createDataPartition} \cite{inside-R-docs-caret-createDataPartition} function (cf. \cite{caret-Splitting}). The 80\% part was used for training and model selection, the 20\% part for evaluating the chosen model.

\subsection*{Model selection}

The training and tuning of models for a classification algorithm was done via caret's \texttt{train} \cite{inside-R-docs-caret-train} function with the training set (obtained as described above). We chose to use 10-fold cross validation for training by default, instructing caret's \texttt{train} via the \texttt{trControl} parameter and caret's \texttt{trainControl} \cite{inside-R-docs-caret-trainControl} function. Furthermore, we set the parameters to tune via \texttt{train}'s \texttt{tuneGrid} parameter, e.g. the $k$ for \texttt{knn} or other parameters for the algorithm in the \texttt{dot} (\texttt{...}) section of \texttt{train}' arguments, e.g. the \texttt{ntree} parameter in case of the \texttt{rf} model to set the number of trees to grow for \texttt{randomForest}.

\subsection*{Reproducibility}

We took care that our results are reproducible on multiple runs by setting a fixed seed value for the random number generator (via \texttt{set.seed}) before executing any operation that relies on random input (like bootstrapping). The seed values were chosen arbitrarily.

\subsection*{Parallel processing}

caret allows for easy parallelization (cf. \cite{caret-Parallel-Processing}) by simply loading and setting up multi-core support with special \textsf{R} packages such as \textit{doMC}. We tried to solely use the \textit{doMC} package, but it did not work with Windows. Therefore, we wrote a quick wrapper function that loads a working package for the environment (either \textit{doMC} or \textit{doParallel}). We set the number of CPU cores to use to two because all our machines had at least two CPU cores.

\subsection*{Hardware}

We finally trained and evaluated our classifiers on a single machine so that runtime metrics are comparable. The key data of this machine:
\\

\begin{tabular}{ll}
	\textbf{Operating system} & Debian GNU/Linux 8 \\
	\textbf{CPU} & MD Athlon(tm) 64 X2 Dual Core Processor 5600+ (2 cores) \\
	\textbf{RAM} & 4GB \\
	\textbf{R version} & 3.2.3 (2015-12-10) -- "Wooden Christmas-Tree" \\
\end{tabular}
