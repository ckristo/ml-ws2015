\section*{\texttt{voting} data set}
\label{sec:voting}

\subsection*{Description}

This data set includes votes for each of the U.S. House of Representatives Congressmen on the 16 key votes identified by the CQA.  The CQA lists nine different types of votes: \textit{voted for}, \textit{paired for}, and \textit{announced for} (these three were simplified to \textit{yea}), \textit{voted against}, \textit{paired against}, and \textit{announced against} (these three were simplified to \textit{nay}), \textit{voted present}, \textit{voted present to avoid conflict of interest}, and \textit{did not vote or otherwise make a position known} (these three were simplified to an \textit{unknown disposition}).

The goal of this data set was to correctly classify a voter's association to one of the two main political parties in the US, namely Democrats and Republicans.

See table \ref{tbl:votingDataSetInformation} for detailed information on the data set.

\begin{table}[h]
\resizebox{16cm}{!} {
	\begin{tabular}{|l|l|l|l|l|}
		\hline
		\multicolumn{5}{|c|}{\textbf{voting data set details}} \\
		\hline
		\textbf{Number of instances} & \multicolumn{4}{|r|}{435 (267 democrats, 168 republicans)} \\
		\hline
		\textbf{Number of attributes} & \multicolumn{4}{|r|}{16 + class name = 17 (all categorical)} \\
		\hline
		\textbf{Missing attribute values} & \multicolumn{4}{|r|}{Yes, denoted by "?"} \\
		\hline
		\textbf{Class distribution (total)} & \multicolumn{4}{|r|}{2 classes, 61.4 percent are democrats, 38.6 percent are republican} \\
		\hline
		\textbf{Class distribution (training)} & \multicolumn{4}{|r|}{2 classes, 61.3 percent are democrats, 38.7 percent are republican} \\
		\hline
		\textbf{Class distribution (test)} & \multicolumn{4}{|r|}{2 classes, 61.6 percent are democrats, 38.4 percent are republican} \\
		\hline
		\multicolumn{5}{|c|}{\textbf{attribute information}} \\
		\hline
		\textbf{name} & \textbf{type} & \textbf{num. of values} & \textbf{poss. values} & \textbf{num. of miss. values} \\
		\hline
		class name & categorical & 2 & democrat, republican & 0 \\
		\hline
		handicapped-infants & categorical & 2 & y, n & 12 \\
		\hline
		water-project-cost-sharing & categorical & 2 & y, n & 48 \\
		\hline
		adoption-of-the-budget-resolution & categorical & 2 & y, n & 11 \\
		\hline
		physician-fee-freeze & categorical & 2 & y, n & 11 \\
		\hline
		el-salvador-aid & categorical & 2 & y, n & 15 \\
		\hline
		religious-groups-in-schools & categorical & 2 & y, n & 11 \\
		\hline
		anti-satellite-test-ban & categorical & 2 & y, n & 14 \\
		\hline
		aid-to-nicaraguan-contras & categorical & 2 & y, n & 15 \\
		\hline
		mx-missile & categorical & 2 & y, n & 22 \\
		\hline
		immigration & categorical & 2 & y, n & 7 \\
		\hline
		synfuels-corporation-cutback & categorical & 2 & y, n & 21 \\
		\hline
		education-spending & categorical & 2 & y, n & 31 \\
		\hline
		superfund-right-to-sue & categorical & 2 & y, n & 25 \\
		\hline
		crime & categorical & 2 & y, n & 17 \\
		\hline
		duty-free-exports & categorical & 2 & y, n & 28 \\
		\hline
		export-administration-act-south-africa & categorical & 2 & y, n & 104 \\
		\hline
	\end{tabular}
}
\centering
\caption{voting data set information}
\label{tbl:votingDataSetInformation}
\end{table}

\subsection*{Normalization}

Since the data set only contains categorical attributes, no normalization had to be applied.

\subsection*{Missing values}

As seen in table \ref{tbl:votingDataSetInformation}, most of the attributes in the data set have a considerable amount of missing values in relation to the number of samples. Only 232 out of 435 samples contain complete information (i.e., don't have a missing value in any of the attributes), thus 53.3\% of the samples contain at least one missing value.

Having an amount of missing values greater than 15\% is expected to have a severe impact on a classifier's accuracy\cite{paper-AcunaRodriguez-TreatmingOfMissingValuesAndItsEffect, blog-Alice-ImputingMissingDataWithR}, so we removed only those that have missing values in a considerable amount of their attributes (above 50\%), dropping about 1\% of all samples.

Looking at the amount of missing values per attribute (also see figure \ref{fig:votingMissingValues}), we see that attributes \textit{export-administration-act-south-africa}, \textit{water-project-cost-sharing} and \textit{education-spending} suffer the most from missing values.

\begin{figure}[!h]
	\centering
		\includegraphics[width=0.95\textwidth]{figures/voting/missing_values}
	\caption{Missing values in Voting data set}
	\label{fig:votingMissingValues}
\end{figure}

In order to cope with this, we followed the methods outlined in section \ref{sec:handlingMissingValues}.

\subsubsection*{Simple approach}

For a first attempt, we just removed the samples having a considerate (above 50\%) amount of missing values in their attributes, as stated above, and did not manipulate the data any further (letting the classifiers figure out how to deal with the missing values, see \cite{website-Breiman-RandomForestsMissingValues} for an example how random forests achieve this). This lead to the initial results shown in table \ref{tbl:votingClassifierResultsWithMissingValues}.

\begin{table}[h]
\resizebox{16cm}{!} {
	\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
		\hline
		\textbf{Classifier} & \textbf{Parameters} & \textbf{Model Selection} & \textbf{Accuracy} & \textbf{Precision} & \textbf{Recall} & \textbf{F1 Score} & \textbf{Train time (sec)} & \textbf{Pred. time (sec)} \\
		\hline
		rf1 & 500 trees & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 4.33 & 0.07 \\
		\hline
		rf2 & 500 trees & bootstrapping with 100 iterations & 0.9167 & 0.9183 & 0.9183 & 0.92 & 6.87 & 0.02 \\
		\hline
		rf3 & 100 trees & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 1.40 & 0.02 \\
		\hline
		rf4 & 20 trees & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 1.14 & 0.02 \\
		\hline
		rf5 & 5 trees & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 1.05 & 0.02 \\
		\hline
		svm1 & linear kernel & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 2.40 & 0.06 \\
			 & C = 0.1 & & & & & & & \\
		\hline
		svm2 & linear kernel & bootstrapping with 25 iterations & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 2.99 & 0.03 \\
			 & C = 0.1 & & & & & & & \\
		\hline
		svm3 & polynomial kernel & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 5.24 & 0.02 \\
			 & (degree = 2, scale = 0.01) & & & & & & & \\
			 & C = 1 & & & & & & & \\
		\hline
		svm4 & radial kernel & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 4.68 & 0.02 \\
			 & (sigma = 0.03166) & & & & & & & \\
			 & C = 1 & & & & & & & \\
		\hline
		mlp1 & 1 hidden layer & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 10.82 & 0.25 \\
			 & size = 5 & & & & & & & \\
		\hline
		mlp2 & 1 hidden layer & bootstrapping with 25 iterations & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 30.16 & 0.10 \\
			 & size = 20 & & & & & & & \\
		\hline
		mlp3 & 2 hidden layers & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 44.70 & 0.12 \\
			 & layer 1 = 5 & & & & & & & \\
			 & layer 2 = 5 & & & & & & & \\
		\hline
		mlp4 & 3 hidden layers & 10-fold cross validation & 0.9167 & 0.9183 & 0.9183 & 0.9167 & 195.55 & 0.12 \\
			& layer 1 = 5 & & & & & & & \\
			& layer 2 = 5 & & & & & & & \\
			& layer 3 = 15 & & & & & & & \\
		\hline
		knn1 & k = 5 & 10-fold cross validation & 0.8958 & 0.8958 & 0.8965 & 0.8958 & 1.51 & 0.02 \\
		\hline
		knn2 & k = 5 & 3x repeated 10-fold cv & 0.8958 & 0.8958 & 0.8965 & 0.8958 & 1.75 & 0.02 \\
		\hline
		knn3 & k = 3 & bootstrapping with 25 iterations & 0.8958 & 0.8958 & 0.8965 & 0.8958 & 1.69 & 0.02 \\
		\hline
		knn4 & k = 5 & 10-fold cross validation & 0.8958 & 0.8958 & 0.8965 & 0.8958 & 1.39 & 0.02 \\
		\hline
		nb1 & fL = 0 & 10-fold cross validation & 0.8958 & 0.8958 & 0.8965 & 0.8958 & 3.39 & 0.30 \\
			& usekernel = TRUE & & & & & & & \\
		\hline
		nb2 & fL = 0 & bootstrapping with 25 iterations & 0.8958 & 0.8958 & 0.8965 & 0.8958 & 15.54 & 0.20 \\
			& usekernel = TRUE & & & & & & & \\
		\hline
	\end{tabular}
}
\centering
\caption{Classifier results for the Voting data set including missing values}
\label{tbl:votingClassifierResultsWithMissingValues}
\end{table}

Random Forests, Support Vector Machines and Multilayer Perceptrons scored the highest both in terms of accuracy, precision, recall and F1 Score.

Since the results were all very similar, one could have guessed that there is no statistically significant difference between each of the algorithms, which could be confirmed by a performed McNemar test (95\% confidence interval).

\subsubsection*{Data Imputation}

Subsequently, we performed data imputation (cf. section \textit{Handling missing values}\ref{sec:handlingMissingValues} in \textit{Methodology}) to handle the amount of missing values within the data set using the \textsf{R} packages \textit{mice}\cite{CRAN-mice} and \textit{ForImp}\cite{CRAN-ForImp}.

Used imputation techniques with \textit{mice} were \textit{predictive model imputation} (using a random forest and cart), mode imputation was done using \textit{ForImp}.

\begin{itemize}
	\item \textbf{Predictive model - random forest}, see table \ref{tbl:votingClassifierResultsWithRFImputation}. Response variable was the attribute containing missing values, input variables were all other attributes except for the \textit{Class} attribute.
	
	Using this approach, results improved slightly for most classifiers, the highest improvement was observed for \textbf{nb1} and \textbf{nb2} (Naive Bayes), which got a 0.04 higher accuracy than on the data set that still included missing values.
	
	Interestingly, \textbf{rf5} (Random Forest) performed worse than on the data set with missing values, having a slightly lower accuracy (-0.01). Also, \textbf{svm1} (Support Vector Machine) went up the ranks, scoring a 0.014 higher accuracy.
	
	Performing a McNemar test to compare each classifier pairwise on the processed datasets (e.g. \textbf{nb1} on the data set with missing values and on the data set with rf imputation) showed that all of the differences in performance were statistically significant (95\% confidence interval).
	
	\item \textbf{Predictive model - cart}, see table \ref{tbl:votingClassifierResultsWithCartImputation}. CART is an umbrella term for \textit{Classification And Regression Tree}, so instead of a forest we used a decision tree for data imputation in this run. Response variable was the attribute
containing missing values, input variables were all other attributes except for the Class
attribute.
	
	Compared to the run where missing values have been included, similar changes as with random forest imputation could be observed. Most noteworthy is that \textbf{nb1} and \textbf{nb2} (Naive Bayes) again improved the most in terms of accuracy, all support vector machines improved (but not as much as with rf imputation), and \textbf{rf5} (Random Forest) became even worse than with rf imputation.
	
	Again, all these differences were statistically significant in terms of a McNemar test (95\% confidence interval).
	
	Compared to the run with random forest imputation, \textbf{rf1} and \textbf{rf2} got worse by a slight margin (-0.015 accuracy), \textbf{rf3} and \textbf{rf4} stayed the same (all Random Forests), \textbf{svm1} (Support Vector Machine) also became worse (-0.015 accuracy), now scoring the same as all the other support vector machines, \textbf{knn2} (k-Nearest Neighbours) got slightly better (+ 0.01 accuracy), and \textbf{rf5} (Random Forest) went down to the bottom of the list (-0.01 accuracy).
	
	None of these differences were statistically significant though in terms of a McNemar test (95\% confidence intervall).
	
	\item \textbf{Mode imputation}, see table \ref{tbl:votingClassifierResultsWithModeImputation}. This time, we used mode imputation to replace missing values with the most frequently observed value of a category.
	
		Interestingly, this time both \textbf{mlp2} and \textbf{mlp3} (Multilayer perceptrons) achieved the highest accuracy (both +0.03 accuracy compared to the run including missing values), where as with other imputation techniques they only improved by 0.002. This also put them at the top of the result list. \textbf{mlp1} and \textbf{mlp4} went up by 0.014 compared to the run including missing values. The only classifier that scored a lower accuracy than in the initial run was \textbf{knn3} (k-Nearest Neighbours), which dropped by 0.012 accuracy.
		
		Compared to the run including missing values, all of these differences are statistically significant in terms of a McNemar test (95\% confidence interval).
		
		Compared with the other data imputation techniques (random forest and cart), differences in performance were not statistically significant in terms of a McNemar test (95\% confidence interval). It's still noteworthy that \textbf{rf5} (Random Forest) was at the end of the list using predictive model imputation, but is now amongst the best classifiers in terms of accuracy.
	
\end{itemize}

\begin{table}[h]
\resizebox{16cm}{!} {
	\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
		\hline
		\textbf{Classifier} & \textbf{Parameters} & \textbf{Model Selection} & \textbf{Accuracy} & \textbf{Precision} & \textbf{Recall} & \textbf{F1 Score} & \textbf{Train time (sec)} & \textbf{Pred. time (sec)} \\
		\hline
		rf1 & 500 trees & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 7.33 & 0.03 \\
		\hline
		rf2 & 500 trees & bootstrapping with 100 iterations & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 11.41 & 0.02 \\
		\hline
		rf3 & 100 trees & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 1.73 & 0.02 \\
		\hline
		rf4 & 20 trees & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 1.24 & 0.02 \\
		\hline
		svm1 & linear kernel & 10-fold cross validation & 0.9302 & 0.9232 & 0.9320 & 0.9270 & 2.56 & 0.02 \\
			 & C = 0.1 & & & & & & & \\
		\hline
		nb1 & fL = 0 & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 4.50 & 0.09 \\
			& usekernel = FALSE & & & & & & & \\
		\hline
		nb2 & fL = 0 & bootstrapping with 25 iterations & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 25.64 & 0.09 \\
			& usekernel = FALSE & & & & & & & \\
		\hline
		svm2 & linear kernel & bootstrapping with 25 iterations & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 3.49 & 0.02 \\
			 & C = 1 & & & & & & & \\
		\hline
		svm3 & polynomial kernel & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 7.42 & 0.02 \\
			 & (degree = 2, scale = 0.01) & & & & & & & \\
			 & C = 100 & & & & & & & \\
		\hline
		svm4 & radial kernel & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 7.34 & 0.02 \\
			 & (sigma = 0.01852) & & & & & & & \\
			 & C = 100 & & & & & & & \\
		\hline
		mlp1 & 1 hidden layer & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 18.09 & 0.15 \\
			 & size = 10 & & & & & & & \\
		\hline
		mlp2 & 1 hidden layer & bootstrapping with 25 iterations & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 51.41 & 0.15 \\
			 & size = 5 & & & & & & & \\
		\hline
		mlp3 & 2 hidden layers & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 73.74 & 0.15 \\
			 & layer 1 = 5 & & & & & & & \\
			 & layer 2 = 20 & & & & & & & \\
		\hline
		mlp4 & 3 hidden layers & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 321.00 & 0.17 \\
			& layer 1 = 10 & & & & & & & \\
			& layer 2 = 15 & & & & & & & \\
			& layer 3 = 15 & & & & & & & \\
		\hline
		rf5 & 5 trees & 10-fold cross validation & 0.9070 & 0.9017 & 0.9017 & 0.9017 & 1.11 & 0.02 \\
		\hline
		knn1 & k = 5 & 10-fold cross validation & 0.8953 & 0.8880 & 0.8922 & 0.8900 & 1.35 & 0.02 \\
		\hline
		knn2 & k = 5 & 3x repeated 10-fold cv & 0.8953 & 0.8880 & 0.8922 & 0.8900 & 1.81 & 0.02 \\
		\hline
		knn3 & k = 3 & bootstrapping with 25 iterations & 0.8954 & 0.8867 & 0.8979 & 0.8911 & 1.90 & 0.02 \\
		\hline
		knn4 & k = 5 & 10-fold cross validation & 0.8954 & 0.8867 & 0.8979 & 0.8911 & 1.47 & 0.02 \\
		\hline
	\end{tabular}
}
\caption{Classifier results for the Voting data set, missing values imputed by predictive model imputation (forest)}
\label{tbl:votingClassifierResultsWithRFImputation}
\end{table}

\begin{table}[h]
\resizebox{16cm}{!} {
	\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
		\hline
		\textbf{Classifier} & \textbf{Parameters} & \textbf{Model Selection} & \textbf{Accuracy} & \textbf{Precision} & \textbf{Recall} & \textbf{F1 Score} & \textbf{Train time (sec)} & \textbf{Pred. time (sec)} \\
		\hline
		rf3 & 100 trees & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 2.27 & 0.02 \\
		\hline
		rf4 & 20 trees & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 1.41 & 0.02 \\
		\hline
		nb1 & fL = 0 & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 4.63 & 0.09 \\
			& usekernel = FALSE & & & & & & & \\
		\hline
		nb2 & fL = 0 & bootstrapping with 25 iterations & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 27.38 & 0.09 \\
			& usekernel = FALSE & & & & & & & \\
		\hline
		rf1 & 500 trees & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 10.50 & 0.03 \\
		\hline
		rf2 & 500 trees & bootstrapping with 100 iterations & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 17.71 & 0.02 \\
		\hline
		svm1 & linear kernel & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 2.92 & 0.02 \\
			 & C = 1 & & & & & & & \\
		\hline
		svm2 & linear kernel & bootstrapping with 25 iterations & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 4.53 & 0.02 \\
			 & C = 1 & & & & & & & \\
		\hline
		svm3 & polynomial kernel & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 10.23 & 0.02 \\
			 & (degree = 2, scale = 0.01) & & & & & & & \\
			 & C = 100 & & & & & & & \\
		\hline
		svm4 & radial kernel & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 8.81 & 0.02 \\
			 & (sigma = 0.03777) & & & & & & & \\
			 & C = 10 & & & & & & & \\
		\hline
		mlp1 & 1 hidden layer & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 26.37 & 0.20 \\
			 & size = 10 & & & & & & & \\
		\hline
		mlp2 & 1 hidden layer & bootstrapping with 25 iterations & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 76.21 & 0.15 \\
			 & size = 20 & & & & & & & \\
		\hline
		mlp3 & 2 hidden layers & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 106.80 & 0.17 \\
			 & layer 1 = 10 & & & & & & & \\
			 & layer 2 = 5 & & & & & & & \\
		\hline
		mlp4 & 3 hidden layers & 10-fold cross validation & 0.9186 & 0.9123 & 0.9168 & 0.9144 & 419.17 & 0.17 \\
			& layer 1 = 20 & & & & & & & \\
			& layer 2 = 10 & & & & & & & \\
			& layer 3 = 20 & & & & & & & \\
		\hline
		knn2 & k = 5 & 3x repeated 10-fold cv & 0.9070 & 0.8992 & 0.9074 & 0.902715 & 2.70 & 0.02 \\
		\hline
		knn1 & k = 5 & 10-fold cross validation & 0.8953 & 0.8880 & 0.8922 & 0.8900 & 2.41 & 0.02 \\
		\hline
		knn3 & k = 3 & bootstrapping with 25 iterations & 0.8953 & 0.8880 & 0.8922 & 0.8900 & 2.29 & 0.02 \\
		\hline
		knn4 & k = 5 & 10-fold cross validation & 0.8953 & 0.8880 & 0.8922 & 0.8900 & 1.67 & 0.02 \\
		\hline
		rf5 & 5 trees & 10-fold cross validation & 0.8953 & 0.8912 & 0.8865 & 0.8887 & 1.26 & 0.02 \\
		\hline
	\end{tabular}
}
\caption{Classifier results for the Voting data set, missing values imputed by predictive model imputation (cart)}
\label{tbl:votingClassifierResultsWithCartImputation}
\end{table}

\begin{table}[h]
\resizebox{16cm}{!} {
	\begin{tabular}{|l|l|l|l|l|l|l|l|l|}
		\hline
		\textbf{Classifier} & \textbf{Parameters} & \textbf{Model Selection} & \textbf{Accuracy} & \textbf{Precision} & \textbf{Recall} & \textbf{F1 Score} & \textbf{Train time (sec)} & \textbf{Pred. time (sec)} \\
		\hline
		mlp2 & 1 hidden layer & bootstrapping with 25 iterations & 0.9419 & 0.9344 & 0.9471 & 0.9395 & 51.63 & 0.13 \\
			 & size = 15 & & & & & & & \\
		\hline
		mlp3 & 2 hidden layers & 10-fold cross validation & 0.9419 & 0.9344 & 0.9471 & 0.9395 & 74.39 & 0.15 \\
			 & layer 1 = 10 & & & & & & & \\
			 & layer 2 = 20 & & & & & & & \\
		\hline
		rf1 & 500 trees & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 7.82 & 0.01 \\
		\hline
		rf2 & 500 trees & bootstrapping with 100 iterations & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 11.63 & 0.01 \\
		\hline
		rf5 & 5 trees & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 1.14 & 0.01 \\
		\hline
		svm1 & linear kernel & 10-fold cross validation & 0.9302 & 0.9232 & 0.9320 & 0.9270 & 2.64 & 0.01 \\
			 & C = 1 & & & & & & & \\
		\hline
		svm2 & linear kernel & bootstrapping with 25 iterations & 0.9302 & 0.9232 & 0.9320 & 0.9270 & 3.60 & 0.01 \\
			 & C = 1 & & & & & & & \\
		\hline
		svm3 & polynomial kernel & 10-fold cross validation & 0.9302 & 0.9232 & 0.9320 & 0.9270 & 7.52 & 0.01 \\
			 & (degree = 3, scale = 0.01) & & & & & & & \\
			 & C = 0.1 & & & & & & & \\
		\hline
		mlp1 & 1 hidden layer & 10-fold cross validation & 0.9302 & 0.9232 & 0.9320 & 0.9270 & 18.26 & 0.14 \\
			 & size = 10 & & & & & & & \\
		\hline
		mlp4 & 3 hidden layers & 10-fold cross validation & 0.9302 & 0.9232 & 0.9320 & 0.9270 & 323.05 & 0.16 \\
			& layer 1 = 10 & & & & & & & \\
			& layer 2 = 20 & & & & & & & \\
			& layer 3 = 20 & & & & & & & \\
		\hline
		nb1 & fL = 0 & 10-fold cross validation & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 5.00 & 0.08 \\
			& usekernel = FALSE & & & & & & & \\
		\hline
		nb2 & fL = 0 & bootstrapping with 25 iterations & 0.9302 & 0.9262 & 0.9262 & 0.9262 & 26.71 & 0.08 \\
			& usekernel = FALSE & & & & & & & \\
		\hline
		rf3 & 100 trees & 10-fold cross validation & 0.9186 & 0.9161 & 0.9111 & 0.9134 & 1.72 & 0.01 \\
		\hline
		knn1 & k = 5 & 10-fold cross validation & 0.9070 & 0.8992 & 0.9074 & 0.9027 & 1.69 & 0.01 \\
		\hline
		knn2 & k = 5 & 3x repeated 10-fold cv & 0.9070 & 0.8992 & 0.9074 & 0.9027 & 1.85 & 0.01 \\
		\hline
		knn4 & k = 5 & 10-fold cross validation & 0.9070 & 0.8992 & 0.9074 & 0.9027 & 1.49 & 0.01 \\
		\hline
		rf4 & 20 trees & 10-fold cross validation & 0.9067 & 0.9062 & 0.8959 & 0.9005 & 1.23 & 0.01 \\
		\hline
		svm4 & radial kernel & 10-fold cross validation & 0.9070 & 0.9017 & 0.9017 & 0.9017 & 7.25 & 0.01 \\
			 & (sigma = 0.08357) & & & & & & & \\
			 & C = 10 & & & & & & & \\
		\hline
		knn3 & k = 3 & bootstrapping with 25 iterations & 0.8837 & 0.8751 & 0.8828 & 0.8784 & 1.91 & 0.01 \\
		\hline
	\end{tabular}
}
\caption{Classifier results for the Voting data set, missing values imputed by mode imputation}
\label{tbl:votingClassifierResultsWithModeImputation}
\end{table}

\subsubsection*{Handling missing values - Summary}

	Compared to the run including missing values, mode imputation improved the results the most, having the best overall accuracy. This is only statistically significant compared to the missing values run though, no statistical significance is present when comparing classifiers between different data imputation attempts. It's also interesting that model selection fits models with different parameters depending on the chosen imputation technique (e.g. hidden layer size in the multilayer perceptrons, kernel parameters for support vector machines, etc).
	
	Regarding the training time of the classifiers, in general data imputation increased the training time since there were more values to process.

\subsubsection*{Effect of different sampling techniques}

Analogous to the evaluation of the \texttt{shuttle} data set, we tried different sampling techniques (10-fold cross validation, 3x repeated 10-fold cross validation, bootstrapping with 25 iterations) for training the classifiers for the \texttt{voting} data set. Here we tried different sampling techniques on all classifiers, but didn't observe any significant change in performance.

	Regarding the training time of the classifiers, 10-fold cross validation is the fastest (e.g., \textbf{rf1} needed 4.33 seconds to train, \textbf{rf2} needed 6.865 seconds, the only difference being the sampling method - 10-fold cross validation for \textbf{rf1}, bootstrapping with 25 iterations for \textbf{rf2}).