#!/usr/bin/env Rscript

source('common.R')
source('eval.R')

library(ForImp)

options(warn=1)

registerMultiCore(2);

dataSetName = "voting-imputed-mode"
dataSetModelsFolder = "../../models/voting"

# load data set
data <- read.table("../../data/voting/voting.data", header = F, sep=",", na.strings = "?")
attrNames <- c("Class", "handicapped-infants", "water-project-cost-sharing", "adoption-of-the-budget-resolution", "physician-fee-freeze", "el-salvador-aid", "religious-groups-in-schools", "anti-satellite-test-ban", "aid-to-nicaraguan-contras", "mx-missile", "immigration", "synfuels-corporation-cutback", "education-spending", "superfund-right-to-sue", "crime", "duty-free-exports", "export-administration-act-south-africa")
names(data) <- attrNames

data$Class <- as.factor(data$Class)

# remove samples with at least 8 missing values in their attributes (total number of attributes = 16)
i <- getMissingValuesByRow(data, 8)
data <- data[-i, ]

# imputation via mode
set.seed(621)
dataImputed <- data.frame(modeimp(data.matrix(data[, !(names(data) %in% c("Class"))])))
dataImputed$Class <- data$Class
data <- dataImputed

# split into training and test set
set.seed(973)
i <- createDataPartition(data$Class, p = .8, list = FALSE)
trainData <- data[ i, ]
testData  <- data[-i, ]

# remove all rows from with missing values from testData set,
# since these apparently get dropped by the predict() function anyways
# and subsequently let the call to, e.g., confusionMatrix() fail
testData <- testData[complete.cases(testData),]

# stores all models in a global variable to make them accessible within the current workspace
# (useful to play around with statistics, plots, etc)
modelCollection <<- list()

beginPerformanceMeasuresRecording()

# switch forceTrain on/off
forceTrain <- F

# ## knn #################################################################

##
# knn1 -- kNN with k = {1,3,5,7,9,11}, 10-fold cross validation.
#
trainKnn1 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "knn",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(k = c(1, 3, 5, 7, 9, 11))
  )
}

##
# knn2 -- kNN with k = {1,3,5,7,9,11}, 3x 10-fold cross validation.
#
trainKnn2 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "knn",
    preProcess = NULL,
    trControl = trainControl(method = "repeatedcv", number = 10, repeats = 3, verboseIter = T),
    tuneGrid = expand.grid(k = c(1, 3, 5, 7, 9, 11))
  )
}

##
# knn3 -- kNN with k = {1,3,5,7,9,11}, bootstrapping with 25 iterations.
#
trainKnn3 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "knn",
    preProcess = NULL,
    trControl = trainControl(method = "boot", number = 25, verboseIter = T),
    tuneGrid = expand.grid(k = c(1, 3, 5, 7, 9, 11))
  )
}

##
# knn4 -- kNN with k = {1,3,5,7,9,11,13,15,17,19,21,23}, 10-fold cross validation.
#
trainKnn4 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "knn",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(k = seq(1, 23, 2))
  )
}

evaluateClassifier(trainKnn1, testData, "Class", dataSetName, classifierName = "knn1", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainKnn2, testData, "Class", dataSetName, classifierName = "knn2", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainKnn3, testData, "Class", dataSetName, classifierName = "knn3", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainKnn4, testData, "Class", dataSetName, classifierName = "knn4", dataSetModelsFolder, seed = 251, forceTrain)

# ## rf ##################################################################

##
# rf1 -- Random Forest with 500 trees, 10-fold cross validation.
#
trainRf1 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "rf",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    importance = TRUE
  )
}

##
# rf2 -- Random Forest with 500 trees, bootstrapping with 100 iterations.
#
trainRf2 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "rf",
    preProcess = NULL,
    trControl = trainControl(method = "boot", number = 25, verboseIter = T)
  )
}

##
# rf3 -- Random Forest with 100 trees, 10-fold cross validation.
#
trainRf3 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    ntree = 100,
    method = "rf",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T)
  )
}

##
# rf4 -- Random Forest with 20 trees, 10-fold cross validation.
#
trainRf4 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    ntree = 20,
    method = "rf",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T)
  )
}

##
# rf5 -- Random Forest with 5 trees, 10-fold cross validation.
#
trainRf5 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    ntree = 5,
    method = "rf",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T)
  )
}

evaluateClassifier(trainRf1, testData, "Class", dataSetName, classifierName = "rf1", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainRf2, testData, "Class", dataSetName, classifierName = "rf2", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainRf3, testData, "Class", dataSetName, classifierName = "rf3", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainRf4, testData, "Class", dataSetName, classifierName = "rf4", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainRf5, testData, "Class", dataSetName, classifierName = "rf5", dataSetModelsFolder, seed = 251, forceTrain)

# ## svm ##################################################################

##
# svm1 -- SVM with linear kernel, 10-fold cross validation.
#
trainSvm1 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "svmLinear",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(C = c(0.001, 0.01, 0.1, 1, 10, 100, 1000))
  )
}

##
# svm2 -- SVM with linear kernel, bootstrapping with 25 iterations.
#
trainSvm2 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "svmLinear",
    preProcess = NULL,
    trControl = trainControl(method = "boot", number = 25, verboseIter = T),
    tuneGrid = expand.grid(C = c(0.001, 0.01, 0.1, 1, 10, 100, 1000))
  )
}

##
# svm3 -- SVM with polynomial kernel, 10-fold cross validation.
#
trainSvm3 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "svmPoly",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(degree = c(2, 3), scale = c(0.01, 0.1), C = c(0.001, 0.01, 0.1, 1, 10, 100, 1000))
  )
}

##
# svm4 -- SVM with radial basis kernel, 10-fold cross validation.
#
trainSvm4 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "svmRadial",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(sigma = sigest(Class ~ ., data = trainData), C = c(0.001, 0.01, 0.1, 1, 10, 100, 1000))
  )
}

evaluateClassifier(trainSvm1, testData, "Class", dataSetName, classifierName = "svm1", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainSvm2, testData, "Class", dataSetName, classifierName = "svm2", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainSvm3, testData, "Class", dataSetName, classifierName = "svm3", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainSvm4, testData, "Class", dataSetName, classifierName = "svm4", dataSetModelsFolder, seed = 251, forceTrain)

# ## mlp ##################################################################

##
# mlp1 -- Multi-layer perceptron with 1 hidden layer, 10-fold cross validation.
#
trainMlp1 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "mlp",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(size = c(5, 10, 15, 20))
  )
}

##
# mlp2 -- Multi-layer perceptron with 1 hidden layer, bootstrapping with 25 iterations.
#
trainMlp2 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "mlp",
    preProcess = NULL,
    trControl = trainControl(method = "boot", number = 25, verboseIter = T),
    tuneGrid = expand.grid(size = c(5, 10, 15, 20))
  )
}

##
# mlp3 -- Multi-layer perceptron with 2 hidden layers, 10-fold cross validation.
#
trainMlp3 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "mlpML",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(layer1 = c(5, 10, 15, 20), layer2 = c(5, 10, 15, 20), layer3 = 0)
  )
}

##
# mlp4 -- Multi-layer perceptron with 3 hidden layers, 10-fold cross validation.
#
trainMlp4 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "mlpML",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(layer1 = c(5, 10, 15, 20), layer2 = c(5, 10, 15, 20), layer3 = c(5, 10, 15, 20))
  )
}

evaluateClassifier(trainMlp1, testData, "Class", dataSetName, classifierName = "mlp1", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainMlp2, testData, "Class", dataSetName, classifierName = "mlp2", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainMlp3, testData, "Class", dataSetName, classifierName = "mlp3", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainMlp4, testData, "Class", dataSetName, classifierName = "mlp4", dataSetModelsFolder, seed = 251, forceTrain)

# ## nb ##################################################################

##
# nb1 -- Naive Bayes, 10-fold cross validation.
#
trainNb1 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "nb",
    preProcess = NULL,
    trControl = trainControl(method = "cv", number = 10, verboseIter = T),
    tuneGrid = expand.grid(usekernel = c(F, T), fL = c(0, 1))
  )
}

##
# nb1 -- Naive Bayes, bootstrapping with 25 iterations.
#
trainNb2 <- function() {
  train(
    form = Class ~ .,
    data = trainData,
    method = "nb",
    preProcess = NULL,
    trControl = trainControl(method = "boot", number = 25, verboseIter = T),
    tuneGrid = expand.grid(usekernel = c(F, T), fL = c(0, 1))
  )
}

evaluateClassifier(trainNb1, testData, "Class", dataSetName, classifierName = "nb1", dataSetModelsFolder, seed = 251, forceTrain)
evaluateClassifier(trainNb2, testData, "Class", dataSetName, classifierName = "nb2", dataSetModelsFolder, seed = 251, forceTrain)

evaluationFile = getPerformanceMetricsOutputFilePath(dataSetName, dataSetModelsFolder)
evaluation <- endPerformanceMeasuresRecording(outputFile = evaluationFile, outputType = "csv")

significanceTests <- testAllForSignificanceWithMcNemar(paste0(evaluationFile, ".csv"), 
                                                       outputFile = getDataSetSignificanceTestingEvaluationOutputFilePath(dataSetName, dataSetModelsFolder), 
                                                       outputType = "csv")
