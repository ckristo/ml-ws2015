#!/bin/bash

readonly SCRIPTNAME=`basename $0`
readonly RSCRIPTSPATH="./R"

##
# Prints usage.
#
function printUsage {
  echo "$SCRIPTNAME <dataSet> [<variant>]"
}

##
# Constructs the R script name for a given data set and variant.
# @param the data set name
# @param (optional) the variant name
# @return $RScriptName
#
function getRScriptName {
  local dataSet=$1
  local variant=$2
  
  RScriptName="$dataSet"
  if [ ! -z "$variant" ]
  then
  	RScriptName="$RScriptName-$variant"
  fi
  RScriptName="$RScriptName.R"
}

##
# main().
#
function main {
  local dataSet=$1
  local variant=$2
  local scriptName=
  
  if [ -z "$dataSet" ]
  then
    >&2 echo "Required argument '<dataSet>' missing"
    printUsage
    exit 1
  fi
  
  getRScriptName $dataSet $variant
  
  if [ ! -f "$RSCRIPTSPATH/$RScriptName" ]
  then
    >&2 echo "No experiment found for given data set and variant"
    exit 2
  fi
  
  cd $RSCRIPTSPATH
  Rscript $RScriptName
  return $?
}

main $@
exit $?
