# data sets #

Chosen ones:

* http://archive.ics.uci.edu/ml/datasets/Statlog+%28Shuttle%29
  - number of instances: 58000
  - number of attributes: 9
  - output: 7 classes
  - attribute characteristics: integer
  - missing values: n/a (no after check)
  
* http://archive.ics.uci.edu/ml/datasets/Annealing
  - number of instances: 798
  - number of attributes: 38
  - output: 6 classes
  - attribute characteristics: categorical, integer, real
  - missing values: yes

* http://archive.ics.uci.edu/ml/datasets/Statlog+%28Vehicle+Silhouettes%29
  - number of instances: 946
  - number of attributes: 18
  - output: 4 classes (vehicle types)
  - attribute characteristics: integer
  - missing values: n/a (I assume no though)

Interesting ones ::

* http://archive.ics.uci.edu/ml/datasets/Covertype
  - number of instances: 581012
  - number of attributes: 54
  - output: 7 classes (1-7 = forest cover types)
  - attribute characteristics: categorical, integer
  - missing values: no
  
* http://archive.ics.uci.edu/ml/datasets/TV+News+Channel+Commercial+Detection+Dataset
  - number of instances: 129685
  - number of attributes: 12
  - output: 2 classes (commercial / non-commercial)
  - attribute characteristics: real
  - missing values: n/a (after checking the dataset, I assume no)
  
* http://archive.ics.uci.edu/ml/datasets/MAGIC+Gamma+Telescope
  - number of instances: 19020
  - number of attributes: 11
  - output: 2 classes (g = gamma = signal, h = hadron = background)
  - attribute characteristics: real
  - missing values: no
  
* http://archive.ics.uci.edu/ml/datasets/p53+Mutants
  - number of instances: 16772
  - number of attributes: 5409
  - output: 2 classes (active / inactive)
  - attribute characteristics: real
  - missing values: yes
  
* http://archive.ics.uci.edu/ml/datasets/Pen-Based+Recognition+of+Handwritten+Digits
  - number of instances: 10992
  - number of attributes: 16
  - output: 10 classes (digits 0-9)
  - attribute characteristics: integer
  - missing values: no
  
* http://archive.ics.uci.edu/ml/datasets/Spoken+Arabic+Digit
  - number of instances: 8800 (10 digits x 10 repetitions x 88 speakers)
  - number of attributes: 13
  - output: 10 classes (digits 0-9)
  - attribute characteristics: real
  - missing values: no
  
* http://archive.ics.uci.edu/ml/datasets/Bach+Choral+Harmony
  - number of instances: 5665
  - number of attributes: 17
  - output: 101 classes (chords)
  - attribute characteristics: nominal, integer
  - missing values: no
  
* http://archive.ics.uci.edu/ml/datasets/Molecular+Biology+%28Splice-junction+Gene+Sequences%29
  - number of instances: 3190
  - number of attributes: 61
  - output: 3 classes (n, ei, ie)
  - attribute characteristics: categorical
  - missing values: no

* http://archive.ics.uci.edu/ml/datasets/Bank+Marketing
  - number of instances: 45211
  - number of attributes: 17
  - output: 2 classes (y/n)
  - attribute characteristics: real
  - missing values: n/a (yes after manual check)

* http://archive.ics.uci.edu/ml/datasets/Statlog+%28Vehicle+Silhouettes%29
  - number of instances: 946
  - number of attributes: 18
  - output: 4 classes (vehicle types)
  - attribute characteristics: integer
  - missing values: n/a (I assume no though)

* http://archive.ics.uci.edu/ml/datasets/Glass+Identification
  - number of instances: 214
  - number of attributes: 10
  - output: 7 (types of glass)
  - attribute characteristics: real
  - missing values: no

* http://archive.ics.uci.edu/ml/datasets/Soybean+%28Large%29
  - number of instances: 307
  - number of attributes: 35
  - output: 19 / 15 (used prev. in literature)
  - attribute characteristics: categorical
  - missing values: yes

* http://archive.ics.uci.edu/ml/datasets/Echocardiogram
  - number of instances: 132
  - number of attributes: 12
  - output: 2 classes (y/n)
  - attribute characteristics: categorical, integer, real
  - missing values: yes

* http://archive.ics.uci.edu/ml/datasets/Annealing
  - number of instances: 798
  - number of attributes: 38
  - output: 6 classes
  - attribute characteristics: categorical, integer, real
  - missing values: yes
  
* http://archive.ics.uci.edu/ml/datasets/Cylinder+Bands
  - number of instances: 512
  - number of attributes: 39
  - output: 2 classes (y/no)
  - attribute characteristics: categorical, integer, real
  - missing values: yes

* http://archive.ics.uci.edu/ml/datasets/Statlog+%28Shuttle%29
  - number of instances: 58000
  - number of attributes: 9
  - output: 7 classes
  - attribute characteristics: integer
  - missing values: n/a (no after check)

blacklisted ::

* http://archive.ics.uci.edu/ml/datasets/Heart+Disease
  - number of instances: 303
  - number of attributes: 75 (all) / 14 (selected)
  - output: 5 classes (0 = absence, 1-4 = presence)
  - attribute characteristics: categorical, integer, real
  - missing values: yes

* http://archive.ics.uci.edu/ml/datasets/Poker+Hand
  - number of instances: 1025010
  - number of attributes: 11
  - output: 10 classes
  - attribute characteristics: categorical, integer
  - missing values: no
  
* http://archive.ics.uci.edu/ml/datasets/Statlog+%28Australian+Credit+Approval%29
  - number of instances: 690
  - number of attributes: 14
  - output: 2 classes (y/n)
  - attribute characteristics: categorical, integer, real
  - missing values: yes
